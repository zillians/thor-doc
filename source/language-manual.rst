Thor Programming Language Manual
================================

Basic
-----

Function and Task
`````````````````

Simple Function
:::::::::::::::

Thor functions are introduced by keyword ``function``, and a name, parameter list,
return type and its body which contains statements. The return type is optional.

.. code-block:: javascript

    function max(a:int32, b:int32) : int32 {
        return a > b ? a : b;
    }

Function arguments are passed by value in Thor. For primitive types, the value of
the primitive type is copied, for object types, the reference's value is copied:

.. code-block:: javascript

    // primitive type
    function foo(a:int32) {
        // change the parameter's value, not the argument
        // which was passed in
        a = 7;
    }

    var x = 3;
    print(x);  // print 3
    foo(x);
    print(x);  // print 3

    // object type
    class IntWrapper {
        public var a : int32 = 3;
    }

    function bar(o:IntWrapper) {
        o.a = 7;
    }

    var x = new IntWrapper();
    print(x.a);  // print 3
    bar(x);
    print(x.a);  // print 7

Task
::::

Task is same as function, except:

#. Only tasks could be entry.
#. ``flow`` could be used in tasks only. Please take a look on later sections
   for detail.

Multiple Return Value
:::::::::::::::::::::

Thor functions can return multiple values, several types are enclosed by parentheses as its
return type. The returned values are bundled together. But the mutiple types can not be parameter's
type.

**How to get multiple values from function call**

Multiple values are bundled together, and there is no way to use it unless **unpack** its values out.
You can **unpack** it by using an assignment with several left-hand-side variables:

.. code-block:: javascript

    // return value types are enclosed by parenthesis
    function swap(a:int32, b:int32) : (int32, int32) {
        return b, a;
    }

    @entry
    task test_main() {
        var a = 3;
        var b = 4;
        print("a = \{a}, b = \{b}\n");
        a, b = swap(a, b);
        print("a = \{a}, b = \{b}\n");
        exit(0);
    }

Output::

    a = 3, b = 4
    a = 4, b = 3

Functions can return even more values at once:

.. code-block:: javascript

    // let compiler finds out proper return types itself(return type inference)
    function sort3(a:int32, b:int32, c:int32) {
        if(a > b) a, b = swap(a, b);
        if(a > c) a, c = swap(a, c);
        if(b > c) b, c = swap(b, c);
        return a, b, c;
    }

    @entry
    task test_main() {
        var a = 5;
        var b = 4;
        var c = 3;

        print("a = \{a}, b = \{b}, c = \{c}\n");
        a, b, c = sort3(a, b, c);
        print("a = \{a}, b = \{b}, c = \{c}\n");

        exit(0);
    }

Output::

    a = 5, b = 4, c = 3
    a = 3, b = 4, c = 5

**Nested Multiple Values**

You can pack multiple expressions as a multiple value by adding parentheses. When
performing assignment, both of the left-hand-side & right-hand-side expressions
should have the same structure:

.. code-block:: javascript

    var a = 0;
    var b = 0;

    a, b = (1, 2); // assign by new values together

    var c = 0;

    (a, b), c = (1, 2), 3;

The outmost parentheses can be omitted on scenarios which expecting only one expression.
And there is one thing worth to mention, multiple values can not be used to swap variables:

.. code-block:: javascript

    var a = 3;
    var b = 4;
    a, b = b, a;                    // won't acts like you expect
    print("a = \{a}, b = \{b}\n");

Because ``a`` is read and written within a statement. The results might be
unidentical on different situations.

The result is::

    a = 3, b = 3 (read then write)

Or::

    a = 4, b = 4 (write then read)

So users should avoid writing statements like this.

Function Overloading
::::::::::::::::::::

For functions which provide similar functionalities, it's better to give them
same name instead of using differents. Which lets users easily group functions
together in mind. For example, there is a simple ``max()`` function, it takes
only two arguments and return the bigger one.

.. code-block:: javascript

    function max(lhs: int32, rhs: int32): int32 { // version #1
        return (lhs > rhs ? lhs : rhs);
    }

What if we want to get the biggest one from three numbers? We can overload
another version of ``max()``:

.. code-block:: javascript

    function max(first: int32, second: int32, third: int32): int32 { // version #2
        return max(first, max(second, third)); // use previous one
    }

Users can write overloaded functions, Thor recognizes them by:

1. Different amounts of parameters
#. Different type of parameters

Notice that you can't overload a function by change its return type because
there is no way to let compiler know what's the expected result type from
a function call:

.. code-block:: javascript

    function f(): void { }

    function f(param: int32): void {  }  // overload

    function f(param: float32): void { } // another overload

    function f(): int32 { } // error! compiler don't know which to call while you write 'f()'
                            // there are two possible candidates: 'f(): void' & 'f(): int32'

Decide the target function from caller side:

.. code-block:: javascript

    function foo(i: int32): void {
        println("foo(int32)");
    }

    function foo(f32: float32): void {
        println("foo(float32)");
    }

    function foo(f64: float64): void {
        println("foo(float64)");
    }

    function test(): void {

        max(2, 3);    // # of parameters is two, call the version #1
        max(1, 2, 3); // # of parameters is three, call the version #2

        foo(123);     // print 'foo(int32)'
        foo(4.5);     // print 'foo(float64)', again, floating point literals has type 'float64'
        foo(4.5f);    // print 'foo(float32)', floating point literals with 'f' postfix is 'float32'
    }

Task: Asynchronous Function
:::::::::::::::::::::::::::

Thor support *asynchronous function call*, a function call runs in the
background parallelly:

.. code-block:: javascript

    function upload_file(filename:String) {
        // code goes here
    }

    function upload_dir() {
        // upload all files in dir at the same time
        // var files_in_dir = ...;
        // those will upload in background
        for(var f in files_in_dir) {
            async -> upload_file(f);  // upload_file() returns immediatly
                                      // and runs in background
        }
        print("uploading...");
    }

Package and Bundle
``````````````````

Package
:::::::

To make code base more maintainable and reusable, it's a common practice to modularize your code.
Thor provides this functionality through **package** system.

A **package** is an encapsulation scope of classes, functions, and variables.
It's structure is directly corresponding to its file system heirarchy.

Each directory creates a **package**, and all symbols defined in the sources under it are
encapsulated in its scope.

For example::

    src
     |---pack1
         |---a.t
         |---b.t
     |---pack2
         |---c.t
     |---pack3
         |---d.t
     |---main.t

.. code-block:: javascript

    // a.t
    function func_a() : void
    {
        println("I'm a!");
    }

.. code-block:: javascript

    // b.t
    class B
    {
        public function func_b() : void
        {
            println("I'm b!");
        }
    }

.. code-block:: javascript

    //c.t
    class C<T>
    {
        public function func_c() : void
        {
            println("I'm c!");
        }
    }

.. code-block:: javascript

    //d.t
    var foo : String = "I'm d!";

Note that all sources should be put under **src** directory and its sub-directories.

This structure creates three packages: **pack1**, **pack2** and **pack3**.
Everything defined in a.t and b.t is packed in **pack1**. Likewise, everything
defined in c.t is packed in **pack2**, and so on. Every sources directly under
the root of the source code heirarchy, i.e. ``src``, belongs to a special
**root package scope**. Thus, things in main.t are under **root** in the above example.

The following example code shows how to use these packages:

.. code-block:: javascript

    //main.t

    import pack1;      //general import
    import p2 = pack2; //alias import
    import .= pack3;   //expose import

    @entry
    task main()
    {
        pack1.func_a();

        var b = new pack1.B;
        b.func_b();

        var c = new p2.C<int32>;
        c.func_c();

        println(foo);
        exit(0);
    }

To access classes, functions or variables under a package, you need to **import** it.
There are three kinds of import:

**General Import**

The first import statement shown in the example is called **general import**.
It consists of the keyword **import** followed by the **package name**.
To access symbols in the package imported in this way, you need to fully qualify it
with the package name. Thus:

.. code-block:: javascript

    import pack1;
    ...
    ...
    pack1.func_a();

    var b = new pack1.B;
    b.func_b();

**Alias Import**

The second import statement is called **alias import**.
Now you can qualify symbols you want with the alias to the imported package:

.. code-block:: javascript

    import p2 = pack2;
    ...
    ...
    var c = new p2.C<int32>; // p2 is an alias to pack2!
    c.func_c();

**Expose Import**

The third import statement is called **expose import**.
It exposes all symbols under the imported package to your current package scope.
I.e. you don't need to qualify them with the package name.

.. code-block:: javascript

    import .= p3;
    ...
    ...
    println(foo); // foo is now accessible without package name qualification!

Package Hierarchy
:::::::::::::::::

You may organize your packages with directory structures containing sub-directories.
For example::

    src
     |---foo
          |---bar1
               |---bar1.t
          |---bar2
               |---bar2.t
          |---foo.t

At first glance, it may be seemed to create one package, foo, containing
two sub-packages, bar1 and bar2. It actually creates three packages:

* foo
* foo.bar1
* foo.bar2

Though **foo.bar1** and **foo.bar2** are seemed to be under **foo**,
**they are not**. They are only syntactically-related. You can't have access
to foo.bar1 or foo.bar2 simply by importing foo. For example:

.. code-block:: javascript

    import foo;

    @entry
    task main()
    {
        foo.bar1.func_in_bar1(); // NO! bar1 is not imported!
    }

.. code-block:: javascript

    import .= foo;

    @entry
    task main()
    {
        bar1.func_in_bar1(); // NO! bar1 is still not imported!
    }

Bundle
::::::

Thor lets you pack your whole package hierarchy into a **bundle**.
A bundle is a compressed file of a package hierarchy. You may create your
own bundle to share code with others, or use others' bundles providing
functionalities you need.

Create a Bundle
~~~~~~~~~~~~~~~

Given::

    src
     |--- foo
           |---foo.t

.. code-block:: javascript

    //foo.t
    function func() : void
    {
        println("Function in foo!");
    }

This file hierarchy creates one package **foo**. To create a bundle,
you need to:

1. Build the project: **tsc build debug** for debug build, or
   **tsc build release** for release build.
#. Create bundle: **tsc generate bundle**.

Now you should have a **foo.bundle** under your **bin** directory.

It's important to note that **a bundle cannot have anything under the root**.
For example::

    src
     |--- foo.t

You can still successfully generate a bundle. However, anyone attempt to use
this bundle will encounter a compile error. We forbid such usage because
the root package scope contains many important components making sure
Thor works. It's dangerous to allow any 3rd-party bundles put things
under the root.

Use a Bundle
~~~~~~~~~~~~

To use a bundle, you simply add the bundle file under the **dependency**
section of your project manifest:

.. code-block:: xml

    <dependency>
        <bundle name="foo.bundle" lpath="path/you/choose" />
    </dependency>

Now you can use packages in the bundle as if they were in your source directory:

.. code-block:: javascript

    import foo;

    @entry
    task main()
    {
        foo.func();
        exit(0);
    }

Types
`````

Thor has four kinds of types:

1. Primitive types

#. Enumeration_

#. Class_

#. Interface

Thor users can create their own types except primitive types.

Primitive Types
:::::::::::::::

Thor supports following primitive types:

- void
- bool
- int8
- int16
- int32
- int64
- float32
- float64

User can use those type directly without declaration.

.. code-block:: javascript

    var i : int32 = 0;
    var j = i * 2;

Enumeration
:::::::::::

Enumeration is for describing finite set of values. We use keyword ``enum``
to declare an enumeration, an ``enum``\'s body consists of identifiers
which are separated by commas. For example:

.. code-block:: javascript

    enum Gender {
        Male, Female
    }

    var gender = Gender.Male;
    switch(gender)
    {
    case Gender.Male:
        println("Male");
    case Gender.Female:
        println("Female");
    default:
        println("something went wrong!");
    }

Enum can be used in switch statement to provide better readability. For
example, it is a common practice to define error code with enum to make
error handling code clearer.

.. code-block:: javascript

    Gender.Male

In above example, both of ``Male`` and ``Female`` are **enumerator**\s of ``Gender``.
You can also explicit give enumerator an initialization value:

.. code-block:: javascript

    enum Weekday {
        Monday = 1, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    }

Each enumerators has values of previous' plus **1**, for example: ``Tuesday``
has value **2**, and  ``Wednesday`` has value **3**, etc. By default, the first
enumerator without initialization value has value **0**. Once you specify value
for enumerators, you have to be careful about it might cause duplicate switch
cases.

.. code-block:: javascript

    enum Color {
        Red = 1,
        Green = 0,
        Blue        // value is 1
    }

    var color = Color.Red;
    switch(color) {
    case Color.Red:
        println("Red");
    case Color.Green:
        println("Green");
    case Color.Blue:      // error! duplicate case value
        println("Blue");
    }

``enum`` can be **explicit** casted into integer for arithmetic operatings
(vise versa). For example, if today is ``Monday``, so what is the weekday
after **99** days?

.. code-block:: javascript

    var today = Weekday.Monday;
    var today_value = cast<int32>(today);

    var result_day_value = (today_value + 99) % 7;
    var result_day = cast<Weekday>(result_day_value);

    switch(result_day)
    {
    case Weekday.Monday:    println("Monday");
    case Weekday.Tuesday:   println("Tuesday");
    case Weekday.Wednesday: println("Wednesday");
    case Weekday.Thursday:  println("Thursday");
    case Weekday.Friday:    println("Friday");
    case Weekday.Saturday:  println("Saturday");
    case Weekday.Sunday:    println("Sundy");
    default:
        // handle domain errors here
    }

We get result by running above codes::

    Tuesday

Class
:::::

Declaration
~~~~~~~~~~~

A class declaration is beginning with keyword ``class``, followed by a class name
and then its body enclosed by braces:

.. code-block:: javascript

    class Client { } // a class named 'Client'

Create Instance
~~~~~~~~~~~~~~~

Thor users can create instances of class type by ``new`` operator:

.. code-block:: javascript

    var patrick: Client = new Client();
    var alice: Client = new Client();

Both of ``patrick`` and ``alice`` are **references** to ``Client`` instances
(themselves occupy memories. they are not instances but you can access instances
through them).

How to use the ``new`` operator? First, we append a class name after ``new``, and
a pair of parentheses. It's just like a function call after ``new``. (actually
it's, detail in later sections).

Data Member
~~~~~~~~~~~

Classes are abstraction for concepts/components in softwore design. For describing
a component's status or attribute, sometimes we need to give it own storage to keep
information. To do this, we can place variable declarations in class body:

.. code-block:: javascript

    class Client {
        var is_male: bool; // data member of class 'Client'
    }

Now we can know if a ``Client`` is male or not.

Member Accessibility
~~~~~~~~~~~~~~~~~~~~

But by default, class data members are inaccessible from outside. Class designers
can specify the accessibility of each data members(default is **protected**):

+---------------+-----------------------------------------------+
| Accessibility | Who can access                                |
+===============+===============================================+
| ``public``    | every pieces of codes                         |
+---------------+-----------------------------------------------+
| ``protected`` | in owner class' body and in subclasses' bodes |
+---------------+-----------------------------------------------+
| ``private``   | only in owner class                           |
+---------------+-----------------------------------------------+

To access an instance's data member, use syntax ``reference.data_member`` to do
things. For example, if we mark ``is_male`` as ``private``:

.. code-block:: javascript

    class Client {
        private var is_male: bool;
    }

    // create an instance and set it as male
    var i_can_do_nothing: Client = new Client();
    i_can_do_nothing.is_male = true; // error! 'is_male' is private

But if we mark it as ``public``:

.. code-block:: javascript

    class Client {
        public var is_male: bool;
    }

    var jerry: Client = new Client();
    jerry.is_male = true;  // fine
    jerry.is_male = false; // fine, but...

Member Function
~~~~~~~~~~~~~~~

Member functions are special functions which have implicit target ``this`` to
manipulate. The syntax to call member functions is similar to how we access
data members: ``reference.member_function()``. And in member functions, you
can use ``this`` to access the referenced instance, written before the dot sign.
Notice that ``this`` is a const reference, means you can't change the target
at runtime.

.. code-block:: javascript

    class Client {
        public var is_male: bool;
        public function changeSex(): void {
            this.is_male = !this.is_male; // this line is same as 'is_male = !is_male;'
                                          // 'this' can be omitted for convenience
        }
    }

    var marry: Client = new Client();
    marry.is_male = true; // marry is male at first, oops
    marry.changeSex();

    println("is marry now a male? \{marry.is_male}");

Special Member Function
~~~~~~~~~~~~~~~~~~~~~~~

There is a special member function named ``new``. The ``new`` member function
should return ``void`` and can be overloaded. ``new`` function is used to
perform initialization works because it starts an instance's life-cycle. When
the time apply ``new`` operator, we also call ``new`` function.

.. code-block:: javascript

    class Client {
        private var is_male: bool; // now this data member can't be modify from
                                   // outside directly

        public function new(male: bool): void {
            is_male = male; // only set value once here
        }

        public function isMale(): bool {
            return is_male;
        }
    }

    var john: Client = new Client(true);
    println("is john a male? " + john.isMale());

Member functions are like guards which can make sure any changes to
data members are under control. In above example, ``is_male`` is not
accessible except by member functions. It's more reasonable for a class
which models human being.

Inheritance
~~~~~~~~~~~

When we want to add some attributes or functionalities to a class. we can
extend from it by using keyword **extends**. For given class ``Point``:

.. code-block:: javascript

    class Point {
        private var x: int32;
        private var y: int32;

        public function new(the_x: int32, the_y: int32): void {
            x = the_x;
            y = the_y;
        }

        public function getX(): int32 { return x; }
        public function getY(): int32 { return y; }
    }

Now we want to let ``Point`` printable, then we create another class
``PrintablePoint``:

.. code-block:: javascript

    class PrintablePoint extends Point {

        public function new(the_x: int32, the_y: int32): void {
            super(the_x, the_y);
        }

        public function show(): void {
            for(var h = 0; h < super.getY(); ++h)
                println("");

                // in the line point locates at
                for(var w = 0; w < getX(); ++w)
                    print(" ");

                println("*");

            for(var h = getY() + 1; h < 10; ++h)
                println("");
        }
    }

Client codes look like following:

.. code-block:: javascript

    var point: PrintablePoint = new PrintablePoint(10, 4);
    println("x: " + point.getX() + ", y: " + point.getY());
    point.show();

    var raw_point: Point = point;
    raw_point.show(); // error! you can't find show() member function
                      // through a reference of 'Point'

There are some features we used in above code:

1. The ``super`` function: to ``new`` a ``Point`` instance, we have to
   pass two arguments to ``new`` function for specifying its location.
   ``PrintablePoint`` is extended from ``Point`` so it should know how to
   initialize the ``Point`` part within it: call the ``new`` function. In
   Thor, the only way to initialize extended class(or super class,
   base class) in a new extending class(or subclass, derived class) is
   to call ``super`` function and give it necessary arguments. At this
   moment, ``super`` is equivalent to super class' ``new`` function.

#. The ``super`` reference: subclass can access all members in super class
   except some with ``private`` accessibility. You can write as
   ``super.data_member`` to do things, just like using ``this`` reference.
   ``super`` reference can also be omitted in codes.

#. Type of reference: you can manipulate an instance by references, but
   also be limited by reference its type. Reference is protocol which
   formulates names you can access, written in the class body. A super
   class reference can point to a subclass instance, because super class
   has more restrictions than subclasses.

#. class can inherit only one super class.

Virtual Member Function
~~~~~~~~~~~~~~~~~~~~~~~

For member functions which are intent to be overridden behaviors by subclasses,
you can add ``virtual`` specifier before keyword ``function``. Let's look
into this feature by comparing two examples:

.. code-block:: javascript

    class Rabbit {
        // non-virtual function
        public function hello(): void {
            println("Hello! I'm a rabbit.");
        }
    }

    // extends from 'Rabbit' and try to override its function
    class FlyableRabbit extends Rabbit {
        public function hello(): void {
            println("Hello! I'm a rabbit, I can fly!");
        }
    }

    var rabbit: Rabbit = new FlyableRabbit();
    rabbit.hello();

    var flyable_rabbit: FlyableRabbit = new FlyableRabbit();
    flyable_rabbit.hello();

Output::

    Hello! I'm a rabbit.
    Hello! I'm a rabbit, I can fly!

In above example, the first declaration
``var rabbit: Rabbit = new FlyableRabbit();`` creates a reference with
class ``Rabbit`` and it points to a ``FlyableRabbit`` instance. By the
reference, we can only see members in ``Rabbit``, so no accidents we
got a non-flyable hello response. Then see what will happen after adding
``virtual`` specifier to the ``hello()``:

.. code-block:: javascript

    class Rabbit {
        // now hello is a virtual function
        public virtual function hello(): void {
            println("Hello! I'm a rabbit.");
        }
    }

Output::

    Hello! I'm a rabbit, I can fly!
    Hello! I'm a rabbit, I can fly!

We do the same, call ``hello()`` via a ``Rabbit`` reference.
But when compiler sees the ``virtual`` version ``hello()`` this time,
it knows that maybe this ``hello()`` was overridden by ``Rabbit``\'s
subclasses. So compiler will ask the pointed instance if it has an
overridden version ``hello()`` then call the new version, otherwise,
call ``Rabbit.hello()``.

Notice that users can create instances of class only if the class had
overridden all the inherited member functions from its super class and
interfaces.

Interface
:::::::::

Interface is like a protocol which formulates the ways user can operate
on instances. Interface is similar to class but it does not provide
function implementations as class does.

.. code-block:: javascript

    interface Flyable {
        public function fly(): void; // end with semicolon, no function body

        var i: int32;         // error! can't declare data members here
    }

As shown above, you are not allowed to declare data members in interface.

Implement Interface
~~~~~~~~~~~~~~~~~~~

Interface is like a class, but all its member functions are virtual.
Class extends interface by keyword ``implements``, unlike the keyword
``extends``, a class can implement more than one interfaces.

.. code-block:: javascript

    class FlyableFoo implements Flyable {
        // does not implement the member function 'fly()'
    }

    class FlyableDog implements Flyable {
        public function fly(): void { // have a body
            println("I'm a dog. I'm flying!");
        }
    }

    var foo: Flyable = new FlyableFoo(); // error! 'FlyableFoo' has an
                                         // unimplemented member function 
    var dog: Flyable = new FlyableDog();
    dog.fly();

Output::

    I'm a dog. I'm flying!

Extend Interface
~~~~~~~~~~~~~~~~

Interface is designed to be ``implement``\ed by classes, but it can also
``extend`` from other interfaces to collect virtual member functions.
Back to our example, now we create a new interface ``Swimmable`` and
then write a ``Flyable`` and ``Swimmable`` class ``Duck``.

.. code-block:: javascript

    // top level interface
    interface Swimmable {
        public function swim(): void;
    }

    // secondary level interface
    interface FlyableSwimmable extend Flyable, Swimmable {
        // no extra interfaces, but there are
        // still fly() and swim() to implement
    }

    class Duck implements FlyableSwimmable {
        public function fly(): void {  // implement function from 'Flyable'
            println("I'm duck, I'm flying!");
        }

        public function swim(): void { // implement function from 'Swimmable'
            println("I'm duck, I'm swimming!");
        }
    }

    var animal: FlyableSwimmable = new Duck();
    animal.fly();
    animal.swim();

Output::

    I'm duck, I'm flying!
    I'm duck, I'm swimming!

cast - Casting Between Different Type
:::::::::::::::::::::::::::::::::::::

Thor is strong typing. You have to ``cast`` between different types,
there ``explicit cast`` and ``implicit cast``. Implicit cast is done by
compiler automatically. Explicit cast is specified by user via ``cast<>``
template, for example:

.. code-block:: javascript

    var i : int32 = 0;
    var d : float64 = 3.14;
    i = d; // implicit cast, warning, precision loss
    i = cast<int32>(d); // explicit cast, ok

isa - Dynamic Type Checking
:::::::::::::::::::::::::::

``isa`` is used to check object's instance type at runtime:

.. code-block:: javascript

    class Animal {
        public function wink() : void {
            if(isa<Fish>(this)) {
                println("Fish has no eyelid");
            } else {
                println("winked");
            }
        }
    }

    class Fish extends Animal { }
    class Dog extends Animal { }

    @entry task test_main() : void {
        var a : Animal = new Fish();
        var b : Animal = new Dog();
        a.wink();
        b.wink();
        exit(0);
    }

output::

    Fish has no eyelid
    winked

Variable and Types
``````````````````

Thor declares variables with ``var name : type = init_expr;``. for
example:

.. code-block:: javascript

    var i : int32 = 0;

There is no need to indicate variable type, compiler will decide its
type from right-hand-side expression in declarations and assignments:

.. code-block:: javascript

    var i = 0;  // same as above. integer literals have type 'int32'

For more detail, see `Type Inference`_

Variable vs Const
:::::::::::::::::

Thor declares variable with keyword ``var``:

.. code-block:: javascript

    var a = 4;
    print(a);  // print 4
    a += 1;
    print(a);  // print 5

Declare constants with keyword ``const``:

.. code-block:: javascript

    const b = 7;
    b = b + 1;  // compile error, b can not be modified

Notice the syntax here is just ``const``, neigher ``const var`` nor ``var const``.

Names
:::::

Language components which can be declared all have their names, ex: variables, functions.
In code sections, we have to specify names to tell compiler which variable to assign,
which to call, etc. A name is introduced into rest scopes once users finish its declaration.
And the names are only kept in the scope where they are declared in, ex: package, class
body, for loop, etc.

.. code-block:: javascript

    var x : int32;   // declares 'x', introduce name 'x'
    var y : float64; // declares 'y', introduce name 'y'

    var x : float32; // error! the name 'x' has been used to declare something

The following is a more complex example which shows how the class name and function name
affect others'.

.. code-block:: javascript

    class Foo {
        public function f(): void { } // declares 'f', introduce name 'f'
        public var i: int32;          // declares 'i', introduce name 'i'

        public var f: float32;        // error! 'f' has been used to declare a function
    }

    var i: Foo = null;     // okay, 'i' can be used by an other declaration now
                           // and the class 'Foo' is already declared

    var f: float32 = 0.0;  // okay, 'f' is reusable

    function h(i: int64) { // error! in package function 'h' is declared, name 'i' was used
                           // and it's a reference to 'Foo' object

        for(var c = 0; c < 10; ++c){
            var f = lambda(): void { }; // error! 'f' is already a float32 variable
        }
    }

Array
:::::

Thor creates arrays by using ``array-literal``\ s, they are formed by syntax ``[ ... ]``,
for example:

.. code-block:: javascript

    var a : Array<int32> = [1, 2, 3];

You can also let compiler deducts the type for you:

.. code-block:: javascript

    var ia = [1, 2, 3];     // 'ia' is an Array<int32>, same type as 'a'
    var fa = [1, 2, 3.14];  // 'fa' is an Array<float64>, floating point literals have type 'float64'
                            // and other integer literals are promoted to 'float64'

You can explicit specify the type of the elements of the array:

.. code-block:: javascript

    var fa2 = <float64>[1, 2, 3];  // 'fa2' is Array<float64>

How about multiple dimension array? you can also create them by using
nested brackets:

.. code-block:: javascript

    var mda = [[1, 2, 3], [4, 5, 6]];  // 'mda' is Array2D<int32>

Thor support up to ``Array11D``. What if you need more than 11 dimension?
What? Why would someone need a 12 dimensioin array? There must be something
wrong with your design, please check it again.

**How to access elements in Array**

Array elements are indexed by integers. For example, a *3*-sized array are indexed from integer *0* to *2*,
a *N*-sized array's indices are in range [0, *N*). You can get array size by calling ``size()``.

.. code-block:: javascript

    @entry
    task test_main(): void
    {
        var v = [3, 2, 1];

        for(var i = 0; i < v.size(); ++i)
            println("v[" + i + "] is: " + v[i]);

        exit(0);
    }

Output::

    v[0] is: 3
    v[1] is: 2
    v[2] is: 1

**Array Syntax Sugar**

The ``[]`` syntax above is a shorthand for calling ``get()`` and ``set()``. There are
two usages when appending ``[]`` after a variable: (1) for **read** and (2) for **write**.

For read, ``v[i]`` is equivalent to function call ``v.get(i)``. For write, ``v[i] = 99``
is equivalent to call ``v.set(i, 99)``. You can not only use the shorthand on arrays, all
user-defined types which provide one-parameter version ``get()`` and two-parameter version
``set()`` share this benefit too. We do not support shorthand for multiple dimension arrays
yet, so users have to call ``set()`` and ``get()`` by hand.

String
::::::

.. TODO: We need to discuss which operations we do support for thor.lang.String)

A string literal is defined with a sequence of characters enclosed by double quote mark.

.. code-block:: javascript

    var str = "hello string";

Every string literal is an instance of the embedding class **String**.

.. code-block:: javascript

    var str : String = "hello string";

Since it is an object type, if you want to check if two strings have the same content,
you need to use ``isEqual()`` instead of operator ``==``.

.. code-block:: javascript

    println("foo".isEqual("foo")); // true
    println("foo" == "foo");       // false

You may concate strings to strings or strings to primitive types with ``+`` operator.

.. code-block:: javascript

    "aaa" + "bbb" // aaabbb
    "aaa" + 1     // aaa1
    "aaa" + 3.1   // aaa3.1

If you need a substring, there is ``substring()`` method.

.. code-block:: javascript

    "0123456".substring(3);   // 3456
    "0123456".substring(2,4); // 23

For those object types, you may supply an ``toString()`` method in help of interacting with
strings.

.. code-block:: javascript

    class Foo
    {
        public function toString() : String
        {
            return "foofoo";
        }
    }

    task test_str()
    {
        var foo = new Foo();
        println("aaa \{foo}");      // print aaa foofoo
        println("bbb " + "\{foo}"); // print bbb foofoo

        exit(0);
    }

As the above code shown, if you write a string literal as:

.. code-block:: javascript

    "some other text \{variable name}"

It's a syntax sugar for:

.. code-block:: javascript

    "some other text " + <variable name>.toString()

For a complete list of available operations, please refer
to the :ref:`String section of system API reference <String>`.

Control Structure
`````````````````

for
:::

A statement for repeat doing something continously until the program can't
meet specific condition.

For loop contains three parts in its header: (1) initialization block
(2) condition (3) action block, be written as:

.. code-block:: javascript

    for( init-block; condition; action-block ) {
       // ...
    }

You can declare variables in initialization block and do something in
action block, both accept only one statement. Only one boolean expression
is allowed in (2). following is a simple usage:

.. code-block:: javascript

    // loop and indexing elements, i has type 'int32' because of integer literal '0'
    var arr = [1, 2, 3];
    for(var i = 0; i < arr.size(); ++i) {
        print(arr[i]);
    }

foreach
:::::::

Foreach is used as a more readable equivalent to the traditional for_ loop
operating over a collection of values, such as all elements in a container.

**Syntax**

.. code-block:: javascript

    for(var VAR in CONTAINER) {
        // statements here
    }

In shown codes, *VAR* represents copy of element in each iterations, And
*CONTAINER* stands for container which we want to go through.(*VAR* and
*CONTAINER* are for exposition only).

**Explanation**

The above syntax produces code similar to the following:

.. code-block:: javascript

    {
        var it = CONTAINER.iterator();
        for( ; it.hasNext(); ) {
            var VAR = it.next();
            // statements here
        }
    }

**Example**

.. code-block:: javascript

    // i has type 'float64' because of arr is an 'Array<float64>'
    var arr = [1.0, 2.0, 3.0];
    for(var i in arr) {
        print(i);
    }

**How to run foreach on your own container**

First, your container have to provide a method named ``iterator``, ``iterator()``
returns an object which also have public interfaces ``hasNext()`` and ``next()`` which can
be called from outside. Following discusses about the responsibility of mentioned methods.

- ``iterator()`` Returns an instance of user-defined class, which can navigate elements in
  your container.

- ``hasNext()`` A predication method to test whether there are any elements left in
  container which have not been visited by iterator.

- ``next()`` Get the next element from container. Of course, you can decide the return type
  by your own.

switch
::::::

Statements in Thor ``switch`` will not fall-through, so users do **not** have to add
``break`` at the end of cases:

.. code-block:: javascript

    enum Color {
        RED,
        GREEN,
        BLUE
    }

    @entry
    task test_main() {
        var c = Color.RED;
        // will print 'RED'
        switch(c) {
            case Color.RED:
                print("RED");
            case Color.GREEN:
                print("GREEN");
            case Color.BLUE:
                print("BLUE");
            default:
                print("default");
        }
        exit(0);
    }

while
:::::

``While`` is similar to for_ but without the initialization block
and action block

.. code-block:: javascript

    @entry
    task test_main() {
        var i = 0;
        while(i != 10) {
            print(i);
            ++i;
        }
        exit(0);
    }

Output::

    0123456789

do-while
::::::::

Similar to while_ loop, but ``do-while`` execute codes in its body first, then test the condition

.. code-block:: javascript

    @entry
    task test_main() {
        var i = 0;
        do {
            print(i);
            ++i;
        } while (i != 10); // don't forget to add this semicolon
        exit(0);
    }

Output::

    0123456789

Class and Interface
```````````````````

Thor supports encapsulation:

.. code-block:: javascript

    import thor.math;

    class Point2D {
        public function new(x:float64, y:float64)
        {
            x_ = x;
            y_ = y;
        }

        public function norm()
        {
            return thor.math.sqrt(x_ * x_ + y_ * y_);
        }

        private var x_ : float64 = 0.0;
        private var y_ : float64 = 0.0;
    }

    @entry
    task test_main() {
        var p = new Point2D(3.0, 4.0);
        print(p.norm());  // print 5
        exit(0);
    }

Enum
::::

Thor ``enum`` is strong-typed, which means you can not assign between
enum value and integer directly. To operate an enum with other enum or integer,
you have to cast explicitly:

.. code-block:: javascript

    enum Color {
        Red,
        Green,
        Blue
    }

    @entry
    task test_main() : void {
        var c = Color.Red;

        var i = 2;
        i = Color.Blue;               // Error, need explicit casting
        i = cast<int32>(Color.Blue);  // OK

        exit(0);
    }

Type Alias (typedef)
::::::::::::::::::::

You can create alias for type with keyword ``typedef``:

.. code-block:: javascript

    import thor.container;

    typedef thor.container.Vector<int32> IntVec;

    @entry
    task test_main() {
        var v = new IntVec();
        v.push_back(2); v.push_back(3);

        for(var i in v) { // print 23
            print(i);
        }
        exit(0);
    }

Type Inference
``````````````

Thor supports type deduction. Compiler can deduct the actual type of
``local variables`` and ``functions``\(not includes class data members and
global variables). For example:

.. code-block:: javascript

    var global_var = 123; // error

    class Foo {
        var data_member = 456; // error
    }

    function f() {
        var i = 0;          // i has type 'int32', integer literals, which was used to
                            // initialize i, has type 'int32'
        var pi = i + 3.14;  // pi has type 'float64', the result type of expression
                            // (i + 3.14) was promoted to 'float64'

        i = 2.71;           // error, right-hand-side expression should have type 'int32',
                            // same in every other statements
    }

    function g() { // okay, g() returns 'float64'
        return 3.14;
    }

    function h() { // error, all returned expressions should have the same type
        return 1;
        return 2.3;
    }

Notice that if there are multiple ``return`` statements in function, then all the type
of returned expressions should be identical.

Lambda
``````
Usage
:::::

Thor supports creating lambda function by keyword ``lambda``, lambda functions
are callable objects which can carry values and strategies. The different between
lambda functions and other functions are: (1) they are ``expressions`` which create objects.
(2) you must specify their return types(compiler can not deduct it for you cause some
implementation issues).

.. code-block:: javascript

    var is_less = lambda(lhs:int32, rhs:int32) : bool {
        return lhs < rhs;
    }; // don't forget semicolon at the end

    print(is_less(3, 4));  // print true

Lambda can be passed as an argument to functions:

.. code-block:: javascript

    // the type of parameter must matches created objects
    function apply(func : lambda(int32, int32):bool) {
                        //^^^^^^^^^^^^^^^^^^^^^^^^^ here is a lambda type
        print(func(3, 4)); // print false
    }

    @entry
    task test_main() {
        // has same type as 'is_less'
        var is_greater = lambda(lhs:int32, rhs:int32) : bool {
            return lhs > rhs;
        };

        apply(is_greater);
        exit(0);
    }

Capture
:::::::

Lambdas can use variables from outside, they ``capture`` used variables
and keep their own copies in internal storage.

.. code-block:: javascript

    class Foo {
        public var value: int32 = 0;

        public function inc(): void {
            ++value;
        }
    }

    class Tester {

        private var data_member: int32 = 0;

        public function run() {
            // try to change local variable's value, capture the value
            var local_variable = 0;
            var change_local = lambda(): void {
                ++local_variable;
            };
            change_local();
            print("after change local: \{local_variable}\n");

            // try to change data member's value, capture reference 'this'
            var change_data = lambda(): void {
                ++data_member; // same as ++this.data_member;
            };
            change_data();
            print("after change data: \{data_member}\n");

            // try to change object's value, capture reference 'foo'
            var foo = new Foo();
            var change_obj = lambda(): void {
                foo.inc();
            };
            change_obj();
            print("after change obj: \{foo.value}\n");
        }
    }

    @entry
    task test_main() {
        var tester = new Tester();
        tester.run();
        exit(0);
    }

Template
````````

Thor support template. Not like syntax sugar in Java, template in
Thor generates and optimizes different codes according to the type arguments,
can truly improve the performance.

Function Template
:::::::::::::::::

.. code-block:: javascript

    function max<T>(a:T, b:T) : T {
        return a > b ? a : b;
    }

    @entry
    task test_main() : void {
        max(2, 3);        // call max<int32> version
        max(3.14, 2.71);  // call max<float64> version
        exit(0);
    }

User can also explicit call specific version, so some possible implicit conversions are needed:

.. code-block:: javascript

    max<float64>(2, 3);  // call max<float64>

But we have to say, currently, Thor can't generate proper interface for function templates
because of C++ compatibility issues.

Class Template
::::::::::::::

Thor also support class template:

.. code-block:: javascript

    class Foo<T> { }

    @entry
    task test_main() : void {
        var fooi32 = new Foo<int32>();
        var foofooi32 = new Foo<Foo<int32> >();
        exit(0);
    }

We have not solve the **double >>** problem yet, so you have to put a whitespace
between the ``>>``.

Template Specialization
:::::::::::::::::::::::

Thor support specialization:

.. code-block:: javascript

    // general version
    function max<T>(a:T, b:T) : T {
        print("general version\n");
        return (a > b ? a : b);
    }

    function log() {
        // do something here
        print("open log file...\n");
        print("write something into log file...\n");
        print("close log file...\n");
    }

    // specialization for 'float64'
    function max<T:float64>(a:T, b:T) : T {
        print("specialized version\n");
        log();
        return (a > b ? a : b);
    }

    @entry
    task test_main() {
        max(2, 3);        // call general version
        max(3.14, 2.71);  // call specialized version
        exit(0);
    }

Operator Overloading
````````````````````

Thor supports operator overloading, users can customize the operator
behavior for their own classes(without changing their precedences and associativities):

.. code-block:: javascript

    class Foo {
        public function __lt__(rhs: Foo) : bool {
            print("Foo.operator<\n");
            return true;
        }
    }

    @entry
    task test_main() {
        var fa = new Foo();
        var fb = new Foo();
        fa < fb;  // call the Foo.__lt__(Foo) function
        exit(0);
    }

Output::

    Foo.operator<

These is the list of overloadable binary operators:

+----------------+-------------------+
| Binary operator| function name     |
+================+===================+
| ``+``          | ``__add__``       |
+----------------+-------------------+
| ``-``          | ``__sub__``       |
+----------------+-------------------+
| ``*``          | ``__mul__``       |
+----------------+-------------------+
| ``/``          | ``__div__``       |
+----------------+-------------------+
| ``%``          | ``__mod__``       |
+----------------+-------------------+
| ``&``          | ``__and__``       |
+----------------+-------------------+
| ``|``          | ``__or__``        |
+----------------+-------------------+
| ``^``          | ``__xor__``       |
+----------------+-------------------+
| ``<<``         | ``__lshift__``    |
+----------------+-------------------+
| ``>>``         | ``__rshift__``    |
+----------------+-------------------+
| ``&&``         | ``__land__``      |
+----------------+-------------------+
| ``||``         | ``__lor__``       |
+----------------+-------------------+
| ``<``          | ``__lt__``        |
+----------------+-------------------+
| ``>=``         | ``__le__``        |
+----------------+-------------------+
| ``>``          | ``__gt__``        |
+----------------+-------------------+
| ``>=``         | ``__ge__``        |
+----------------+-------------------+
| ``+=``         | ``__iadd__``      |
+----------------+-------------------+
| ``-=``         | ``__isub__``      |
+----------------+-------------------+
| ``*=``         | ``__imul__``      |
+----------------+-------------------+
| ``/=``         | ``__idiv__``      |
+----------------+-------------------+
| ``%=``         | ``__imod__``      |
+----------------+-------------------+
| ``&=``         | ``__iand__``      |
+----------------+-------------------+
| ``|=``         | ``__ior__``       |
+----------------+-------------------+
| ``^=``         | ``__ixor__``      |
+----------------+-------------------+
| ``<<=``        | ``__ilshift__``   |
+----------------+-------------------+
| ``>>=``        | ``__irshift__``   |
+----------------+-------------------+

Following are overloadable unary operators:

+----------------+-------------------+
| Unary operator | function name     |
+================+===================+
| ``++``         | ``__inc__``       |
+----------------+-------------------+
| ``--``         | ``__dec__``       |
+----------------+-------------------+
| ``~``          | ``__invert__``    |
+----------------+-------------------+
| ``!``          | ``__not__``       |
+----------------+-------------------+
| ``-``          | ``__neg__``       |
+----------------+-------------------+

We don't allow overloading operator ``=``, ``==`` and ``!=`` because it will ruin reference
semantics.

Concurrency and Parallelism
---------------------------

Exeuction Model
```````````````

Before looking into Thor language features for concurrency and
parallelism, let us show you the underlying execution model since it's
different from other popular languages.

Thor executes code as the following:

.. digraph:: flow

    start    [label = "start VM"];
    exit     [label = "exit VM"];
    batch    [label = "execute functions in batch"];
    execute  [label = "execute foo(), bar(), and ..."];
    returned [label = "all returned"];

    start    -> batch   ;
    batch    -> execute ;
    execute  -> returned;
    returned -> exit   [label = "request to exit"];
    returned -> batch  [label = "not request to exit"];

As you see, functions are grouped before execution, execute them in a batch,
then wait until **ALL** of them returned. Then the next batch is triggered.

There are several points in this design:

#. Executions of asynchronous functions are grouped as batch.
#. VM will wait for all asynchronous functions returned for next batch.

This is designed to act as this, in order to execute asynchronous functions in a
more efficient way. Under this behavior, writing infinite loop or long-run
function will **BLOCK** new asynchronous functions. So users should write
function smaller, and use asynchronous functions for long-run tasks.

Note that "request to exit" means ```thor.lang.exit()`` is called.

Async
`````

``Async`` is used to execute function or code block asynchronously. For
example, in the following code:

.. code-block:: javascript

    function callee(): void {
        print("this is callee\n");
    }

    function caller(): void {
        print("begin of caller\n");
        async -> callee();
        print("end of caller\n");
    }

``caller`` will not be blocked by the execution of ``callee`` since the
execution of ``callee`` is asynchronous.

For more detailed execution sequence, ``caller`` should be executed after
``callee`` has done its job. The reason is Thor runtime will try to
execute asyncs in a more efficient way by collecting all of them (not only the
one in ``caller``).

.. digraph:: flow

    node[fontname = "monospace"];
    "caller()" [shape="record"];
    "callee()" [shape="record"];
    "caller()" -> "callee()";

**Async** has *implicit* and *explicit* versions. Let us take a look on the
following code:

.. code-block:: javascript

    function callee(): int32 { return 1234; }
    function caller(): void {
        async ->   callee();   // implicit async
        async -> { callee(); } // explicit async
    }

As you see, there is no "{" and "}" in *implicit* one. *implicit* one is
focused on *single* function/task call, in order to provide better
performance. So the syntax for this is restricted to single function call,
with assignment optionally:

.. code-block:: javascript

    var variable: int32 = 0;

    function callee(): int32 { return 1234; }

    function caller(): void {
        async ->            callee();
        async -> variable = callee();
    }

The first async in ``caller`` is simple, simply calls ``callee``
asynchronously. But second async does the same job of the first one **AND**
stores the result of ``callee`` to ``variable``.

Note that ``variable`` is not allowed to be local non-static, because local
non-static variable will run out of scope before ``callee()`` returns.

Additionally, **ALL** expressions is evaluated at ``caller`` in *implicit*
async. That is, in the following example:

.. code-block:: javascript

    function max(a: int32, b: int32): int32 { return a > b ? a : b; }
    function callee(value: int32): void { print("this is callee: \{value}\n"); }
    function caller(): void {
        async -> callee(max(1, 2));
    }

``max(1, 2)`` is called in ``caller``, then execute ``callee`` with result
asynchronously.

User may need to write more complicated logic in single async by reasons,
then you need the *explicit* one. For example:

.. code-block:: javascript

    function min(a: int32, b: int32): int32 { return a > b ? b : a; }
    function max(a: int32, b: int32): int32 { return a > b ? a : b; }
    function caller(a: int32, b: int32): void {
        async -> {
            var the_min = min(a, b);
            var the_max = max(a, b);

            println("min = \{the_min}, max = \{the_max}\n");
        }
    }

Then the whole block will act as implicit function, variables are captured with
the same rule of lambda. Actually, you can image the following is the
equivalent as the previous one:

.. code-block:: javascript

    function min(a: int32, b: int32): int32 { return a > b ? b : a; }
    function max(a: int32, b: int32): int32 { return a > b ? a : b; }
    function callee(a: int32, b: int32): void {
        var the_min = min(a, b);
        var the_max = max(a, b);

        println("min = \{the_min}, max = \{the_max}\n");
    }
    function caller(a: int32, b: int32): void {
        async -> callee(a, b);
    }

But for **tasks**, you are able to execute them through *implicit* version
**ONLY**, due to limitation of **task**.

Flow
````

``Flow`` is used to execute several tasks parallelly, and wait them all
finished. For example:

.. code-block:: javascript

    function a() : void {}
    function z() : void {}
    function foo() : void { }
    function bar() : void { }

    @entry task test_main() {
        a();
        flow -> {
            foo();
            bar();
        }
        z();
    }

Will executed as follow:

.. digraph:: flow

    node[fontname = "monospace"];
    "a()" [shape="record"];
    "z()" [shape="record"];
    "foo()" [shape="record"];
    "bar()" [shape="record"];
    "a()" -> "foo()";
    "a()" -> "bar()";
    "foo()" -> "z()";
    "bar()" -> "z()";

``z()`` will be called only after both ``foo()`` and ``bar()`` are finished.

Note that ``flow`` could be placed in **tasks** only. This is the only
difference between **function** and **task**.

Transactional Memory
````````````````````

Thor support *Software Transactional Memory* to easy parallel
programming, with key word ``atomic``, user can construct an **atomic-block**,
which statements in it are excuted like atomicly. It Frees developer from the
problem of racing condition:

.. code-block:: javascript

    var gi32 : int32 = 0;
    var hello1_finished : bool = false;
    var hello2_finished : bool = false;

    function hello1() : void
    {
        for(var i = 0; i < 100000000; ++i)
        {
            // atomic block
            atomic -> {
                ++gi32;
                --gi32;
            }
        }
        hello1_finished = true;
        return;
    }

    function hello2() : void
    {
        for(var i = 0; i < 100000000; ++i)
        {
            // atomic block
            atomic -> {
                ++gi32;
                --gi32;
            }
        }
        hello2_finished = true;
        return;
    }

    // run following entry with command: tsc r test_main --domain=mt
    @entry
    task test_main() : void {
        flow -> {
            hello1();
            hello2();
        }
        print("gi32 = \{gi32}\n");  // print "0"
        exit(0);
    }

For implementation issue, function calls are not allowed in atomic-block.

Distributed Domain
------------------

In Thor, ``Domain`` means targets response for execution. Targets could be
run on every supported platform and could communicate with each other by
network connection.

Remote Invocation
`````````````````

For distributed domain, Thor provide functionality to execute functions on
other targets.

You can run ``foo()`` on caller's domain with the following lines.

.. code-block:: javascript

    var target_to_execute: Domain = Domain.caller();
    async[target_to_execute] -> foo();

Here are several points in the previous code:

#. ``Domain.caller()`` will return a domain which represents caller of
   **async execution**.
#. ``foo()`` will be executed on domain specified by ``target_to_execute``.

Then, ``foo()`` will be executed on caller's domain.

With this, following demonstrates a echo server with domain. First, provide
functions should be executed on server/client:

.. code-block:: javascript

    // run on server (sent by client) function
    function hello_server(msg: String) : void {
        print("get message from client: \{msg}\n");

        async[Domain.caller()] -> hello_client(msg);
    }

    // run on client (sent by server)
    function hello_client(msg: String) : void {
        print("get message from server: \{msg}\n");

        async -> exit(0);
    }

Then, provide entries for server and client:

.. code-block:: javascript

    @entry task server_entry() : void {
        Domain.listen(
            "tcp://*:1234",
            null, // on     connected callback
            null, // on dis-connected callback
            lambda(id: thor.util.UUID, e: DomainError) : void {
                if (e != DomainError.OK)
                    exit(1);
            } // on error callback
        );
    }

    @entry task client_entry() : void {
        Domain.connect(
            "tcp://localhost:1234",
            lambda(id: thor.util.UUID, d: Domain) : void {
                async[d] -> hello_server("hello~~~");
            },    // on     connected callback
            null, // on dis-connected callback
            lambda(id: thor.util.UUID, e: DomainError) : void {
                if (e != DomainError.OK)
                    exit(1);
            } // on error callback
        );
    }

Note: ``thor.util.UUID`` is used, do not forget to import it.

Finally, build and run client and server separately:

.. code-block:: sh

    [project-dir]$ mkdir on_server on_client
    [project-dir]$ cp -r src *.xml on_server
    [project-dir]$ cp -r src *.xml on_client
    [project-dir]$ sh -c 'cd on_server && tsc build release'
    [project-dir]$ sh -c 'cd on_client && tsc build release'
    [project-dir]$ sh -c 'cd on_server && tsc r server_entry' &
    [project-dir]$ sh -c 'sleep 3 && cd on_client && tsc r client_entry'
    [project-dir]$ kill $!

Then the following lines should be in console output:

.. code-block:: text

    get message from client: hello~~~
    get message from server: hello~~~

Note: In this example, we build project twice since project is JIT (will compile
on execution). If you run twice in the same folder, this will cause problems.


Object Replication
``````````````````

In order to send data to remote target, Thor implements basic object
replication service. That is, object is deep copied to remote target, and there
is no any relationship between copied and origin ones.

For the following code:

.. code-block:: javascript

    async[target] -> foo(i32, obj);

Thor will dump the content of ``i32`` and ``obj``, send it to target, then
restore to objects (or primitive value) on target. Following graph shows the
execution sequence of ``foo(i32, obj)``.

.. digraph:: flow

    node[fontname = "monospace"];
    "serialize i32 and obj" [shape="record"];
    "send content to target" [shape="record"];
    "deserialize i32 and obj" [shape="record"];
    "foo(i32, obj)" [shape="record"];
    "serialize i32 and obj" -> "send content to target";
    "send content to target" -> "deserialize i32 and obj";
    "deserialize i32 and obj" -> "foo(i32, obj)" ;

Primitive types and built-in classes which support object replication could be
passed to remote target, or you'll see no action on remote.

For user defined class, you could mark it by ``@replicable`` to enable object
replication on it. If it's not a native class, then you should have done the
work. For native class, it's a bit more complicated since Thor has no
knowledge of native implementation. Please take a look at customizing object
replication.

Note, object replication requires default constructor by design. If there is no
default constructor, you may add one with private accessibility for object
replication only.

Customizing Object Replication
``````````````````````````````

In some case, you will need to control how object replication works. For
example, part of members should not be replicated or there is native
implementation Thor do not understand.

Actually, Thor implies three functions for object replication. For class
named ``Foo``, Thor will generate the following functions:

#. ``public static function __create():thor.lang.Object``
#. ``protected static serialize(ReplicationEncoder, Foo):bool``
#. ``protected static deserialize(ReplicationDecoder, Foo):bool``

``__create`` is required since Thor will have no knowledge of user
implementation. Object replication will use this to create a default
constructed instance of ``Foo``. As the name of ``serialize`` and
``deserialize``, they serialize/deserialize data of ``Foo``. Note that they
takes a existing instance of ``Foo``, such instance is the target to be
serialized in ``serialize``; deserialized to otherwise (default constructed).

In most of the cases, overriding ``serialize`` and ``deserialize`` should work.
For example, ``Foo`` may contain two member but only one should be replicated:

.. code-block:: javascript

    @replicable
    class Foo {
        private var    need_replication: int32;
        private var no_need_replication: int32;

        // other members..
    }

In order to serialize/deserialize ``values``, you could define customized
serialize/deserialize:

.. code-block:: javascript

    @replicable
    class Foo {
        protected static function serialize(encoder:ReplicationEncoder, instance:Foo):bool {
            if (!Object.serialize(encoder, instance))
                return false;

            if (!encoder.put(instance.need_replication))
                return false;

            return true;
        }

        protected static function deserialize(decoder:ReplicationDecoder, instance:Foo):bool {
            if (!Object.deserialize(decoder, instance))
                return false;

            var is_success: bool = false;

            is_success, instance.need_replication = decoder.get(instance.need_replication);
            if (!is_success)
                return false;

            return true;
        }

        private var    need_replication: int32;
        private var no_need_replication: int32;

        // .. other members
    }

There are several points in the previous code:

#. You need to pass serialize/deserialize to parent class first.
#. Reject serialize/deserialize by returning `false` on error. Or you may get
   garbage at remote.
#. ``ReplicationEncoder.put()`` determines result type by single argument,
   argument value is not used.

In some cases, you may need object replication on native classes. For example,
user defined array class ``Arr4``. Then you could declare serialize/deserialize
in Thor:

.. code-block:: javascript

    import thor.unmanaged;

    @native
    @replicable
    class Arr4 {
        // other members (ctor, dtor, etc...)

        @native protected static function   serialize(encoder:ReplicationEncoder, instance:Arr4):bool;
        @native protected static function deserialize(decoder:ReplicationDecoder, instance:Arr4):bool;

        private var data:thor.unmanaged.ptr_<int8>; // reserve memory space for native pointer in Thor
    }

Then implemenet it in C++ to control how replication work in native code:

.. code-block:: c++

    #include <array>   // std::array
    #include <utility> // std::pair

    #include <thor/PrimitiveTypes.h>
    #include <thor/lang/Language.h>
    #include <thor/lang/Replication.h>

    class Arr4 : public ::thor::lang::Object {
    public:
        // other members (ctor, dtor, etc...)

    protected:
        static bool serialize(::thor::lang::ReplicationEncoder* encoder, Arr4* instance) {
            if (!::thor::lang::Object::serialize(encoder, instance))
                return false;

            const std::array<int8, 4>& data_ref = *instance->data;

            return  encoder->put(data_ref[0]) &&
                    encoder->put(data_ref[1]) &&
                    encoder->put(data_ref[2]) &&
                    encoder->put(data_ref[3]);
        }

        static bool deserialize(::thor::lang::ReplicationDecoder* decoder, Arr4* instance) {
            if (!::thor::lang::Object::deserialize(decoder, instance))
                return false;

            for (int8& value : *instance->data)
            {
                const std::pair<bool, int8> result    = decoder->nativeGetInt8();
                const bool                  is_found  = result.first;
                const int8                  got_value = result.second;

                if (!is_found)
                    return false;

                value = got_value;
            }

            return true;
        }

    private:
        std::array<int8, 4>* data;
    };

Most of parts is the same as Thor version, but there are
``nativeGet*`` for deserialization, since C++ does not support multiple return value. For that, please use ``nativeGet*``
to deserialize value, test the first of pair for existence and use the second
of pair for the deserialized value.

Cooperating with C++
--------------------

Thor is compatible with C++ in order to:

* Gain native performance on hotspots
* Leverage rich resources of C++ community

C++ Functions
`````````````

Given that you have a C++ library consists of::

    foo.h
    libfoo.a

To use this library, first you need to expose its symbol to Thor.

Given ``foo.h``:

.. code-block:: cpp

    #include <iostream>
    #include "thor/PrimitiveTypes.h"

    namespace foo
    {
        namespace bar
        {
            thor::int32 f1();

            void f2(thor::int64 x);

            template<typename T>
            void miow(T arg)
            {
                std::cout << "Miow: " << arg << std::endl;
            }
        }
    }

You need a corresponding ``.t``:

.. code-block:: javascript

    # src/foo/bar/foo.t

    @native
    function f1() : int32;

    @native
    function f2(x : int64);

    @native {include = "foo.h"}
    function miow<T>(arg : T) : void;

Several points here:

#. Thor **module scope** is corresponding to **C++ namespace**. In the
   example above, to expose symbols under namespace foo::bar, you need to put
   your foo.t in **src/foo/bar**.

#. The corresponding type definitions of all primitive types in Thor
   can be found in **PrimitiveTypes.h**. You may first run **tsc extract**
   under your project directory, and find it under
   **dep/system.bundle/src/thor**. You'll also find that you need to enable
   C++11 features to compile, and generate position-independent code to link
   correctly. i.e. **-std=c++11 -fPIC** for GCC or Clang.

#. Annotate functions with **\@native** to specify that they are native
   functions.

#. If it's a template, provide the header with {include = ...}.

Now you can use foo() and bar, just like normal functions:

.. code-block:: javascript

    # src/main.t

    import .= foo.bar;

    @entry
    task main()
    {
        var x = foo();
        bar(x);
        miow(x);

        exit(0);
    }

Next, you need to indicate linking to ``libfoo.a`` in the project manifest:

.. code-block:: xml

    <project author="Zillians" bundle_type="native" id="1beaa1a1-a2b6-4c66-bcfc-4532eded4a87" name="simple" signature="" using_64bit="true" version="0.0.0.1">
        <dependency>
            <native_library name="foo" lpath="path/you/choose" />
        </dependency>
        <platform arch="" />
    </project>

Note the tag **native_library**:

* **name**: the native library name. In this case, we fill in ``foo`` to indicate ``libfoo.a``.
* **lpath**: the path to find the given library.

If your library is provided as a shared library, you can use **native_shared_library**:

.. code-block:: xml

    <native_shared_library name="foo" />  # libfoo.so

If your library is provided with a set of object files, you can use **native_object**:

.. code-block:: xml

    <native_object name="foo.o" />

C++ Classes
```````````

The workflow of exposing native C++ classes to Thor is in general the same as exposing C++ functions.

Given a class Foo:

.. code-block:: cpp

    #include "thor/PrimitiveTypes.h"
    #include "thor/lang/Language.h"  // for thor::lang::Object

    class Foo : public thor::lang::Object
    {
    public:
        Foo();
        ~Foo();

        void method1();
        thor::int32 method2();
        thor::int64 method3(thor::int64);

    private:
        thor::int32 data1;
        thor::int64 data2;
    };

For every C++ class you attempt to use in Thor, it **must**
inherit to **thor::lang::Object**.

Now you can expose the class to Thor as:

.. code-block:: javascript

    // src/foo/foo.t

    @native
    class Foo
    {
        @native
        public function new();    // corresponding to C++ Foo::Foo()

        @native
        public function delete(); // corresponding to C++ Foo::~Foo()

        @native
        public function method1();

        @native
        public function method2() : int32;

        @native
        public function method3(int64) : int64;

        private var data1 : int32;
        private var data2 : int64;
    }

Note that the class is annotated with **\@native**. It's often safer to
manually define constructor and destructor, since C++ compilers are not
guaranteed to generate one for you if they are not called.

Since all Thor classes implicitly inherit to **Object**, thus the class
declaration above is equivalent to:

.. code-block:: javascript

    @native
    class Foo extends Object

It's important that the corresponding class defined in Thor has defined
data members with **the same size and the same order** as those in C++.
Otherwise, you will encounter all kinds of memory corruption error on allocating
or freeing instances of the class.

Now you can use ``Foo`` as usual:

.. code-block:: javascript

    import .= foo;

    @entry
    task main()
    {
        var f = new Foo();
        f.method1();
        f.method2();
        f.method3(123);

        exit(0);
    }

Just like exposing native function templates, you can expose native class templates:

.. code-block:: cpp

    #include "thor/lang/Language.h"

    template <typename T>
    class Bar : public thor::lang::Object
    {
    public:
        Bar(T data) : data(data)
        {
        }
        ~Bar()
        {
        }

        T data;
    };

.. code-block:: javascript

    @native {include = "foo.h"}
    class Bar<T> extends Object
    {
        @native
        public function new(d : T) : void;

        @native
        public function delete() : void;


        public var data : T;
    }

and use it just like other Thor class templates:

.. code-block:: javascript

    @entry
    task main()
    {
        var b = new Bar<int32>(123);
        println("Bar data: \{b.data}");

        exit(0);
    }

Garbage Collection
------------------

In Thor, there is garbage collection in order to improve productivity.

Introduction
````````````

In the following example:

.. code-block:: javascript

    class foo {}

    function do_something(): void {
        for (var i = 0; i < 5; ++i) {
            print("i = \{i}\n");

            new foo();
        }

        print("end of do_something\n");
    }

``do_something()`` will create five foo objects in loop, there is no need to
care about anying except how created instances is used, garbage collection will
take care of other things.

Also, we'll track member variables which is objects. For example:

.. code-block:: javascript

    class foo {}

    class bar {
        public function new(): void { member = new foo(); }

        private var f: foo;
    }

Member ``f`` of any ``bar`` instances will be tracked automatically. That is,
Thor will track the object which ``f`` referenced if it's not null.

Native Class Support
````````````````````

If there is a native class that stores some references to some Thor
objects. You may need to control how Thor tracking your native class.

For that, there is interface named ``GarbageCollectable`` and class named
``CollectableObject``. The following example shows the usage of it.

First, ``foo`` is declared as the following in Thor:

.. code-block:: javascript

    import thor.unmanaged;

    @native
    class foo implements thor.lang.GarbageCollectable {
        @native public function new(o1: Object, o2: Object, o3: Object): void;
        @native public function delete(): void;

        // implement interface from thor.lang.GarbageCollectable
        @native public function getContainedObjects(o: thor.lang.CollectableObject): void;

        // other methods..

        // native data pointer
        private var single_obj   : thor.unmanaged.ptr_<int8>;
        private var multiple_objs: thor.unmanaged.ptr_<int8>;
    }

There is function named ``getContainedObjects`` from interface
``GarbageCollectable`` as you see. Thor will invoke it when GC is
traversing objects under instances implementing such interface.

The following is the C++ part implementing it.

.. code-block:: c++

    #include <array>

    #include <thor/lang/GarbageCollectable.h>
    #include <thor/lang/Language.h>

    class foo
        : public ::thor::lang::Object
        , public ::thor::lang::GarbageCollectable
    {
    private:
        using Object            = ::thor::lang::Object           ;
        using CollectableObject = ::thor::lang::CollectableObject;

    public:
        foo(Object* o1, Object* o2, Object* o3)
            : single_obj   (nullptr)
            , multiple_objs(nullptr)
        {
            // the same object could be referenced more than once, this is allowed
            single_obj    = new std::array<Object*, 1>{o1};
            multiple_objs = new std::array<Object*, 3>{o1, o2, o3};
        }

        ~foo()
        {
            delete multiple_objs;
            delete single_obj   ;
        }

        // implement interface from thor.lang.GarbageCollectable
        void getContainedObjects(CollectableObject* o)
        {
            // track single object
            o->add(*single_obj);

            // track multiple objects
            o->add(multiple_objs->begin(), multiple_objs->end());
        }

        // other methods..

    private:
        std::array<Object*, 1>* single_obj   ;
        std::array<Object*, 3>* multiple_objs;
    };

As what you see,``foo::getContainedObjects`` uses ``CollectableObject::add`` to
tell GC the referenced objects in foo. Hence that there is no need to do null
check, Thor GC will take care of it.

Debugging with Thor
-------------------

For debugging, Thor has ability to debug user codes with popular
debuggers.

For example, with the following code:

.. code-block:: javascript

    @entry // this is the first line of src/thortest.t
    task test_entry(): void {
        var i32_origin : int32 = 1234;
        var i32_flipped: int32 = ~i32_origin;

        exit(0); // assume this line is line #6
    }

You could build as **debug**, then run it through a debugger:

.. code-block:: bash

    user@~/hello$ tsc build debug
    user@~/hello$ tsc run test_entry --debug-tool "--debug-tool-cmd=gdb --args"

Then Thor toolchain will use the command specified by *debug-tool-cmd* to
launch debugger, by append arguments required for Thor.

This example uses *gdb*, so toolchain will launch it, then *gdb* will wait for
your input. Now add a breakpoint by **b** (gdb command) and run it:

GDB prompt::

    (gdb) b thortest.t:6
    No source file named thortest.t.
    Make breakpoint pending on future shared library load? (y or [n]) y
    Breakpoint 1 (thortest.t:6) pending.
    (gdb) r
    Starting program: /usr/local/bin/ts-vm --project-path=~/hello --debug-tool=ts-vm --debug-tool-cmd gdb\ --args --client --dev=0 test_entry
    [Thread debugging using libthread_db enabled]
    Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".
    JIT compile project 'hello' as Tangle AST mode.
    manifest path: ~/hello/manifest.xml
    [New Thread 0x7fffee9ad700 (LWP 8605)]
    [New Thread 0x7fffee1ac700 (LWP 8606)]
    [New Thread 0x7fffed9ab700 (LWP 8607)]
    [New Thread 0x7fffdf42b700 (LWP 8608)]
    [New Thread 0x7fffdec2a700 (LWP 8609)]
    [New Thread 0x7fffde429700 (LWP 8610)]
    [New Thread 0x7fffddc28700 (LWP 8611)]
    STM_CONFIG environment variable not found... using NOrec
    STM library configured using config == NOrec
    [Switching to Thread 0x7fffee9ad700 (LWP 8605)]

    Breakpoint 1, test_entry () at thortest.t:6
    6           exit(0); // assume this line is line #6
    (gdb) 

The output may be a bit verbose, but you could find that *gdb* reached the
breakpoint we set and waits for another command.

Now you could test the value of *i32_origin* and *i32_flipped*:

GDB prompt::

    (gdb) p i32_origin
    $1 = 1234
    (gdb) p i32_flipped
    $2 = -1235
    (gdb) 

After debugging, just quit the debugger:

GDB prompt::

    (gdb) q
    A debugging session is active.

            Inferior 1 [process 7640] will be killed.

            Quit anyway? (y or n) y

For projects built as debug, Thor will emit debug information compatible
with popular debugger such as gdb. That is, enjoy debugging with your favorite
debugger.

Note that Thor provides debug information **only**, not debugger. You may
need to read manual of debugger you're using.

