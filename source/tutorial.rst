Language Tutorial
=================

Hello World
-----------

Objective: hands-on the compiler tool chain
```````````````````````````````````````````

Create Project
``````````````

You can create project with::

    [user@~]$ tsc project create helloworld

Create a source file named ``test.t``::

    [user@~]$ cd helloworld
    [user@~helloworld]$ vi src/test.t

copy paste following code::

    @entry
    task main()
    {
        print("Hello world!\n");
        exit(0);
    }

Compile and Run
```````````````

Compile
:::::::

Build the project with::

    [user@~helloworld]$ tsc build debug

``tsc`` is Thor Compiler driver, will search all ``*.t`` files under
project directory and compile.

Run
:::

Run the built binary::

    [user@~helloworld]$ tsc run main

output::

    hello world!

The Hello World
:::::::::::::::

::

    @entry
    task main()
    {
        print("Hello world!\n");
        exit(0);
    }


This declares a function named ``main``, takes no argument, has no return
value.  The function print out ``"Hello world!"`` and exit with ``0``.
(Actually, there are two kinds of function in Thor, ``function`` and
``task``, see `Function and Task <language-manual.html#function-and-task>`_.)

The Cult of Fibonacci
---------------------

Let's look into another classic example, but with more powerful features.
Following Thor snippet shows how to compute a term in Fibonacci
sequence **parallelly**.

.. code-block:: javascript

    // a task returns the 'n'th term in Fibonacci sequence
    task fib(n: int32): int32
    {
        print(n); // to examine execution flow

        if (n == 0)
            return 0;

        if (n == 1)
            return 1;

        var f1: int32;
        var f2: int32;

        flow -> {
            f1 = fib(n - 1);
            f2 = fib(n - 2);
        }

        return f1 + f2;
    }

Above is a ``task`` named ``fib``. Task is designed to parallelly
execute statements on another thread. And you have to call a ``task``
by ``flow`` or ``async`` keyword:

.. code-block:: javascript

    flow -> {
        // call tasks here
    }

``flb()`` uses ``flow`` block wrapping sub-problem statements. All
statements in block body are executed at the same time. But ``fib()`` will
stop running until ``flow`` statements are all finished, then it returns
value of ``f1 + f2``. Client side code is like following:

.. code-block:: javascript

    @entry
    task main(): void
    {
        var result: int32;
        flow -> {
            result = fib(5);
        }

        println("\nresult: \{result}");

        exit(0);
    }

To examine execution flow, we added a statement ``print(n);`` at
the ``fib()``\s first line to output parameter ``n`` whenever
``fib()`` is called. The result is a sequence of numbers. Try out
this example by type command:

.. code-block:: sh

    $ tsc build debug && tsc run main --domain=mt

We put an extra option ``--domain=mt`` to ``tsc`` before running
entry function ``main()``. ``--domain`` specifies the execution
mode of Thor virtual machine, and ``mt`` means
**Multi-Threaded**. ``--domain=mt`` enables multi-thread
support so that ``flow`` statements can really run together.
If users do not give any ``domain`` option, Thor virtual
machine will be launched by default setting: ``--domain=st``, run
by only one thread.

Output::

    534231221101010
    result: 5

The sequence maybe different when you run.

Zillians Bank
-------------

Let's say we start a bank, Zillians. Our customers can safely deposit their
life savings. As a software developer in the IT department, Denny's job is to create
a reliable ATM system which enables our customers withdraw their money anytime.

One day, our CEO Mark, stormed in Denny's office and speak out loud with great
joy: "We SHOULD allow thousands and thousands of people access one account
at the same time! Or how on earth we named our bank Zillians?"

He stormed out.

Forget about whether this is a reasonable functionality or not. Now Denny is
facing a client-server program. Each client would need to deal with a separate
thread to make response quick. To make things worse, they are now access the
same account! He is allergic to racing condtion!

Lucky for Denny, Thor supports **excution domain** which makes dispatching
works to client and server easy, and **software transactional memory** which
saves Denny from his worst allergy.

.. code-block:: javascript

    import thor.util;

    var account : int32 = 100;

    function withdraw(amount : int32) : void
    {
        var success : bool = false;
        atomic -> {
            if(account > 0)
            {
                account -= amount;
                success = true;
            }
        }
        if(success)
        {
            async[Domain.caller()]->receive_money(amount);
        }
        else
        {
            println("Oops! No money!");
            async[Domain.caller()]->done();
        }
    }

    var pocket : int32 = 0;

    function receive_money(amount : int32)
    {
        pocket += amount;
        async[Domain.caller()] -> withdraw(amount);
    }

    function done()
    {
        println("total money: \{pocket}");
        exit(0);
    }

    @entry
    task bank()
    {
        Domain.listen(
                "tcp://*:1234",
                null, // on-connected callback
                null, // on-disconnected callback
                lambda(id: thor.util.UUID, e: DomainError) : void {
                    if(e != DomainError.OK)
                        exit(1);
                }); // on-error callback
    }

    @entry
    task client()
    {
        Domain.connect(
                "tcp://localhost:1234",
                lambda(id: thor.util.UUID, d: Domain) : void{
                    async[d]->withdraw(1);
                },
                null,
                lambda(id: thor.util.UUID, e: DomainError) : void {
                    if(e != DomainError.OK)
                        exit(1);
                });
    }

Note that there are two entries: **bank()** and **client()**.
You may first start up an instance of **bank()**:

.. code-block:: javascript

    tsc r bank --domain=mt

Then, start up as many clients as you want. For example, 10 client instances:

.. code-block:: bash

    for i in {1..10}; do
        tsc r client &
    done

You'll see that the total amount of money received by all clients summing up to
the initial account value correctly::

    # an example run result
    total money: 9
    total money: 9
    total money: 9
    total money: 11
    total money: 10
    total money: 13
    total money: 10
    total money: 10
    total money: 9
    total money: 10

Async Call on Execution Domain
``````````````````````````````

It's time to see the complete syntax of a async statement::

    async[ExecutionDomain]->{
        ...
    }

An execution domain can be thought as a computational device doing the job.
An async statement allows you to **dispatch** your work to a
execution domain without worrying about details of communication.
You've seen async without the domain part. That's actually equivalent to
designate the execution domain as your local machine.

To write a client-server program with this mechanism, all you have to do
is to create domains respect to client and server, and use async to dispatch
jobs among them.

In the example, we first define two entry tasks for **bank** and **client**:

.. code-block:: javascript

    @entry
    task bank()
    {
        Domain.listen(
                "tcp://*:1234",
                lambda(id: thor.util.UUID, d: Domain) : void{ // on-connected callback
                    print("Incoming client: ");
                    println(id.toString());
                },
                null, // on-disconnected callback
                lambda(id: thor.util.UUID, e: DomainError) : void {
                    if(e != DomainError.OK)
                        exit(1);
                }); // on-error callback
    }

    @entry
    task client()
    {
        Domain.connect(
                "tcp://localhost:1234",
                lambda(id: thor.util.UUID, d: Domain) : void{ // on-connected callback
                    async[d]->withdraw(1);
                },
                null,
                lambda(id: thor.util.UUID, e: DomainError) : void {
                    if(e != DomainError.OK)
                        exit(1);
                });
    }

**bank()** uses **Domain.listen()** to wait for incoming connection.
**client()** uses **Domain.connect()** to connect to the **bank instance**.
You can see how the callback functions of Domain.connect() invoking **withdraw**
on the **bank instance** here:

.. code-block:: javascript

        lambda(id: thor.util.UUID, d: Domain) : void{
            async[d]->withdraw(1);
        }

The connected domain is passed in as the second argument.

Next, we use **Domain.caller()** to designate **the execution domain
calling this function**:

.. code-block:: javascript

   async[Domain.caller()]->{
       ...
   }

In case of **withdraw()**, it is **client instance**. In case of **receive_money()**,
it is **bank instance**:

.. code-block:: javascript

    // withdraw()
    ...
    async[Domain.caller()]->receive_money(amount);
    ...

    //receive_money()
    ...
    async[Domain.caller()] -> withdraw(amount);
    ...

Now you have a client-server program. Next we'll see
how easy it is to solving concurrent access of account using
**software transactional memory**.

For more details about Thor domain, please refer to the language manual:
`Distributed Domain <language-manual.html#distributed-domain>`_.

Software Transactional Memory
`````````````````````````````

Thor supports software transactional memory directly by the language itself.
Simply enclose the memory access statements in the **atomic** block:

.. code-block:: javascript

    atomic -> {
        ...
    }

All works done in the atomic block are guaranteed to be **transactional**, i.e.
be successful or be rewinded, no intermidiate result. Thus in the example program
above:

.. code-block:: javascript

    var success : bool = false;
    atomic -> {
        if(account > 0)
        {
            account -= amount;
            success = true;
        }
    }

Although **acount** is accessed by multiple clients concurrently, STM
guarantees we don't have to worry about a penny lost or a penny more
caused by race condition.

For more details about software transactional memory, please refer to the language manual: `Transactional Memory <language-manual.html#transactional-memory>`_.
