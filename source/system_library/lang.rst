thor.lang
=========

.. Language.t

.. _Object:

class Object
------------

Base class of all classes in Thor.

All classes will derived from this (directly or indirectly). Additionally,
Thor uses this as base class if no one is given.

.. _Object-hash:

hash():int64
`````````````

Generate hash value based on instance.

Default implementation generates hash value based on memory location. So
user-defined classes may want to overwrite this behavior, in order to generate value
based on logical value. For example, string content.

**Return**
    Hashed value.

**Remark**
    O(1) Time complexity

*[virtual]*

.. _Cloneable:

interface Cloneable
-------------------

Indicate classes are able to be cloned

Classes which implements this interface need to implement method ``clone``,
which responses to create copies of instance.

.. _cloneable-clone:

clone():Cloneable
``````````````````

Function to create copies of instance

This method defines the copying of instance. For example, user could create a
copy of string since thor.lang.String implements this interface.

This method creates copied, but do not define the copy is deep or shallow
copied. For classes implementing ``Cloneable``, user needs to implement this
method to provide required functionality of it.

**Return**
    copy of instance

**Return Value**
    **non-null** Be successfully copied

    **null**     Fail to be copied

**Remark**
    This method did not define data should be deep or shallow copied. Please
    refer to classes who implements this interface.

    For cases that classes implement Cloneable indirectly (by inheritance),
    it's meaningful to override this method to copy required data in
    sub-classes.

*[virtual]*

.. String.t

.. _String:

class String
------------

Built-in class holds character strings.

All string literal in Thor, for example "abc", are instances of this class.

Strings are immutable, that is, you cannot modify the value of it. Use
MutableString for such purposes.

String class provide the following functionalities:

- comparison.
- character/string search.
- sub-string.
- case conversion.

**Remark**
    Current implementation for case insensitive handling may fail under some
    locale, for example, Greek letter "Σ" may not be handled correctly.

**See**
    MutableString_

**Extends**
    Object_

**Implements**
    Cloneable_

.. _String-new:

new():void
``````````

Default constructor which creates empty string (equivalent to "").

.. _String-delete:

delete():void
`````````````

Destructor which releases all resources hold by instance.

*[virtual]* *[override]*

.. _String-hash:

hash():int64
````````````

Generate hash value based on character string representation.

Implementation of String.clone is different from the origin one. The generated
value is based on character string representation, for example "abc". So it's
possible that two String instance generate the same hash value.

**Return**
    Hashed value.

**Remark**
    O(n) time complexity.

**Overrides**
    :ref:`Object.hash():int64 <Object-hash>`

*[virtual]* *[override]*

.. _String-clone:

clone():String
``````````````

Create new instance with the same character string representation.

**Return**
    New instance with the same character string representation.

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[virtual]* *[override]*

.. _String-isEqual-String:

isEqual(rhs:String):bool
````````````````````````

Test if this instance and given one have the same character string
representation.

For example:

.. code-block:: javascript

    "abc".isEqual("abc"); // true
    "abc".isEqual("def"); // false
    "abc".isEqual("ABC"); // false

**Parameters**
    **rhs** String to compare to.

**Return**
    Boolean value indicates this and *rhs* are same or not.

**Return Value**
    **true**  They are the same.

    **false** They are not the same.

**Remark**
    The comparison is case sensitive. If case insensitive is required,
    please take a look on the overloaded version.

**See**
    :ref:`String.isEqual(String, bool):bool <String-isEqual-String-bool>`

    :ref:`String.isLessThan(String):bool <String-isLessThan-String>`

.. _String-isEqual-String-bool:

isEqual(rhs:String, ignore_case:bool):bool
``````````````````````````````````````````

Test if this instance and given one have the same character string
representation.

This is overloaded function to compare strings in case sensitive or insensitive
way. According to *ignore_case*.

For example:

.. code-block:: javascript

    "abc".isEqual("ABC", true ); // true
    "abc".isEqual("ABC", false); // false

**Parameters**
    **rhs**         String to compare to.

    **ignore_case** Flag indicates comparison should be case insensitive or
    not.

**Return**
    Boolean value indicates this and *rhs* are same or not.

**Return Value**
    **true**  They are the same.

    **false** They are not the same.

**See**
    :ref:`String.isEqual(String):bool <String-isEqual-String>`

    :ref:`String.isLessThan(String):bool <String-isLessThan-String>`

.. _String-isLessThan-String:

isLessThan(rhs:String):bool
```````````````````````````

Test if instance's character string representation is less than the given one's.

For example:

.. code-block:: javascript

    "abc".isLessThan("abc");  // false
    "abc".isLessThan("abcd"); // true
    "abc".isLessThan("def");  // true

**Parameters**
    **rhs** String to compare to.

**Return**
    Boolean value indicates this is less than *rhs*.

**Return Value**
    **true**  This is lesser.

    **false** This is not lesser.

**Remark**
    The comparison is case sensitive.

    Comparison is done in lexicographical way.

**See**
    :ref:`String.isEqual(String):bool <String-isEqual-String>`

    :ref:`String.isEqual(String, bool):bool <string-isEqual-String-bool>`

.. _String-length:

length():int32
``````````````

Count the number of characters stored.

**Return**
    Number of characters.

**Remark**
    The return value will not be negative integer.

.. _String-concate:

concate(other:String):String
````````````````````````````

Create concatenated string with this followed by the given one.

For example:

.. code-block:: javascript

    var new_str = "abc".concate("def");
    new_str.isEqual("abcdef"); // true

**Parameters**
    **other** Another string to be appended at the end.

**Return**
    The concatenated string.

.. _String-find-String:

find(candidate:String):int32
````````````````````````````

Get the index of first occurrence of give string.

For example:

.. code-block:: javascript

    "abcdef"    .find("cd"); // 2
    "abcdefcdef".find("cd"); // 2

**Parameters**
    **candidate** The string to search for.

**Return**
    The index of first occurrence.

**Return Value**
    **Non-negative-value** The index.

    **Negative-value**     No occurrence of *candidate*.

.. _String-compareTo-String:

compareTo(other:String):int32
`````````````````````````````

Compare character string representation with another one.

This function compares character string representation, then returns negative
value, zero, or positive value.

Negative value means the instance is less than the given one; positive value
means greater than; otherwise, zero is returned.

For example:

.. code-block:: javascript

    "def".compareTo("abc"); // > 0
    "def".compareTo("def"); //   0
    "def".compareTo("ghi"); // < 0

**Parameters**
    **other** String to be compared to.

**Return**
    Integer value indicates this is less than, equal to, or greater than
    *other*.

**Return Value**
    **Negative-value** This is less than *other*.

    **0**              This is equal to *other*.

    **Positive-value** This is greater than *other*.

**Remark**
    The comparison is case sensitive.

    Comparison is done in lexicographical way.

**See**
    :ref:`String.compareTo(String, bool):int32 <String-compareTo-String-bool>`

.. _String-compareTo-String-bool:

compareTo(other:String, ignore_case:bool):int32
```````````````````````````````````````````````

Compare character string representation with another one.

This function compares character string representation (case insensitive), then
returns negative value, zero, or positive value.

Negative value means the instance is less than the given one; positive value
means greater than; otherwise, zero is returned.

This is overloaded function to compare strings in case sensitive or insensitive
way. According to *ignore_case*.

For example:

.. code-block:: javascript

    "def".compareTo("abc", true ); // >  0
    "def".compareTo("def", true ); //    0
    "def".compareTo("DEF", false); // != 0
    "def".compareTo("ghi", true ); // <  0

**Parameters**
    **other**       String to be compared to.

    **ignore_case** Flag indicates comparison should be case insensitive or
    not.

**Return**
    Integer value indicates this is less than, equal to, or greater than
    *other*.

**Return Value**
    **Negative-value** This is less than *other*.

    **0**              This is equal to *other*.

    **Positive-value** This is greater than *other*.

**Remark**
    Comparison is done in lexicographical way.

**See**
    :ref:`String.compareTo(String):int32 <String-compareTo-String>`

.. _String-endsWith-String:

endsWith(suffix:String):bool
````````````````````````````

Test if this instance has suffix the same as given string.

For example:

.. code-block:: javascript

    "abc".endsWith(  "c"); // true
    "abc".endsWith( "bc"); // true
    "abc".endsWith("abc"); // true
    "abc".endsWith(  "a"); // false

**Parameters**
    **suffix** Suffix to test for.

**Return**
    Boolean value indicates suffix is the same or not.

**Return Value**
    **true**  This instance has given suffix.

    **false** This instance has no given suffix.

**See**
    :ref:`String.startsWith(String):bool <String-startsWith-String>`

    :ref:`String.startsWith(String, int32):bool <String-startsWith-String-int32>`

.. _String-indexOf-String:

indexOf(str:String):int32
`````````````````````````

Search of the first occurrence of given string.

For example:

.. code-block:: javascript

    "abcabcabc".indexOf("a" ); // 0
    "abcabcabc".indexOf("b" ); // 1
    "abcabcabc".indexOf("bc"); // 1

**Parameters**
    **str** Given string to search for.

**Return**
    Index of first character of the first occurrence.

**Return Value**
    **-1**        Given string is not found.

    **Otherwise** The index.

**See**
    :ref:`String.indexOf(String, int32):int32 <String-indexOf-String-int32>`

.. _String-indexOf-String-int32:

indexOf(str:String, fromIndex:int32):int32
``````````````````````````````````````````

Search of the first occurrence of given string, starts at the given index.

For example:

.. code-block:: javascript

    "abcabcabc".indexOf("a"    ); // 0
    "abcabcabc".indexOf("a" , 1); // 3
    "abcabcabc".indexOf("a" , 4); // 6

**Parameters**
    **str**       Given string to search for.

    **fromIndex** The begining of search.

**Return**
    Index of first character of the first occurrence.

**Return Value**
    **-1**        Given string is not found.

    **Otherwise** The index.

**See**
    :ref:`String.indexOf(String):int32 <String-indexOf-String>`

.. _String-lastIndexOf-String:

lastIndexOf(str:String):int32
`````````````````````````````

Search of the last occurrence of given string.

For example:

.. code-block:: javascript

    "abcabcabc".lastIndexOf("a" ); // 6
    "abcabcabc".lastIndexOf("b" ); // 7
    "abcabcabc".lastIndexOf("bc"); // 7

**Parameters**
    **str** Given string to search for.

**Return**
    Index of first character of the last occurrence.

**Return Value**
    **-1**        Given string is not found.

    **Otherwise** The index.

**See**
    :ref:`String.lastIndexOf(String, int32):int32 <String-lastIndexOf-String-int32>`

.. _String-lastIndexOf-String-int32:

lastIndexOf(str:String, fromIndex:int32):int32
``````````````````````````````````````````````

Search of the last occurrence of given string, starts backword at the given
index.

For example:

.. code-block:: javascript

    "abcabcabc".lastIndexOf("a"    ); // 6
    "abcabcabc".lastIndexOf("a" , 1); // 0
    "abcabcabc".lastIndexOf("a" , 4); // 3

**Parameters**
    **str**       Given string to search for.

    **fromIndex** The begining of search.

**Return**
    Index of first character of the last occurrence.

**Return Value**
    **-1**        Given string is not found.

    **Otherwise** The index.

**See**
    :ref:`String.lastIndexOf(String):int32 <String-lastIndexOf-String>`

.. _String-regionMatches-bool-int32-String-int32-int32:

regionMatches(ignoreCase:bool, toffset:int32, other:String, ooffset:int32, len:int32):bool
``````````````````````````````````````````````````````````````````````````````````````````

Test if the specified regions of strings are the same, could be case sensitive
or insensitive.

This function will compare case sensitively or insensitively *len* characters
between two regions. One is this current string starts at *toffset*, another
one is *other* starts at *ooffset*.

Then return boolean value indicates the two regions are the same or not.

**Parameters**
    **ignoreCase** Flag indicates comparison should be in case sensitive way
    or not.

    **toffset**    Start position of comparison for this string.

    **other**      Given string to compare to.

    **ooffset**    Start position of comparison for *other*.

    **len**        Number of characters to be compared.

**Return**
    Boolean value indicates the two regions are the same or not.

**Return Value**
    **true**  Two regions are the same.

    **false** Two regions are not the same.

**See**
    :ref:`String.regionMatches(int32, String, int32, int32):bool <String-regionMatches-int32-String-int32-int32>`

.. _String-regionMatches-int32-String-int32-int32:

regionMatches(toffset:int32, other:String, ooffset:int32, len:int32):bool
`````````````````````````````````````````````````````````````````````````

Test if the specified regions of strings are the same.

This function will compare *len* characters between two regions. One is this
current string starts at *toffset*, another one is *other* starts at
*ooffset*.

Then return boolean value indicates the two regions are the same or not.

**Parameters**
    **toffset** Start position of comparison for this string.

    **other**   Given string to compare to.

    **ooffset** Start position of comparison for *other*.

    **len**     Number of characters to be compared.

**Return**
    Boolean value indicates the two regions are the same or not.

**Return Value**
    **true**  Two regions are the same.

    **false** Two regions are not the same.

**Remark**
    The comparison is case sensitive.

**See**
    :ref:`String.regionMatches(bool, int32, String, int32, int32):bool <String-regionMatches-bool-int32-String-int32-int32>`

.. _String-startsWith-String:

startsWith(prefix:String):bool
``````````````````````````````

Test if this instance has prefix the same as given string.

For example:

.. code-block:: javascript

    "abc".startsWith("a"  ); // true
    "abc".startsWith("ab" ); // true
    "abc".startsWith("abc"); // true
    "abc".startsWith("c"  ); // false

**Parameters**
    **prefix** Prefix to test for.

**Return**
    Boolean value indicates prefix is the same or not.

**Return Value**
    **true**  This instance has given prefix.

    **false** This instance has no given prefix.

**See**
    :ref:`String.endsWith(String):bool <String-endsWith-String>`

    :ref:`String.startsWith(String, int32):bool <String-startsWith-String-int32>`

.. _String-startsWith-String-int32:

startsWith(prefix:String, toffset:int32):bool
`````````````````````````````````````````````

Test if this instance has prefix the same as given string.

This function will test for prefix which starts at position specified by
*toffset*.

For example:

.. code-block:: javascript

    "abc".startsWith("bc", 0); // false
    "abc".startsWith("bc", 1); // true
    "abc".startsWith("bc", 2); // false
    "abc".startsWith("bc", 3); // false

**Parameters**
    **prefix**  Prefix to test for.

    **toffset** Prefix should start from.

**Return**
    Boolean value indicates prefix is the same or not.

**Return Value**
    **true**  This instance has given prefix.

    **false** This instance has no given prefix.

**See**
    :ref:`String.endsWith(String):bool <String-endsWith-String>`

    :ref:`String.startsWith(String):bool <String-startsWith-String>`

.. _String-substring-int32:

substring(beginIndex:int32):String
``````````````````````````````````

Create new instance with all characters start at *beginIndex*.

**Parameters**
    **beginIndex** The first character to be copied.

**Return**
    The new instance contains copied characters.

**See**
    :ref:`String.substring(int32, int32):String <String-substring-int32-int32>`

.. _String-substring-int32-int32:

substring(beginIndex:int32, endIndex:int32):String
``````````````````````````````````````````````````

Create new instance with all characters start at *beginIndex* and end of
*endIndex*.

**Parameters**
    **beginIndex** The first character to be copied.

    **endIndex**   Next character of the last one to be copied.

**Return**
    The new instance contains copied characters.

**See**
    :ref:`String.substring(int32):String <String-substring-int32>`

.. _String-toLowerCase:

toLowerCase():String
````````````````````

Create new instance with all characters converted to lower case.

**Return**
    The new instance with lower case characters.

**See**
    :ref:`String.toUpperCase():String <String-toUpperCase>`

.. _String-toUpperCase:

toUpperCase():String
````````````````````

Create new instance with all characters converted to upper case.

**Return**
    The new instance with upper case characters.

**See**
    :ref:`String.toLowerCase():String <String-toLowerCase>`

.. _String-trim:

trim():String
`````````````

Create new instance with all characters, except leading and trailing whitespaces.

For example:

.. code-block:: javascript

    "abc"  .trim(); // "abc"
    "  abc".trim(); // "abc"
    "abc  ".trim(); // "abc"

**Return**
    The new instance without leading and trailing whitespaces.

.. _String-toString:

toString():String
`````````````````

Create new instance with the same character string representation.

**Return**
    Created new instance.

.. _String-__add__-bool:

__add__(other:bool):MutableString
`````````````````````````````````

Overload operator '+' to create new instance with ``true`` or ``false`` appended.

For example:

.. code-block:: javascript

    "abc " + true ; // "abc true"
    "abc " + false; // "abc false"

**Parameters**
    **other** Append ``true`` if value is *true*; ``false`` otherwise.

**Return**
    MutableString contains characters of origin one with ``true`` or ``false``
    appended.

**See**
    :ref:`String.__add__(int8):MutableString <String-__add__-int8>`

    :ref:`String.__add__(int16):MutableString <String-__add__-int16>`

    :ref:`String.__add__(int32):MutableString <String-__add__-int32>`

    :ref:`String.__add__(int64):MutableString <String-__add__-int64>`

    :ref:`String.__add__(float32):MutableString <String-__add__-float32>`

    :ref:`String.__add__(float64):MutableString <String-__add__-float64>`

    :ref:`String.__add__(String):MutableString <String-__add__-String>`

.. _String-__add__-int8:

__add__(other:int8):MutableString
`````````````````````````````````

Overload operator '+' to create new instance with string representation of
integer value appended.

For example:

.. code-block:: javascript

    "abc " + cast<int8>(12); // "abc 12"
    "abc " + cast<int8>(21); // "abc 21"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    MutableString contains characters of origin one with value string
    appended.

**See**
    :ref:`String.__add__(bool):MutableString <String-__add__-bool>`

    :ref:`String.__add__(int16):MutableString <String-__add__-int16>`

    :ref:`String.__add__(int32):MutableString <String-__add__-int32>`

    :ref:`String.__add__(int64):MutableString <String-__add__-int64>`

    :ref:`String.__add__(float32):MutableString <String-__add__-float32>`

    :ref:`String.__add__(float64):MutableString <String-__add__-float64>`

    :ref:`String.__add__(String):MutableString <String-__add__-String>`

.. _String-__add__-int16:

__add__(other:int16):MutableString
``````````````````````````````````

Overload operator '+' to create new instance with string representation of
integer value appended.

For example:

.. code-block:: javascript

    "abc " + cast<int16>(1234); // "abc 1234"
    "abc " + cast<int16>(7777); // "abc 7777"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    MutableString contains characters of origin one with value string
    appended.

**See**
    :ref:`String.__add__(bool):MutableString <String-__add__-bool>`

    :ref:`String.__add__(int8):MutableString <String-__add__-int8>`

    :ref:`String.__add__(int32):MutableString <String-__add__-int32>`

    :ref:`String.__add__(int64):MutableString <String-__add__-int64>`

    :ref:`String.__add__(float32):MutableString <String-__add__-float32>`

    :ref:`String.__add__(float64):MutableString <String-__add__-float64>`

    :ref:`String.__add__(String):MutableString <String-__add__-String>`

.. _String-__add__-int32:

__add__(other:int32):MutableString
``````````````````````````````````

Overload operator '+' to create new instance with string representation of
integer value appended.

For example:

.. code-block:: javascript

    "abc " + 1234; // "abc 1234"
    "abc " + 7777; // "abc 7777"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    MutableString contains characters of origin one with value string
    appended.

**See**
    :ref:`String.__add__(bool):MutableString <String-__add__-bool>`

    :ref:`String.__add__(int8):MutableString <String-__add__-int8>`

    :ref:`String.__add__(int16):MutableString <String-__add__-int16>`

    :ref:`String.__add__(int64):MutableString <String-__add__-int64>`

    :ref:`String.__add__(float32):MutableString <String-__add__-float32>`

    :ref:`String.__add__(float64):MutableString <String-__add__-float64>`

    :ref:`String.__add__(String):MutableString <String-__add__-String>`

.. _String-__add__-int64:

__add__(other:int64):MutableString
``````````````````````````````````

Overload operator '+' to create new instance with string representation of
integer value appended.

For example:

.. code-block:: javascript

    "abc " + cast<int64>(1234); // "abc 1234"
    "abc " + cast<int64>(7777); // "abc 7777"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    MutableString contains characters of origin one with value string
    appended.

**See**
    :ref:`String.__add__(bool):MutableString <String-__add__-bool>`

    :ref:`String.__add__(int8):MutableString <String-__add__-int8>`

    :ref:`String.__add__(int16):MutableString <String-__add__-int16>`

    :ref:`String.__add__(int32):MutableString <String-__add__-int32>`

    :ref:`String.__add__(float32):MutableString <String-__add__-float32>`

    :ref:`String.__add__(float64):MutableString <String-__add__-float64>`

    :ref:`String.__add__(String):MutableString <String-__add__-String>`

.. _String-__add__-float32:

__add__(other:float32):MutableString
````````````````````````````````````

Overload operator '+' to create new instance with string representation of
floating value appended.

For example:

.. code-block:: javascript

    "abc " + cast<float32>(12.34); // "abc 12.34"
    "abc " + cast<float32>(77.77); // "abc 77.77"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    MutableString contains characters of origin one with value string
    appended.

**See**
    :ref:`String.__add__(bool):MutableString <String-__add__-bool>`

    :ref:`String.__add__(int8):MutableString <String-__add__-int8>`

    :ref:`String.__add__(int16):MutableString <String-__add__-int16>`

    :ref:`String.__add__(int32):MutableString <String-__add__-int32>`

    :ref:`String.__add__(int64):MutableString <String-__add__-int64>`

    :ref:`String.__add__(float64):MutableString <String-__add__-float64>`

    :ref:`String.__add__(String):MutableString <String-__add__-String>`

.. _String-__add__-float64:

__add__(other:float64):MutableString
````````````````````````````````````

Overload operator '+' to create new instance with string representation of
floating value appended.

For example:

.. code-block:: javascript

    "abc " + 12.34; // "abc 12.34"
    "abc " + 77.77; // "abc 77.77"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    MutableString contains characters of origin one with value string
    appended.

**See**
    :ref:`String.__add__(bool):MutableString <String-__add__-bool>`

    :ref:`String.__add__(int8):MutableString <String-__add__-int8>`

    :ref:`String.__add__(int16):MutableString <String-__add__-int16>`

    :ref:`String.__add__(int32):MutableString <String-__add__-int32>`

    :ref:`String.__add__(int64):MutableString <String-__add__-int64>`

    :ref:`String.__add__(float32):MutableString <String-__add__-float32>`

    :ref:`String.__add__(String):MutableString <String-__add__-String>`

.. _String-__add__-String:

__add__(other:String):MutableString
```````````````````````````````````

Overload operator '+' to create new instance which concates this and the given
one.

For example:

.. code-block:: javascript

    "abc " + "abc"; // "abc abc"
    "abc " + "def"; // "abc def"

**Parameters**
    **other** Another string to be concated.

**Return**
    MutableString contains concated characters.

**See**
    :ref:`String.__add__(bool):MutableString <String-__add__-bool>`

    :ref:`String.__add__(int8):MutableString <String-__add__-int8>`

    :ref:`String.__add__(int16):MutableString <String-__add__-int16>`

    :ref:`String.__add__(int32):MutableString <String-__add__-int32>`

    :ref:`String.__add__(int64):MutableString <String-__add__-int64>`

    :ref:`String.__add__(float32):MutableString <String-__add__-float32>`

    :ref:`String.__add__(float64):MutableString <String-__add__-float64>`

.. _String-serialize-ReplicationEncoder-String:

serialize(encoder:ReplicationEncoder, instance:String):bool
```````````````````````````````````````````````````````````

Customized serialization for native data.

**See**
    :ref:`String.deserialize(ReplicationDecoder, String):bool <String-deserialize-ReplicationDecoder-String>`

*[protected]* *[static]*

.. _String-deserialize-ReplicationDecoder-String:

deserialize(decoder:ReplicationDecoder, instance:String):bool
`````````````````````````````````````````````````````````````

Customized deserialization for native data.

**See**
    :ref:`String.serialize(ReplicationEncoder, String):bool <String-serialize-ReplicationEncoder-String>`

*[protected]* *[static]*

.. _MutableString:

class MutableString
-------------------

Built-in class holds character strings, but is mutable.

For string operations, immutable string may produce bad performance in some
cases. For that, Thor provides ``MutableString``, which is mutable.

This class may change the underlying data by operations, which gains performance
by reusing resources.

**Extends**
    String_

.. _MutableString-new:

new(s:String):void
``````````````````

Constructor creates instance with the same character string as the given one.

.. _MutableString-delete:

delete():void
`````````````

Destructor which releases all resources hold by instance.

**Overrides**
    :ref:`String.delete():void <String-delete>`

*[virtual]* *[override]*

.. _MutableString-clone:

clone():MutableString
`````````````````````

Create new instance with the same character string representation.

**Return**
    New instance with the same character string representation.

**Overrides**
    :ref:`String.clone():String <String-clone>`

*[virtual]* *[override]*

.. _MutableString-concate-bool:

concate(other:bool):MutableString
`````````````````````````````````

Append ``true`` or ``false``.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate(true ); // "abc true"
    (new MutableString("abc ")).concate(false); // "abc false"

**Parameters**
    **other** Append ``true`` if value is *true*; ``false`` otherwise.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-concate-int8:

concate(other:int8):MutableString
`````````````````````````````````

Append string representation of integer value.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate(cast<int8>(12)); // "abc 12"
    (new MutableString("abc ")).concate(cast<int8>(21)); // "abc 21"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-concate-int16:

concate(other:int16):MutableString
``````````````````````````````````

Append string representation of integer value.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate(cast<int16>(1234)); // "abc 1234"
    (new MutableString("abc ")).concate(cast<int16>(7777)); // "abc 7777"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-concate-int32:

concate(other:int32):MutableString
``````````````````````````````````

Append string representation of integer value.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate(1234); // "abc 1234"
    (new MutableString("abc ")).concate(7777); // "abc 7777"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-concate-int64:

concate(other:int64):MutableString
``````````````````````````````````

Append string representation of integer value.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate(cast<int64>(1234)); // "abc 1234"
    (new MutableString("abc ")).concate(cast<int64>(7777)); // "abc 7777"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-concate-float32:

concate(other:float32):MutableString
````````````````````````````````````

Append string representation of floating value.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate(cast<float32>(12.34)); // "abc 12.34"
    (new MutableString("abc ")).concate(cast<float32>(77.77)); // "abc 77.77"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-concate-float64:

concate(other:float64):MutableString
````````````````````````````````````

Append string representation of floating value.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate(12.34); // "abc 12.34"
    (new MutableString("abc ")).concate(77.77); // "abc 77.77"

**Parameters**
    **other** Value to generate as string representation.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-concate-String:

concate(other:String):MutableString
```````````````````````````````````

Append the given string.

For example:

.. code-block:: javascript

    (new MutableString("abc ")).concate("abc"); // "abc abc"
    (new MutableString("abc ")).concate("def"); // "abc def"

**Parameters**
    **other** Another string to be concated.

**Return**
    This instance.

**See**
    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

.. _MutableString-__add__-bool:

__add__(other:bool):MutableString
`````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(int8):MutableString <MutableString-__add__-int8>`

    :ref:`MutableString.__add__(int16):MutableString <MutableString-__add__-int16>`

    :ref:`MutableString.__add__(int32):MutableString <MutableString-__add__-int32>`

    :ref:`MutableString.__add__(int64):MutableString <MutableString-__add__-int64>`

    :ref:`MutableString.__add__(float32):MutableString <MutableString-__add__-float32>`

    :ref:`MutableString.__add__(float64):MutableString <MutableString-__add__-float64>`

    :ref:`MutableString.__add__(String):MutableString <MutableString-__add__-String>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-__add__-int8:

__add__(other:int8):MutableString
`````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(bool):MutableString <MutableString-__add__-bool>`

    :ref:`MutableString.__add__(int16):MutableString <MutableString-__add__-int16>`

    :ref:`MutableString.__add__(int32):MutableString <MutableString-__add__-int32>`

    :ref:`MutableString.__add__(int64):MutableString <MutableString-__add__-int64>`

    :ref:`MutableString.__add__(float32):MutableString <MutableString-__add__-float32>`

    :ref:`MutableString.__add__(float64):MutableString <MutableString-__add__-float64>`

    :ref:`MutableString.__add__(String):MutableString <MutableString-__add__-String>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-__add__-int16:

__add__(other:int16):MutableString
``````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(bool):MutableString <MutableString-__add__-bool>`

    :ref:`MutableString.__add__(int8):MutableString <MutableString-__add__-int8>`

    :ref:`MutableString.__add__(int32):MutableString <MutableString-__add__-int32>`

    :ref:`MutableString.__add__(int64):MutableString <MutableString-__add__-int64>`

    :ref:`MutableString.__add__(float32):MutableString <MutableString-__add__-float32>`

    :ref:`MutableString.__add__(float64):MutableString <MutableString-__add__-float64>`

    :ref:`MutableString.__add__(String):MutableString <MutableString-__add__-String>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-__add__-int32:

__add__(other:int32):MutableString
``````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(bool):MutableString <MutableString-__add__-bool>`

    :ref:`MutableString.__add__(int8):MutableString <MutableString-__add__-int8>`

    :ref:`MutableString.__add__(int16):MutableString <MutableString-__add__-int16>`

    :ref:`MutableString.__add__(int64):MutableString <MutableString-__add__-int64>`

    :ref:`MutableString.__add__(float32):MutableString <MutableString-__add__-float32>`

    :ref:`MutableString.__add__(float64):MutableString <MutableString-__add__-float64>`

    :ref:`MutableString.__add__(String):MutableString <MutableString-__add__-String>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-__add__-int64:

__add__(other:int64):MutableString
``````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(bool):MutableString <MutableString-__add__-bool>`

    :ref:`MutableString.__add__(int8):MutableString <MutableString-__add__-int8>`

    :ref:`MutableString.__add__(int16):MutableString <MutableString-__add__-int16>`

    :ref:`MutableString.__add__(int32):MutableString <MutableString-__add__-int32>`

    :ref:`MutableString.__add__(float32):MutableString <MutableString-__add__-float32>`

    :ref:`MutableString.__add__(float64):MutableString <MutableString-__add__-float64>`

    :ref:`MutableString.__add__(String):MutableString <MutableString-__add__-String>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-__add__-float32:

__add__(other:float32):MutableString
````````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(bool):MutableString <MutableString-__add__-bool>`

    :ref:`MutableString.__add__(int8):MutableString <MutableString-__add__-int8>`

    :ref:`MutableString.__add__(int16):MutableString <MutableString-__add__-int16>`

    :ref:`MutableString.__add__(int32):MutableString <MutableString-__add__-int32>`

    :ref:`MutableString.__add__(int64):MutableString <MutableString-__add__-int64>`

    :ref:`MutableString.__add__(float64):MutableString <MutableString-__add__-float64>`

    :ref:`MutableString.__add__(String):MutableString <MutableString-__add__-String>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-__add__-float64:

__add__(other:float64):MutableString
````````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(bool):MutableString <MutableString-__add__-bool>`

    :ref:`MutableString.__add__(int8):MutableString <MutableString-__add__-int8>`

    :ref:`MutableString.__add__(int16):MutableString <MutableString-__add__-int16>`

    :ref:`MutableString.__add__(int32):MutableString <MutableString-__add__-int32>`

    :ref:`MutableString.__add__(int64):MutableString <MutableString-__add__-int64>`

    :ref:`MutableString.__add__(float32):MutableString <MutableString-__add__-float32>`

    :ref:`MutableString.__add__(String):MutableString <MutableString-__add__-String>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. _MutableString-__add__-String:

__add__(other:String):MutableString
```````````````````````````````````

Overload operator '+' with the same operation as concate, for convenience.

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`MutableString.__add__(bool):MutableString <MutableString-__add__-bool>`

    :ref:`MutableString.__add__(int8):MutableString <MutableString-__add__-int8>`

    :ref:`MutableString.__add__(int16):MutableString <MutableString-__add__-int16>`

    :ref:`MutableString.__add__(int32):MutableString <MutableString-__add__-int32>`

    :ref:`MutableString.__add__(int64):MutableString <MutableString-__add__-int64>`

    :ref:`MutableString.__add__(float32):MutableString <MutableString-__add__-float32>`

    :ref:`MutableString.__add__(float64):MutableString <MutableString-__add__-float64>`

    :ref:`MutableString.concate(bool):MutableString <MutableString-concate-bool>`

    :ref:`MutableString.concate(int8):MutableString <MutableString-concate-int8>`

    :ref:`MutableString.concate(int16):MutableString <MutableString-concate-int16>`

    :ref:`MutableString.concate(int32):MutableString <MutableString-concate-int32>`

    :ref:`MutableString.concate(int64):MutableString <MutableString-concate-int64>`

    :ref:`MutableString.concate(float32):MutableString <MutableString-concate-float32>`

    :ref:`MutableString.concate(float64):MutableString <MutableString-concate-float64>`

    :ref:`MutableString.concate(String):MutableString <MutableString-concate-String>`

.. Domain.t

.. _DomainError:

enum DomainError
----------------

Define possible errors by Domain APIs.

**Enumrators**
    **OK**                      No error.

    **INVALID_ENDPOINT_SPEC**   Incorrect end-point specified by string.

    **UNSUPPORTED_TRANSPORT**   Unsupported protocol specified by end-point
    string.

    **EXCEED_CONCURRENT_LIMIT** Run out of concurrent limit (hard limit) of
    domain connections.

    **NO_SUCH_REQUEST**         ID specified by user is not recognized as
    any Domain session.

    **UNKNOWN_ERROR**           Other errors.

.. _DomainConnCallback:

typedef lambda(thor.util.UUID, Domain):void DomainConnCallback
--------------------------------------------------------------

Connection callback for Domain APIs.

**Remark**
    This callback is used in both connect and disconnect.

.. _DomainErrCallback:

typedef lambda(thor.util.UUID, DomainError):void DomainErrCallback
------------------------------------------------------------------

Error callback for Domain APIs.

.. _Domain:

class Domain
------------

Class represents the virtual target for program execution.

In Thor, program execution could be dispatched to any possible targets.
That is, a function call could be decided to run on CPU or GPU at runtime.

This class collects the required functionality for that:

- local() and caller() to retrieve Domain object represents the target of
  local and invoker.
- type() to retrieve the type of Domain, for example, single-threaded or
  multi-threaded.
- listen, connect, and cancel for inter-domain communication.

**Extends**
    Object_

.. _Domain-local:

local():Domain
``````````````

Function to retrieve instance represents local target.

**Return**
    The instance represents local target.

**Remark**
    This function will not return ``null``.

**See**
    :ref:`Doman.caller():Domain <Domain-caller>`

*[static]*

.. _Domain-caller:

caller():Domain
```````````````

Function to retrieve instance represents target of asynchronous caller.

In Thor, the asynchronous caller means the caller who requests to execute
a asynchronous function.

For example, in the following code:

.. code-block:: javascript

    function callee_inner()
    {
        const caller_domain: Domain = Domain.caller();
    }
    function callee()
    {
        const caller_domain: Domain = Domain.caller();
        callee_inner();
    }
    function caller()
    {
        const requester: Domain = Domain.local();
        const executor : Domain = ...; // get the execution target
        async[executor] -> callee();
    }

``caller_domain`` in both ``callee()`` and ``callee_inner()`` should be logically
equivalent to ``requester`` in ``caller()``.

**Return**
    The instance represents target of asynchronous caller.

**Remark**
    This function will not return ``null``.

**See**
    :ref:`Domain.local():Domain <Domain-local>`

*[static]*

.. _Domain-listen-String-DomainConnCallback-DomainConnCallback-DomainErrCallback:

listen(endpoint:String, conn_cb:DomainConnCallback, disconn_cb:DomainConnCallback, err_cb:DomainErrCallback):thor.util.UUID
```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Function to listen for connection from other domains.

By design, user needs to specifies callbacks with string which specifies end
point to listen on. Then Thor VM could notify user through callbacks on
events, in order to avoid long-run functions.

To stop listening, pass the returned identifier to Domain.cancel.

**Parameters**
    **endpoint**   URI string specifies the end point to listen on.

    **conn_cb**    Callback to be called on connected.

    **disconn_cb** Callback to be called on disconnected.

    **err_cb**     Callback to be called on errors.

**Return**
    Identifier for this listen request.

**Return Value**
    **nil**    Fail to listen.

    **others** Listen is started.

**Remark**
    This function will not return ``null``.

**See**
    :ref:`Domain.connect(Stirng, DomainConnCallback, DomainConnCallback, DomainErrCallback):thor.util.UUID <Domain-connect-String-DomainConnCallback-DomainConnCallback-DomainErrCallback>`

    :ref:`Domain.cancel(thor.util.UUID):bool <Domain-cancel-thor.util.UUID>`

.. _Domain-connect-String-DomainConnCallback-DomainConnCallback-DomainErrCallback:

connect(endpoint:String, conn_cb:DomainConnCallback, disconn_cb:DomainConnCallback, err_cb:DomainErrCallback):thor.util.UUID
````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Function to connect to remote domains.

By design, user needs to specifies callbacks with string which specifies end
point to connect to. Then Thor VM could notify user through callbacks on
events, in order to avoid long-run functions.

To disconnect, pass the returned identifier to Domain.cancel.

**Parameters**
    **endpoint**   URI string specifies the end point to connect to.

    **conn_cb**    Callback to be called on connected.

    **disconn_cb** Callback to be called on disconnected.

    **err_cb**     Callback to be called on errors.

**Return**
    Identifier for this connection request.

**Return Value**
    **nil**    Fail to connect to domain.

    **others** Connected to remote domain.

**Remark**
    This function will not return ``null``.

**See**
    :ref:`Domain.listen(Stirng, DomainConnCallback, DomainConnCallback, DomainErrCallback):thor.util.UUID <Domain-listen-String-DomainConnCallback-DomainConnCallback-DomainErrCallback>`

    :ref:`Domain.cancel(thor.util.UUID):bool <Domain-cancel-thor.util.UUID>`

.. _Domain-cancel-thor.util.UUID:

cancel(id:thor.util.UUID):bool
``````````````````````````````

Function to cancel a connected or listening session.

For each connected or listening sessions (*Domain.listen* or
*Domain.connect*), user could abort it by this function.

**Parameters**
    **id** The identifier represents connected or listening session.

**Return**
    Boolean value indicates the session is successfully canceled or not.

**Return Value**
    **true**  Successfully canceled.

    **false** Corresponding session not found.

**See**
    :ref:`Domain.listen(Stirng, DomainConnCallback, DomainConnCallback, DomainErrCallback):thor.util.UUID <Domain-listen-String-DomainConnCallback-DomainConnCallback-DomainErrCallback>`

    :ref:`Domain.connect(Stirng, DomainConnCallback, DomainConnCallback, DomainErrCallback):thor.util.UUID <Domain-connect-String-DomainConnCallback-DomainConnCallback-DomainErrCallback>`

.. Array.t

.. _ArrayIterator-T:

class ArrayIterator<T>
----------------------

Iterator iterates over the whole array.

Thor provides this class in order to support iteration on array with
foreach loop. User may not want to use this class directly.

The following code shows how to use iterator directly:

.. code-block:: javascript

    var arr: Array<int32> = new Array(1234);

    ... // initialize array data

    for (var i: ArrayIterator<int32> = arr.iterator(); i.hasNext(); )
    {
        var element: int32 = i.next();
        print("element = \{element}\n");
    }

**See**
    :ref:`Array\<T\> <Array-T>`

**Extends**
    Object_

.. _ArrayIterator-T-new-Array-E:

new(c:Array<T>):void
````````````````````

Constructor initializes data to iterate over the whole array.

.. _ArrayIterator-T-delete:

delete():void
`````````````

Destructor releases resources for iteration.

*[virtual]* *[override]*

.. _ArrayIterator-T-hasNext:

hasNext():bool
``````````````

Test if there is remained element to be iterated.

**Return**
    Boolean value indicates there is remained element.

**Return Value**
    **true**  There is remained element.

    **false** No more element to be iterated.

**See**
    :ref:`ArrayIterator\<T\>.next():T <ArrayIterator-T-next>`

.. _ArrayIterator-T-next:

next():T
````````

Get next element in iteration.

**Return**
    The next element.

**Remark**
    The return value is undefined if hasNext() returns ``false``.

**See**
    :ref:`ArrayIterator\<T\>.hasNext():bool <ArrayIterator-T-hasNext>`

.. _Array-T:

class Array<T>
--------------

Dynamic allocated array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`ArrayIterator\<T\> <ArrayIterator-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array-T-new:

new():void
``````````

Default constructor allocates array with zero-length.

**See**
    :ref:`Array\<T\>.new(int64):void <Array-T-new-int64>`

.. _Array-T-new-int64:

new(size:int64):void
````````````````````

Constructor allocates array with given element count.

**Parameters**
    **size** The number of elements that allocated array should contain.

**See**
    :ref:`Array\<T\>.new():void <Array-T-new>`

.. _Array-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[virtual]* *[override]*

.. _Array-T-create-int64:

create(size_0:int64):Array<T>
`````````````````````````````

Provides functionality to create array in C++.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _Array-T-clone:

clone():Array<T>
````````````````

Create new instance using shallow copy.

By shallow copy, the underlying elements is copied instead of cloned.
That is, the new instance contains the same references for object types
and the same values for other types.

For example, with the following code:

.. code-block:: javascript

    import thor.util;

    class Foo
    {
        public var value: int32 = 0;
    }

    function get_foo_value(f: Foo): String
    {
        if (f == null)
            return "null";
        else
            return thor.util.Convert.toString(f.value);
    }

    function print_data(arr_int32: Array<int32>, arr_foo: Array<Foo>)
    {
        var i0 = arr_int32[0];
        var i1 = arr_int32[1];
        var f0 = arr_foo  [0];
        var f1 = arr_foo  [1];

        print("i0: " +               i0  + "\n");
        print("i1: " +               i1  + "\n");
        print("f0: " + get_foo_value(f0) + "\n");
        print("f2: " + get_foo_value(f1) + "\n");
    }

    @entry
    task test_entry()
    {
        var arr_int32 =      [        0,              1 ];
        var arr_foo   = <Foo>[new Foo(), cast<Foo>(null)];

        var cloned_arr_int32 = arr_int32.clone();
        var cloned_arr_foo   = arr_foo  .clone();

        // modifying array element!
        cloned_arr_int32[0]       = 1;
        cloned_arr_foo  [0].value = 1;

        print_data(       arr_int32,        arr_foo);
        print_data(cloned_arr_int32, cloned_arr_foo);

        exit(0);
    }

The output will be the following, you will see the first elements of
int32 arrays are different::

    i0: 0
    i1: 1
    f0: 1
    f2: null
    i0: 1
    i1: 1
    f0: 1
    f2: null

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[virtual]* *[override]*

.. _Array-T-get-int64:

get(index:int64):T
``````````````````

Get the element by index.

**Parameters**
    **index** The index of element.

**Return**
    The element indicated by index.

**See**
    :ref:`Array\<T\>.set(int64, T):void <Array-T-set-int64-T>`

.. _Array-T-set-int64-T:

set(index:int64, v:T):void
``````````````````````````

Set the element by index.

**Parameters**
    **index** The index of element.

    **v**     The value to set to.

**See**
    :ref:`Array\<T\>.get(int64):T <Array-T-get-int64>`

.. _Array-T-size:

size():int64
````````````

Get the size of array.

**Return**
    The size of array.

.. _Array-T-iterator:

iterator():ArrayIterator<T>
```````````````````````````

Create iterator which iterates the whole array elements.

**Return**
    The new iterator which is ready for iteration.

.. _Array-T-getContainedObjects-CollectableObject:

getContainedObjects(o:CollectableObject):void
`````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array2D-T:

class Array2D<T>
----------------

Dynamic allocated 2-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array2D-T-new:

new():void
``````````

Default constructor allocates 2-dimension array with zero-length.

**See**
    :ref:`Array2D\<T\>.new(int64, int64):void <Array2D-T-new-int64-int64>`

.. _Array2D-T-new-int64-int64:

new(size_0:int64, size_1:int64):void
````````````````````````````````````

Constructor allocates 2-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

**See**
    :ref:`Array2D\<T\>.new():void <Array2D-T-new>`

.. _Array2D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

.. _Array2D-T-clone:

clone():Array2D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

.. _Array2D-T-get-int64-int64:

get(idx_0:int64, idx_1:int64):T
```````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array2D\<T\>.set(int64, int64, T):void <Array2D-T-set-int64-int64-T>`

.. _Array2D-T-set-int64-int64-T:

set(idx_0:int64, idx_1:int64, v:T):void
```````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **v**     The value to set to.

**See**
    :ref:`Array2D\<T\>.get(int64, int64):T <Array2D-T-get-int64-int64>`

.. _Array2D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 2-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array2D-T-getContainedObjects-CollectableObject:

getContainedObjects(o:CollectableObject):void
`````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array3D-T:

class Array3D<T>
----------------

Dynamic allocated 3-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array3D-T-new:

new():void
``````````

Default constructor allocates 3-dimension array with zero-length.

**See**
    :ref:`Array3D\<T\>.new(int64, int64, int64):void <Array3D-T-new-int64-int64-int64>`

.. _Array3D-T-new-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64):void
``````````````````````````````````````````````````

Constructor allocates 3-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

**See**
    :ref:`Array3D\<T\>.new():void <Array3D-T-new>`

.. _Array3D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[virtual]* *[override]*

.. _Array3D-T-clone:

clone():Array3D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone() <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[virtual]* *[override]*

.. _Array3D-T-get-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64):T
````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array3D\<T\>.set(int64, int64, int64, T):void <Array3D-T-set-int64-int64-int64-T>`

.. _Array3D-T-set-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, v:T):void
````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **v**     The value to set to.

**See**
    :ref:`Array3D\<T\>.get(int64, int64, int64):T <Array3D-T-get-int64-int64-int64>`

.. _Array3D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 3-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

**See**
    :ref:`Array3D\<T\>.size(int64):int64 <Array3D-T-size-int64>`

.. _Array3D-T-getContainedObjects-CollectableObject:

getContainedObjects(o:CollectableObject):void
`````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array4D-T:

class Array4D<T>
----------------

Dynamic allocated 4-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array4D-T-new:

new():void
``````````

Default constructor allocates 4-dimension array with zero-length.

**See**
    :ref:`Array4D\<T\>.new(int64, int64, int64, int64):void <Array4D-T-new-int64-int64-int64-int64>`

.. _Array4D-T-new-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64):void
````````````````````````````````````````````````````````````````

Constructor allocates 4-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

    **size_3** The size of 4th dimension.

**See**
    :ref:`Array4D\<T\>.new():void <Array4D-T-new>`

.. _Array4D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array4D-T-clone:

clone():Array4D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array4D-T-get-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64):T
`````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array4D\<T\>.set(int64, int64, int64, int64, T):void <Array4D-T-set-int64-int64-int64-int64-T>`

.. _Array4D-T-set-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, v:T):void
`````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **v**     The value to set to.

**See**
    :ref:`Array4D\<T\>.get(int64, int64, int64, int64):T <Array4D-T-get-int64-int64-int64-int64>`

.. _Array4D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 4-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array4D-T-getContainedObjects-CollectableObject:

getContainedObjects(o:CollectableObject):void
`````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array5D-T:

class Array5D<T>
----------------

Dynamic allocated 5-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array5D-T-new:

new():void
``````````

Default constructor allocates 5-dimension array with zero-length.

**See**
    :ref:`Array5D\<T\>.new(int64, int64, int64, int64, int64):void <Array5D-T-new-int64-int64-int64-int64-int64>`

.. _Array5D-T-new-int64-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64, size_4:int64):void
``````````````````````````````````````````````````````````````````````````````

Constructor allocates 5-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3 * size_4``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

    **size_3** The size of 4th dimension.

    **size_4** The size of 5th dimension.

**See**
    :ref:`Array5D\<T\>.new():void <Array5D-T-new>`

.. _Array5D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array5D-T-clone:

clone():Array5D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array5D-T-get-int64-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64):T
``````````````````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array5D\<T\>.set(int64, int64, int64, int64, int64, T):void <Array5D-T-set-int64-int64-int64-int64-int64-T>`

.. _Array5D-T-set-int64-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, v:T):void
``````````````````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **v**     The value to set to.

**See**
    :ref:`Array5D\<T\>.get(int64, int64, int64, int64, int64):T <Array5D-T-get-int64-int64-int64-int64-int64>`

.. _Array5D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 5-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array5D-T-getContainedObjects-CollectableObject:

getContainedObjects(o:CollectableObject):void
`````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array6D-T:

class Array6D<T>
----------------

Dynamic allocated 6-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array6D-T-new:

new():void
``````````

Default constructor allocates 6-dimension array with zero-length.

**See**
    :ref:`Array6D\<T\>.new(int64, int64, int64, int64, int64, int64):void <Array6D-T-new-int64-int64-int64-int64-int64-int64>`

.. _Array6D-T-new-int64-int64-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64, size_4:int64, size_5:int64):void
````````````````````````````````````````````````````````````````````````````````````````````

Constructor allocates 6-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3 * size_4 * size_5``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

    **size_3** The size of 4th dimension.

    **size_4** The size of 5th dimension.

    **size_5** The size of 6th dimension.

**See**
    :ref:`Array6D\<T\>.new():void <Array6D-T-new>`

.. _Array6D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array6D-T-clone:

clone():Array6D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array6D-T-get-int64-int64-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64):T
```````````````````````````````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array6D\<T\>.set(int64, int64, int64, int64, int64, int64, T):void <Array6D-T-set-int64-int64-int64-int64-int64-int64-T>`

.. _Array6D-T-set-int64-int64-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, v:T):void
```````````````````````````````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **v**     The value to set to.

**See**
    :ref:`Array6D\<T\>.get(int64, int64, int64, int64, int64, int64):T <Array6D-T-get-int64-int64-int64-int64-int64-int64>`

.. _Array6D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 6-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array6D-T-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject):void
``````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array7D-T:

class Array7D<T>
----------------

Dynamic allocated 7-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array7D-T-new:

new():void
``````````

Default constructor allocates 7-dimension array with zero-length.

**See**
    :ref:`Array7D\<T\>.new(int64, int64, int64, int64, int64, int64, int64):void <Array7D-T-new-int64-int64-int64-int64-int64-int64-int64>`

.. _Array7D-T-new-int64-int64-int64-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64, size_4:int64, size_5:int64, size_6:int64):void
``````````````````````````````````````````````````````````````````````````````````````````````````````````

Constructor allocates 7-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3 * size_4 * size_5 * size_6``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

    **size_3** The size of 4th dimension.

    **size_4** The size of 5th dimension.

    **size_5** The size of 6th dimension.

    **size_6** The size of 7th dimension.

**See**
    :ref:`Array7D\<T\>.new():void <Array7D-T-new>`

.. _Array7D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array7D-T-clone:

clone():Array7D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array7D-T-get-int64-int64-int64-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64):T
````````````````````````````````````````````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array7D\<T\>.set(int64, int64, int64, int64, int64, int64, int64, T):void <Array7D-T-set-int64-int64-int64-int64-int64-int64-int64-T>`

.. _Array7D-T-set-int64-int64-int64-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, v:T):void
````````````````````````````````````````````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

    **v**     The value to set to.

**See**
    :ref:`Array7D\<T\>.get(int64, int64, int64, int64, int64, int64, int64):T <Array7D-T-get-int64-int64-int64-int64-int64-int64-int64>`

.. _Array7D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 7-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array7D-T-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject):void
``````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array8D-T:

class Array8D<T>
----------------

Dynamic allocated 8-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array8D-T-new:

new():void
``````````

Default constructor allocates 8-dimension array with zero-length.

**See**
    :ref:`Array8D\<T\>.new(int64, int64, int64, int64, int64, int64, int64, int64):void <Array8D-T-new-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array8D-T-new-int64-int64-int64-int64-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64, size_4:int64, size_5:int64, size_6:int64, size_7:int64):void
````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Constructor allocates 8-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3 * size_4 * size_5 * size_6 * size_7``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

    **size_3** The size of 4th dimension.

    **size_4** The size of 5th dimension.

    **size_5** The size of 6th dimension.

    **size_6** The size of 7th dimension.

    **size_7** The size of 8th dimension.

**See**
    :ref:`Array8D\<T\>.new():void <Array8D-T-new>`

.. _Array8D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array8D-T-clone:

clone():Array8D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array8D-T-get-int64-int64-int64-int64-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64):T
`````````````````````````````````````````````````````````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

    **idx_7** The index of 8th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array8D\<T\>.set(int64, int64, int64, int64, int64, int64, int64, int64, T):void <Array8D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-T>`

.. _Array8D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64, v:T):void
`````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

    **idx_7** The index of 8th dimension.

    **v**     The value to set to.

**See**
    :ref:`Array8D\<T\>.get(int64, int64, int64, int64, int64, int64, int64, int64):T <Array8D-T-get-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array8D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 8-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array8D-T-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject):void
``````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array9D-T:

class Array9D<T>
----------------

Dynamic allocated 9-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array9D-T-new:

new():void
``````````

Default constructor allocates 9-dimension array with zero-length.

**See**
    :ref:`Array9D\<T\>.new(int64, int64, int64, int64, int64, int64, int64, int64, int64):void <Array9D-T-new-int64-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array9D-T-new-int64-int64-int64-int64-int64-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64, size_4:int64, size_5:int64, size_6:int64, size_7:int64, size_8:int64):void
``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Constructor allocates 9-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3 * size_4 * size_5 * size_6 * size_7 * size_8``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

    **size_3** The size of 4th dimension.

    **size_4** The size of 5th dimension.

    **size_5** The size of 6th dimension.

    **size_6** The size of 7th dimension.

    **size_7** The size of 8th dimension.

    **size_8** The size of 9th dimension.

**See**
    :ref:`Array9D\<T\>.new():void <Array9D-T-new>`

.. _Array9D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array9D-T-clone:

clone():Array9D<T>
``````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array9D-T-get-int64-int64-int64-int64-int64-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64, idx_8:int64):T
``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

    **idx_7** The index of 8th dimension.

    **idx_8** The index of 9th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array9D\<T\>.set(int64, int64, int64, int64, int64, int64, int64, int64, int64, T):void <Array9D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-int64-T>`

.. _Array9D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64, idx_8:int64, v:T):void
``````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

    **idx_7** The index of 8th dimension.

    **idx_8** The index of 9th dimension.

    **v**     The value to set to.

**See**
    :ref:`Array9D\<T\>.get(int64, int64, int64, int64, int64, int64, int64, int64, int64):T <Array9D-T-get-int64-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array9D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 9-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array9D-T-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject):void
``````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array10D-T:

class Array10D<T>
-----------------

Dynamic allocated 10-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array11D\<T\> <Array11D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array10D-T-new:

new():void
``````````

Default constructor allocates 10-dimension array with zero-length.

**See**
    :ref:`Array10D\<T\>.new(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64):void <Array10D-T-new-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array10D-T-new-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64, size_4:int64, size_5:int64, size_6:int64, size_7:int64, size_8:int64, size_9:int64):void
````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Constructor allocates 10-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3 * size_4 * size_5 * size_6 * size_7 * size_8 * size_9``.

**Parameters**
    **size_0** The size of 1st dimension.

    **size_1** The size of 2nd dimension.

    **size_2** The size of 3rd dimension.

    **size_3** The size of 4th dimension.

    **size_4** The size of 5th dimension.

    **size_5** The size of 6th dimension.

    **size_6** The size of 7th dimension.

    **size_7** The size of 8th dimension.

    **size_8** The size of 9th dimension.

    **size_8** The size of 9th dimension.

    **size_9** The size of 10th dimension.

**See**
    :ref:`Array10D\<T\>.new():void <Array10D-T-new>`

.. _Array10D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array10D-T-clone:

clone():Array10D<T>
```````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array10D-T-get-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64, idx_8:int64, idx_9:int64):T
```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

    **idx_7** The index of 8th dimension.

    **idx_8** The index of 9th dimension.

    **idx_9** The index of 10th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array10D\<T\>.set(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, T):void <Array10D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-T>`

.. _Array10D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64, idx_8:int64, idx_9:int64, v:T):void
```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0** The index of 1st dimension.

    **idx_1** The index of 2nd dimension.

    **idx_2** The index of 3rd dimension.

    **idx_3** The index of 4th dimension.

    **idx_4** The index of 5th dimension.

    **idx_5** The index of 6th dimension.

    **idx_6** The index of 7th dimension.

    **idx_7** The index of 8th dimension.

    **idx_8** The index of 9th dimension.

    **idx_9** The index of 10th dimension.

    **v**     The value to set to.

**See**
    :ref:`Array10D\<T\>.get(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64):T <Array10D-T-get-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array10D-T-size-int64:

size(idx:int64):int64
`````````````````````

Get the size of specific dimension of 10-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array10D-T-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject):void
``````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. _Array11D-T:

class Array11D<T>
-----------------

Dynamic allocated 11-dimension array in run-time.

This class represents array and provides functionality to access contained
elements. For example.

- get/set elements through index.
- iterate elements through iterator.
- etc.

**See**
    :ref:`Array\<T\> <Array-T>`

    :ref:`Array2D\<T\> <Array2D-T>`

    :ref:`Array3D\<T\> <Array3D-T>`

    :ref:`Array4D\<T\> <Array4D-T>`

    :ref:`Array5D\<T\> <Array5D-T>`

    :ref:`Array6D\<T\> <Array6D-T>`

    :ref:`Array7D\<T\> <Array7D-T>`

    :ref:`Array8D\<T\> <Array8D-T>`

    :ref:`Array9D\<T\> <Array9D-T>`

    :ref:`Array10D\<T\> <Array10D-T>`

**Extends**
    Object_

**Implements**
    GarbageCollectable_, Cloneable_

.. _Array11D-T-new:

new():void
``````````

Default constructor allocates 11-dimension array with zero-length.

**See**
    :ref:`Array11D\<T\>.new(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, int64):void <Array11D-T-new-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array11D-T-new-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64:

new(size_0:int64, size_1:int64, size_2:int64, size_3:int64, size_4:int64, size_5:int64, size_6:int64, size_7:int64, size_8:int64, size_9:int64, size_10:int64):void
```````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Constructor allocates 11-dimension array with given element count which is computed by dimensions.

The element count is equal to ``size_0 * size_1 * size_2 * size_3 * size_4 * size_5 * size_6 * size_7 * size_8 * size_9 * size_10``.

**Parameters**
    **size_0**  The size of 1st dimension.

    **size_1**  The size of 2nd dimension.

    **size_2**  The size of 3rd dimension.

    **size_3**  The size of 4th dimension.

    **size_4**  The size of 5th dimension.

    **size_5**  The size of 6th dimension.

    **size_6**  The size of 7th dimension.

    **size_7**  The size of 8th dimension.

    **size_8**  The size of 9th dimension.

    **size_8**  The size of 9th dimension.

    **size_9**  The size of 10th dimension.

    **size_10** The size of 11th dimension.

**See**
    :ref:`Array11D\<T\>.new():void <Array11D-T-new>`

.. _Array11D-T-delete:

delete():void
`````````````

Destructor releases underlying allocated memory.

*[private]* *[virtual]* *[override]*

.. _Array11D-T-clone:

clone():Array11D<T>
```````````````````

Create new instance using shallow copy.

**See**
    :ref:`Array\<T\>.clone():Array\<T\> <Array-T-clone>`

**Overrides**
    :ref:`Cloneable.clone():Cloneable <cloneable-clone>`

*[private]* *[virtual]* *[override]*

.. _Array11D-T-get-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64:

get(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64, idx_8:int64, idx_9:int64, idx_10:int64):T
`````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Get the element by indices.

**Parameters**
    **idx_0**  The index of 1st dimension.

    **idx_1**  The index of 2nd dimension.

    **idx_2**  The index of 3rd dimension.

    **idx_3**  The index of 4th dimension.

    **idx_4**  The index of 5th dimension.

    **idx_5**  The index of 6th dimension.

    **idx_6**  The index of 7th dimension.

    **idx_7**  The index of 8th dimension.

    **idx_8**  The index of 9th dimension.

    **idx_9**  The index of 10th dimension.

    **idx_10** The index of 11th dimension.

**Return**
    The element indicated by index.

**See**
    :ref:`Array11D\<T\>.set(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, T):void <Array11D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-T>`

.. _Array11D-T-set-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-T:

set(idx_0:int64, idx_1:int64, idx_2:int64, idx_3:int64, idx_4:int64, idx_5:int64, idx_6:int64, idx_7:int64, idx_8:int64, idx_9:int64, idx_10:int64, v:T):void
`````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Set the element by indices.

**Parameters**
    **idx_0**  The index of 1st dimension.

    **idx_1**  The index of 2nd dimension.

    **idx_2**  The index of 3rd dimension.

    **idx_3**  The index of 4th dimension.

    **idx_4**  The index of 5th dimension.

    **idx_5**  The index of 6th dimension.

    **idx_6**  The index of 7th dimension.

    **idx_7**  The index of 8th dimension.

    **idx_8**  The index of 9th dimension.

    **idx_9**  The index of 10th dimension.

    **idx_10** The index of 11th dimension.

    **v**      The value to set to.

**See**
    :ref:`Array11D\<T\>.get(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, int64):T <Array11D-T-get-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64-int64>`

.. _Array11D-T-size-int64:

size(idx:int64):int64
`````````````````````
Get the size of specific dimension of 11-dimension array.

**Parameters**
    **idx** The index of dimension.

**Return**
    The size of ``idx`` dimension.

**Remark**
    Dimension index is 0-indexed.

.. _Array11D-T-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject):void
``````````````````````````````````````````````

Implement this function to make garbage collector know the underlying objects.

**Parameters**
    **o** C++ only interface which collects objects referenced by native classes.

**Overrides**
    :ref:`GarbageCollectable.getContainedObjects(CollectableObject):void <GarbageCollectable-getContainedObjects-CollectableObject>`

*[private]* *[virtual]* *[override]*

.. GarbageCollectable.t

.. _GarbageCollectable:

interface GarbageCollectable
----------------------------

Interface for native classes which keep object references in internal storages.

Thor Garbage Collector keeps track of each created objects
and will take them back when they are no longer referenced. There
is no way to know if an object are finally referenced by only a C++
pointer, any usage in native code. To keep objects alive,
Thor allows native library developers to tell Garbage
Collector which objects are still in use, implement interface
GarbageCollectable.

At each checkpoints in runtime, Garbage Collector trys to call
the member function ``getContainedObjects()`` to give users a chance
claiming any reference to objects.

**See**
    CollectableObject_

.. _GarbageCollectable-getContainedObjects-CollectableObject:

getContainedObjects(o:CollectableObject):void
``````````````````````````````````````````````

Member function will be called by Garbage Collector.

Native library developers should work with the passed in
``CollectableObject`` instance to give it objects via its
member functions.

**Parameters**
    **o** Object which collects any in-use objects.

**See**
    CollectableObject_

*[virtual]*

.. _CollectableObject:

class CollectableObject
-----------------------

A CollectableObject instance which will be passed as an argument of ``GarbaeCollectable.getContainedObjects()``.

CollectableObject provides two ways to pass it object references in
C++ codes.

For example:

.. code-block:: c++

    virtual void getContainedObjects(thor::lang::CollectableObject* collector)
    {
        // by single parameter version 'add()'
        collector->add(pointer_to_object);

        // by C++ range version 'add()'
        collector->add(array, array + size);
    }

**See**
    GarbageCollectable_

.. Debug.t

.. _print-bool:

print(value:bool):void
----------------------

Write ``true`` or ``false`` to standard output.

**Parameters**
    **value** The value indicates ``true`` or ``false`` should be written to.

**See**
    :ref:`print(int8) <print-int8>`

    :ref:`print(int16) <print-int16>`

    :ref:`print(int32) <print-int32>`

    :ref:`print(int64) <print-int64>`

    :ref:`print(float32) <print-float32>`

    :ref:`print(float64) <print-float64>`

    :ref:`print(String) <print-String>`

    :ref:`println(bool) <println-bool>`

.. _print-int8:

print(value:int8):void
----------------------

Write string representation of integer value to standard output.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`print(bool) <print-bool>`

    :ref:`print(int16) <print-int16>`

    :ref:`print(int32) <print-int32>`

    :ref:`print(int64) <print-int64>`

    :ref:`print(float32) <print-float32>`

    :ref:`print(float64) <print-float64>`

    :ref:`print(String) <print-String>`

    :ref:`println(int8) <println-int8>`

.. _print-int16:

print(value:int16):void
-----------------------

Write string representation of integer value to standard output.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`print(bool) <print-bool>`

    :ref:`print(int8) <print-int8>`

    :ref:`print(int32) <print-int32>`

    :ref:`print(int64) <print-int64>`

    :ref:`print(float32) <print-float32>`

    :ref:`print(float64) <print-float64>`

    :ref:`print(String) <print-String>`

    :ref:`println(int16) <println-int16>`

.. _print-int32:

print(value:int32):void
-----------------------

Write string representation of integer value to standard output.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`print(bool) <print-bool>`

    :ref:`print(int8) <print-int8>`

    :ref:`print(int16) <print-int16>`

    :ref:`print(int64) <print-int64>`

    :ref:`print(float32) <print-float32>`

    :ref:`print(float64) <print-float64>`

    :ref:`print(String) <print-String>`

    :ref:`println(int32) <println-int32>`

.. _print-int64:

print(value:int64):void
-----------------------

Write string representation of integer value to standard output.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`print(bool) <print-bool>`

    :ref:`print(int8) <print-int8>`

    :ref:`print(int16) <print-int16>`

    :ref:`print(int32) <print-int32>`

    :ref:`print(float32) <print-float32>`

    :ref:`print(float64) <print-float64>`

    :ref:`print(String) <print-String>`

    :ref:`println(int64) <println-int64>`

.. _print-float32:

print(value:float32):void
-------------------------

Write string representation of floating value to standard output.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`print(bool) <print-bool>`

    :ref:`print(int8) <print-int8>`

    :ref:`print(int16) <print-int16>`

    :ref:`print(int32) <print-int32>`

    :ref:`print(int64) <print-int64>`

    :ref:`print(float64) <print-float64>`

    :ref:`print(String) <print-String>`

    :ref:`println(float32) <println-float32>`

.. _print-float64:

print(value:float64):void
-------------------------

Write string representation of floating value to standard output.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`print(bool) <print-bool>`

    :ref:`print(int8) <print-int8>`

    :ref:`print(int16) <print-int16>`

    :ref:`print(int32) <print-int32>`

    :ref:`print(int64) <print-int64>`

    :ref:`print(float32) <print-float32>`

    :ref:`print(String) <print-String>`

    :ref:`println(float64) <println-float64>`

.. _print-String:

print(value:String):void
------------------------

Write string to standard output.

**Parameters**
    **value** The string be written to.

**See**
    :ref:`print(bool) <print-bool>`

    :ref:`print(int8) <print-int8>`

    :ref:`print(int16) <print-int16>`

    :ref:`print(int32) <print-int32>`

    :ref:`print(int64) <print-int64>`

    :ref:`print(float32) <print-float32>`

    :ref:`print(float64) <print-float64>`

    :ref:`println(String) <println-String>`

.. _println-bool:

println(value:bool):void
------------------------

Write ``true`` or ``false`` to standard output, with new line appended.

**Parameters**
    **value** The value indicates ``true`` or ``false`` should be written to.

**See**
    :ref:`println(int8) <println-int8>`

    :ref:`println(int16) <println-int16>`

    :ref:`println(int32) <println-int32>`

    :ref:`println(int64) <println-int64>`

    :ref:`println(float32) <println-float32>`

    :ref:`println(float64) <println-float64>`

    :ref:`println(String) <println-String>`

    :ref:`print(bool) <print-bool>`

.. _println-int8:

println(value:int8):void
------------------------

Write string representation of integer value to standard output, with new line appended.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`println(bool) <println-bool>`

    :ref:`println(int16) <println-int16>`

    :ref:`println(int32) <println-int32>`

    :ref:`println(int64) <println-int64>`

    :ref:`println(float32) <println-float32>`

    :ref:`println(float64) <println-float64>`

    :ref:`println(String) <println-String>`

    :ref:`print(int8) <print-int8>`

.. _println-int16:

println(value:int16):void
-------------------------

Write string representation of integer value to standard output, with new line appended.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`println(bool) <println-bool>`

    :ref:`println(int8) <println-int8>`

    :ref:`println(int32) <println-int32>`

    :ref:`println(int64) <println-int64>`

    :ref:`println(float32) <println-float32>`

    :ref:`println(float64) <println-float64>`

    :ref:`println(String) <println-String>`

    :ref:`print(int16) <print-int16>`

.. _println-int32:

println(value:int32):void
-------------------------

Write string representation of integer value to standard output, with new line appended.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`println(bool) <println-bool>`

    :ref:`println(int8) <println-int8>`

    :ref:`println(int16) <println-int16>`

    :ref:`println(int64) <println-int64>`

    :ref:`println(float32) <println-float32>`

    :ref:`println(float64) <println-float64>`

    :ref:`println(String) <println-String>`

    :ref:`print(int32) <print-int32>`

.. _println-int64:

println(value:int64):void
-------------------------

Write string representation of integer value to standard output, with new line appended.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`println(bool) <println-bool>`

    :ref:`println(int8) <println-int8>`

    :ref:`println(int16) <println-int16>`

    :ref:`println(int32) <println-int32>`

    :ref:`println(float32) <println-float32>`

    :ref:`println(float64) <println-float64>`

    :ref:`println(String) <println-String>`

    :ref:`print(int64) <print-int64>`

.. _println-float32:

println(value:float32):void
---------------------------

Write string representation of floating value to standard output, with new line appended.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`println(bool) <println-bool>`

    :ref:`println(int8) <println-int8>`

    :ref:`println(int16) <println-int16>`

    :ref:`println(int32) <println-int32>`

    :ref:`println(int64) <println-int64>`

    :ref:`println(float64) <println-float64>`

    :ref:`println(String) <println-String>`

    :ref:`print(float32) <print-float32>`

.. _println-float64:

println(value:float64):void
---------------------------

Write string representation of floating value to standard output, with new line appended.

**Parameters**
    **value** The value of string representation to be written to.

**See**
    :ref:`println(bool) <println-bool>`

    :ref:`println(int8) <println-int8>`

    :ref:`println(int16) <println-int16>`

    :ref:`println(int32) <println-int32>`

    :ref:`println(int64) <println-int64>`

    :ref:`println(float32) <println-float32>`

    :ref:`println(String) <println-String>`

    :ref:`print(float64) <print-float64>`

.. _println-String:

println(value:String):void
--------------------------

Write string to standard output, with new line appended.

**Parameters**
    **value** The string be written to.

**See**
    :ref:`println(bool) <println-bool>`

    :ref:`println(int8) <println-int8>`

    :ref:`println(int16) <println-int16>`

    :ref:`println(int32) <println-int32>`

    :ref:`println(int64) <println-int64>`

    :ref:`println(float32) <println-float32>`

    :ref:`println(float64) <println-float64>`

    :ref:`print(String) <print-String>`

.. Flag.t

.. _Flag:

class Flag
----------

This class helps users define/parse command line options.

One may pass command line options via ``--args`` option on launch of entry
task. A *name-value pair option* or a *positional option* can be
defined. A name-value pair option is given by a name string prefixed with
``--``, optionally followed by a ‘=’, and then a value string. A positional
option is a series of values. For example:

.. code-block:: bash

    tsc r test --args --arg1=val1 --arg2=val2         # name-value pairs with '='
    tsc r test --args --arg1 val1 --arg2 val2         # name-value pairs without '='
    tsc r test --args 1 2 3 4                         # positional options
    tsc r test --args --arg1=val1 1 2 --arg2 val2 3 4 # mixed

Flag can support three types of values: integer, float, and string:

.. code-block:: bash

    tsc r test --args --arg=123        # integer value
    tsc r test --args --arg=3.14159    # float value
    tsc r test --args --arg="Zillians" # string value

The following code shows the basic usage:

.. code-block:: javascript

    @entry
    task hello_flag()
    {
        const f = new Flag();                        // 1. Create Flag class instance
        if (!f.createInteger("tint","Test integer")) // 2. Define options via ``create`` series methods
            println("Failed to create option");

        if (f.parse())                               // 3. Call parse().
        {
            const v : int64 = f.getInteger("tint");  // 4. Use ``get`` series methods to query values.
            println("Integer: \{v}");
            exit(0);
        }
        else
        {
            exit(-1);
        }
    }

A name-pair option can be set as a required option with the third optional
parameter:

.. code-block:: javascript

    f.createInteger("tint", "test", true); // required option
    f.createFloat("tfloat", "test");       // default as optional

A positional option can be set as a required option as well. Additionally,
a required number of values can be specified:

.. code-block:: javascript

    f.createPositionalInteger("pint", "test", 3, true); // 3 integers are required

**Extends**
    Object_

.. _Flag-new:

new():void
``````````

Constructor which creates command line parser with no options.

.. _Flag-delete:

delete():void
`````````````

Destructor releases resources.

*[private]* *[virtual]* *[override]*

.. _Flag-createInteger-String-String-bool:

createInteger(name:String, description:String, required:bool = false):bool
``````````````````````````````````````````````````````````````````````````

Define name-value pair option with type int64.

**Parameters**
    **name**        The name of option.

    **description** The option description that will be shown in helping text.

    **required**    Set if this option is required or not.

**Return**
    Boolean value indicates such options is added successfully or not.

**Return Value**
    **true**  Success.

    **false** If *name* is empty or option is already created.

.. _Flag-createFloat-String-String-bool:

createFloat(name:String, description:String, required:bool = false):bool
````````````````````````````````````````````````````````````````````````

Define name-value pair option with type float64.

**Parameters**
    **name**        The name of option.

    **description** The option description that will be shown in helping text.

    **required**    Set if this option is required or not.

**Return**
    Boolean value indicates such options is added successfully or not.

**Return Value**
    **true**  Success.

    **false** If *name* is empty or option is already created.

.. _Flag-createString-String-String-bool:

createString(name:String, description:String, required:bool = false):bool
`````````````````````````````````````````````````````````````````````````

Define name-value pair option with type String.

**Parameters**
    **name**        The name of option.

    **description** The option description that will be shown in helping text.

    **required**    Set if this option is required or not.

**Return**
    Boolean value indicates such option is added successfully or not.

**Return Value**
    **true**  Success.

    **false** If *name* is empty or option is already created.

.. _Flag-createPositionalInteger-String-String-int32-bool:

createPositionalInteger(name:String, description:String, count:int32 = -1, required:bool = false):bool
``````````````````````````````````````````````````````````````````````````````````````````````````````

Define positional command line option with type int64.

**Parameters**
    **name**        The name of the option.

    **description** The option description that will be shown in helping text.

    **count**       The maximum number of values can be given. -1 if no limit.

    **required**    Set if this option is required or not.

**Return**
    Boolean value indicates such option is added successfully or not.

**Return Value**
    **true**  Success.

    **false** If *name* is empty or option is already created.

.. _Flag-createPositionalFloat-String-String-int32-bool:

createPositionalFloat(name:String, description:String, count:int32 = -1, required:bool = false):bool
````````````````````````````````````````````````````````````````````````````````````````````````````

Define positional command line option with type float64.

**Parameters**
    **name**        The name of the option.

    **description** The option description that will be shown in helping text.

    **count**       The maximum number of values can be given. -1 if no limit.

    **required**    Set if this option is required or not.

**Return**
    Boolean value indicates such option is added successfully or not.

**Return Value**
    **true**  Success.

    **false** If *name* is empty or option is already created.

.. _Flag-createPositionalString-String-String-int32-bool:

createPositionalString(name:String, description:String, count:int32 = -1, required:bool = false):bool
`````````````````````````````````````````````````````````````````````````````````````````````````````

Define positional command line option with type String.

**Parameters**
    **name**        The name of the option.

    **description** The option description that will be shown in helping text.

    **count**       The maximum number of values can be given. -1 if no limit.

    **required**    Set if this option is required or not.

**Return**
    Boolean value indicates such option is added successfully or not.

**Return Value**
    **true**  Success.

    **false** If *name* is empty or option is already created.

.. _Flag-parse:

parse():bool
````````````

Parse the given command line options.

Command line parsing will fail if:

- An undefined option is encountered.
- An required option is not given.
- The number of values given to a positional option exceeds the
  user-defined limit.
- The type of a given value mismatches the defined type.

**Return**
    Boolean value indicates successfully parsing command line options.

**Return Value**
    **true**  Successfully parse options.

    **false** Failed to parse options.

**Remark**
    has/get series functions can be in use after this function return ``true``.

.. _Flag-has-String:

has(name:String):bool
`````````````````````

Query if the name option is given.

**Parameters**
    **name** The name of option to be queried.

**Return**
    Boolean value indicates option of *name* is given.

**Return Value**
    **true**  Be given.

    **false** Not be given.

.. _Flag-getInteger-String:

getInteger(name:String):int64
`````````````````````````````

Query the value of the *name-value* pair option with type int64.

**Parameters**
    **name** The name of the queried option.

**Return**
    The given value is returned or an empty value is returned if
    the option is not given.

.. _Flag-getFloat-String:

getFloat(name:String):float64
`````````````````````````````

Query the value of the *name-value* pair option with type float64.

**Parameters**
    **name** The name of the queried option.

**Return**
    The given value is returned or an empty value is returned if
    the option is not given.

.. _Flag-getString-String:

getString(name:String):String
`````````````````````````````

Query the value of the *name-value* pair option with type String.

**Parameters**
    **name** The name of the queried option.

**Return**
    The given value is returned or an empty value is returned if
    the option is not given.

.. _Flag-getPositionalInteger-String:

getPositionalInteger(name:String):thor.container.Vector<int64>
``````````````````````````````````````````````````````````````

Query the value of the name positional option with type int64.

**Parameters**
    **name** The name of the queried option.

**Return**
    The given value is returned as a thor.container.Vector. An
    empty vector is returned if the option is not given.

.. _Flag-getPositionalFloat-String:

getPositionalFloat(name:String):thor.container.Vector<float64>
``````````````````````````````````````````````````````````````

Query the value of the name positional option with type float64.

**Parameters**
    **name** The name of the queried option.

**Return**
    The given value is returned as a thor.container.Vector. An
    empty vector is returned if the option is not given.

.. _Flag-getPositionalString-String:

getPositionalString(name:String):thor.container.Vector<String>
``````````````````````````````````````````````````````````````

Query the value of the name positional option with type String.

**Parameters**
    **name** The name of the queried option.

**Return**
    The given value is returned as a thor.container.Vector. An
    empty vector is returned if the option is not given.

.. _Flag-getRaw:

getRaw():thor.container.Vector<String>
``````````````````````````````````````

Retrieve the unprocessed command line options.

**Return**
    A vector stores all command line options string.

.. _Flag-help:

help():String
`````````````

Generate help string for this parser.

**Return**
    A structured helping text string according to the defined
    command line options.

.. Lambda.t

.. _Lambda:

class Lambda
------------

Base class for lambda holders.

This class is used internally and do not provide any functionality for
Thor. So, user may get wrong to start if trying to use this class.

**Extends**
    Object_

.. _Lambda0-R:

class Lambda0<R>
----------------

Lambda holder for zero-argument lambdas which returns *R*.

The lambda holder is provided to define variable type for lambdas. For
example:

.. code-block:: javascript

    var lambda_1: Lambda0<void>;
    var lambda_2: lambda(): void; // equivalent to Lambda0<void>

**Remark**
    It's suggested to use keyword one (on *lambda_2*) to declare lambda
    holders.

**See**
    :ref:`Lambda1\<R, T0\> <Lambda1-R-T0>`

    :ref:`Lambda2\<R, T0, T1\> <Lambda2-R-T0-T1>`

**Extends**
    Lambda_

.. _Lambda0-R-new-T-T:

new<T>(o:T):void
``````````````````

Constructor creates lambda holders according to the given lambda object.

This constructor is provided for lambda feature provided by Thor.
Users should not use this function directly.

.. _Lambda0-R-delete:

delete():void
`````````````

Destructor releases referenced lambda object.

*[private]* *[virtual]* *[override]*

.. _Lambda0-R-invoke:

invoke():R
``````````

Helper to invoke the real lambda function from lambda object.

User could call the real lambda function by the following code:

.. code-block:: javascript

    var l = lambda(): void { ... };
    l();        // equivalent to l.invoke()
    l.invoke(); // equivalent to l()

**Remark**
    Thor will generate ".invoke" for objects, that is,
    similar functionality of functors, by providing this function.

.. _Lambda1-R-T0:

class Lambda1<R, T0>
--------------------

Lambda holder for one-argument lambdas which returns *R*.

The lambda holder is provided to define variable type for lambdas. For
example:

.. code-block:: javascript

    var lambda_1: Lambda1<void, int32>;
    var lambda_2: lambda(int32): void;  // equivalent to Lambda1<void, int32>

**Remark**
    It's suggested to use keyword one (on *lambda_2*) to declare lambda
    holders.

**See**
    :ref:`Lambda0\<R\> <Lambda0-R>`

    :ref:`Lambda2\<R, T0, T1\> <Lambda2-R-T0-T1>`

**Extends**
    Lambda_

.. _Lambda1-R-T0-new-T-T:

new<T>(o:T):void
````````````````

Constructor creates lambda holders according to the given lambda object.

This constructor is provided for lambda feature provided by Thor.
Users should not use this function directly.

.. _Lambda1-R-T0-delete:

delete():void
`````````````

Destructor releases referenced lambda object.

*[private]* *[virtual]* *[override]*

.. _Lambda1-R-T0-invoke:

invoke(t0:T0):R
```````````````

Helper to invoke the real lambda function from lambda object.

User could call the real lambda function by the following code:

.. code-block:: javascript

    var l = lambda(int32): void { ... };
    l(1);        // equivalent to l.invoke(1)
    l.invoke(1); // equivalent to l(1)

**Remark**
    Thor will generate ".invoke" for objects, that is,
    similar functionality of functors, by providing this function.

.. _Lambda2-R-T0-T1:

class Lambda2<R, T0, T1>
------------------------

Lambda holder for two-argument lambdas which returns *R*.

The lambda holder is provided to define variable type for lambdas. For
example:

.. code-block:: javascript

    var lambda_1: Lambda2<void, int32, float32>;
    var lambda_2: lambda(int32, float32): void;  // equivalent to Lambda2<void, int32, float32>

**Remark**
    It's suggested to use keyword one (on *lambda_2*) to declare lambda
    holders.

**See**
    :ref:`Lambda0\<R\> <Lambda0-R>`

    :ref:`Lambda1\<R, T0\> <Lambda1-R-T0>`

**Extends**
    Lambda_

.. _Lambda2-R-T0-T1-new-T-T:

new<T>(o:T):void
````````````````

Constructor creates lambda holders according to the given lambda object.

This constructor is provided for lambda feature provided by Thor.
Users should not use this function directly.

.. _Lambda2-R-T0-T1-delete:

delete():void
`````````````

Destructor releases referenced lambda object.

*[private]* *[virtual]* *[override]*

.. _Lambda2-R-T0-T1-invoke:

invoke(t0:T0, t1:T1):R
``````````````````````

Helper to invoke the real lambda function from lambda object.

User could call the real lambda function by the following code:

.. code-block:: javascript

    var l = lambda(int32, float32): void { ... };
    l(1, 1.1f);        // equivalent to l.invoke(1, 1.1f)
    l.invoke(1, 1.1f); // equivalent to l(1, 1.1f)


**Remark**
    Thor will generate ".invoke" for objects, that is,
    similar functionality of functors, by providing this function.

.. Process.t

.. _exit-int32:

exit(exit_code:int32):void
--------------------------

Terminate Thor VM.

User needs to call this function in order to terminate a Thor VM.

That is, entry function will not terminate execution after returned by
design.

**Parameters**
    **exit_code** Exit code will be propagate as Thor VM's exit code.

**Remark**
    This function will not return.

    Thor VM will try to exit as soon as possible, but there are
    some cases it can't. User should not expect the termination will
    take effect right after this call.

.. Replication.t

.. _ReplicationBase:

class ReplicationBase
---------------------

Base class carries common data in both replication encoding and decoding.

This class is for **ReplicationEncoder** and **ReplicationDecoder** only.
Users should not use this class directly.

**See**
    ReplicationEncoder_

    ReplicationDecoder_

**Extends**
    Object_

.. _ReplicationEncoder:

class ReplicationEncoder
------------------------

Class provides actions which dump value into raw data.

For the following types:

- Primitive values
- Objects

This class defines how they are dumped, then provide methods (overloaded
with name ``put``) to dump them into raw data.

If encoding is failure, ``put`` will return ``false``. For example, ``false``
will be returned if there is no more memory to store raw data.

**See**
    ReplicationDecoder_

**Extends**
    ReplicationBase_

.. _ReplicationEncoder-new:

new():void
``````````

Default constructor initializes instance to empty state.

.. _ReplicationEncoder-delete:

delete():void
`````````````

Destructor releases resources.

*[private]* *[virtual]* *[override]*

.. _ReplicationEncoder-put-bool:

put(b:bool):bool
`````````````````

Dump boolean value into raw data.

**Parameters**
    **b** Boolean value to be dumped.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``b``.

**See**
    :ref:`ReplicationEncoder.put(int8):bool <ReplicationEncoder-put-int8>`

    :ref:`ReplicationEncoder.put(int16):bool <ReplicationEncoder-put-int16>`

    :ref:`ReplicationEncoder.put(int32):bool <ReplicationEncoder-put-int32>`

    :ref:`ReplicationEncoder.put(int64):bool <ReplicationEncoder-put-int64>`

    :ref:`ReplicationEncoder.put(float32):bool <ReplicationEncoder-put-float32>`

    :ref:`ReplicationEncoder.put(float64):bool <ReplicationEncoder-put-float64>`

.. _ReplicationEncoder-put-int8:

put(i8:int8):bool
`````````````````

Dump 8-bit integer value into raw data.

**Parameters**
    **i8** 8-bit integer value to be dumped.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``i8``.

**See**
    :ref:`ReplicationEncoder.put(bool):bool <ReplicationEncoder-put-bool>`

    :ref:`ReplicationEncoder.put(int16):bool <ReplicationEncoder-put-int16>`

    :ref:`ReplicationEncoder.put(int32):bool <ReplicationEncoder-put-int32>`

    :ref:`ReplicationEncoder.put(int64):bool <ReplicationEncoder-put-int64>`

    :ref:`ReplicationEncoder.put(float32):bool <ReplicationEncoder-put-float32>`

    :ref:`ReplicationEncoder.put(float64):bool <ReplicationEncoder-put-float64>`

.. _ReplicationEncoder-put-int16:

put(i16:int16):bool
```````````````````

Dump 16-bit integer value into raw data.

**Parameters**
    **i16** 16-bit integer value to be dumped.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``i16``.

**See**
    :ref:`ReplicationEncoder.put(bool):bool <ReplicationEncoder-put-bool>`

    :ref:`ReplicationEncoder.put(int8):bool <ReplicationEncoder-put-int8>`

    :ref:`ReplicationEncoder.put(int32):bool <ReplicationEncoder-put-int32>`

    :ref:`ReplicationEncoder.put(int64):bool <ReplicationEncoder-put-int64>`

    :ref:`ReplicationEncoder.put(float32):bool <ReplicationEncoder-put-float32>`

    :ref:`ReplicationEncoder.put(float64):bool <ReplicationEncoder-put-float64>`

.. _ReplicationEncoder-put-int32:

put(i32:int32):bool
```````````````````

Dump 32-bit integer value into raw data.

**Parameters**
    **i32** 32-bit integer value to be dumped.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``i32``.

**See**
    :ref:`ReplicationEncoder.put(bool):bool <ReplicationEncoder-put-bool>`

    :ref:`ReplicationEncoder.put(int8):bool <ReplicationEncoder-put-int8>`

    :ref:`ReplicationEncoder.put(int16):bool <ReplicationEncoder-put-int16>`

    :ref:`ReplicationEncoder.put(int64):bool <ReplicationEncoder-put-int64>`

    :ref:`ReplicationEncoder.put(float32):bool <ReplicationEncoder-put-float32>`

    :ref:`ReplicationEncoder.put(float64):bool <ReplicationEncoder-put-float64>`

.. _ReplicationEncoder-put-int64:

put(i64:int64):bool
```````````````````

Dump 64-bit integer value into raw data.

**Parameters**
    **i64** 64-bit integer value to be dumped.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``i64``.

**See**
    :ref:`ReplicationEncoder.put(bool):bool <ReplicationEncoder-put-bool>`

    :ref:`ReplicationEncoder.put(int8):bool <ReplicationEncoder-put-int8>`

    :ref:`ReplicationEncoder.put(int16):bool <ReplicationEncoder-put-int16>`

    :ref:`ReplicationEncoder.put(int32):bool <ReplicationEncoder-put-int32>`

    :ref:`ReplicationEncoder.put(float32):bool <ReplicationEncoder-put-float32>`

    :ref:`ReplicationEncoder.put(float64):bool <ReplicationEncoder-put-float64>`

.. _ReplicationEncoder-put-float32:

put(f32:float32):bool
`````````````````````

Dump 32-bit floating value into raw data.

**Parameters**
    **f32** 32-bit floating value to be dumped.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``f32``.

**See**
    :ref:`ReplicationEncoder.put(bool):bool <ReplicationEncoder-put-bool>`

    :ref:`ReplicationEncoder.put(int8):bool <ReplicationEncoder-put-int8>`

    :ref:`ReplicationEncoder.put(int16):bool <ReplicationEncoder-put-int16>`

    :ref:`ReplicationEncoder.put(int32):bool <ReplicationEncoder-put-int32>`

    :ref:`ReplicationEncoder.put(int64):bool <ReplicationEncoder-put-int64>`

    :ref:`ReplicationEncoder.put(float64):bool <ReplicationEncoder-put-float64>`

.. _ReplicationEncoder-put-float64:

put(f64:float64):bool
`````````````````````

Dump 64-bit floating value into raw data.

**Parameters**
    **f64** 64-bit floating value to be dumped.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``f64``.

**See**
    :ref:`ReplicationEncoder.put(bool):bool <ReplicationEncoder-put-bool>`

    :ref:`ReplicationEncoder.put(int8):bool <ReplicationEncoder-put-int8>`

    :ref:`ReplicationEncoder.put(int16):bool <ReplicationEncoder-put-int16>`

    :ref:`ReplicationEncoder.put(int32):bool <ReplicationEncoder-put-int32>`

    :ref:`ReplicationEncoder.put(int64):bool <ReplicationEncoder-put-int64>`

    :ref:`ReplicationEncoder.put(float32):bool <ReplicationEncoder-put-float32>`

.. _ReplicationEncoder-put-Object:

put(o:Object):bool
``````````````````

Dump objects into raw data.

**Parameters**
    **o** Instance to be dumped. **null** is acceptable.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``o``.

**See**
    :ref:`ReplicationEncoder.put\<T\>(T):bool <ReplicationEncoder-put-T-T>`

.. _ReplicationEncoder-put-T-T:

put<T>(t:T):bool
````````````````

Dump objects into raw data.

This function is provided as template, in order to dump instance through
interfaces which not inherits from thor.lang.Object.

**Parameters**
    **t** Instance to be dumped. **null** is acceptable.

**Return**
    Boolean value indicates that value is successfully dumped or not.

**Return Value**
    **true**  Dumped successfully.

    **false** Failed to dump value of ``t``.

**See**
    :ref:`ReplicationEncoder.put(Object):bool <ReplicationEncoder-put-Object>`

.. _ReplicationDecoder:

class ReplicationDecoder
------------------------

Class provides actions which restore value from raw data.

For the following types:
- Primitive values
- Objects

This class defines how they are restored, then provide methods (overloaded
with name ``get``) to restore them from raw data. Such methods use multiple
return value to get value and with additional boolean value which indicates
value is successfully decoded or not.

For more details, decoder will keep track of references which refer to the
same instance. So if there are more than one references refer to instance
``obj``. Decoder will return the same reference as expected.

**Remark**
    Note ``get`` is overloaded by single dummy argument since overloading
    is not allowed on return type only.

**See**
    ReplicationEncoder_

**Extends**
    ReplicationBase_

.. _ReplicationDecoder-new:

new():void
``````````

Default constructor initializes instance to empty state

.. _ReplicationDecoder-delete:

delete():void
`````````````

Destructor releases resources.

*[private]* *[virtual]* *[override]*

.. _ReplicationDecoder-get-bool:

get(unused:bool):(bool, bool)
`````````````````````````````

Restore boolean value from raw data.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored boolean value.

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-get-int8:

get(unused:int8):(bool, int8)
`````````````````````````````

Restore 8-bit integer value from raw data.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored 8-bit integer value.

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-get-int16:

get(unused:int16):(bool, int16)
```````````````````````````````

Restore 16-bit integer value from raw data.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored 16-bit integer value.

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-get-int32:

get(unused:int32):(bool, int32)
```````````````````````````````

Restore 32-bit integer value from raw data.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored 32-bit integer value.

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-get-int64:

get(unused:int64):(bool, int64)
```````````````````````````````

Restore 64-bit integer value from raw data.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored 64-bit integer value.

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-get-float32:

get(unused:float32):(bool, float32)
```````````````````````````````````

Restore 32-bit floating value from raw data.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored 32-bit floating value.

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-get-float64:

get(unused:float64):(bool, float64)
```````````````````````````````````

Restore 64-bit floating value from raw data.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored 64-bit floating value.

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-get-T-T:

get<T>(unused:T):(bool, T)
``````````````````````````

Restore object instance from raw data.

This function is provided as template, in order to restore instance to
interfaces which not inherits from thor.lang.Object.

**Parameters**
    **unused** Unused value for function overloading only.

**Return**
    Value pair where first boolean value indicates second value is
    successfully restored or not.

**Return Value**
    **(true, any)** Restored value successfully, second value is the restored object instance (``null`` is a valid value).

    **(false, any)** Failed to restore value, second value is meaningless.

.. _ReplicationDecoder-isEnded:

isEnded():bool
``````````````

Test if there is no more data for deserialization.

**Return**
    Boolean value indicates there is no more data or not.

**Return Value**
    **true**  No more data.

    **false** There is more data.
