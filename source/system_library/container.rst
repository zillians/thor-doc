thor.container
==============

.. _Vector:

class Vector<E>
---------------

An array-like class which can contain components that can
be accessed using an integer index.

Vector is dynamic-sized array. Users can put new components
into it in runtime. Each Vector tries to optimize storage
management by maintaining a capacity(). The capacity is always at
least as large as the Vector size(). It is usually larger
because it prepares more rooms for upcoming added elements,
to avoid frequently request for memory from system. When addition
is made and there is no more spaces to keep the new element,
Vector doubles its capacity in order to allow increasing
size.

**See**
    :ref:`VectorIterator\<E\> <VectorIterator>`

**Extends**
    :ref:`Object <Object>`

.. _Vector-new:

new(): void
```````````

Constructor creates Vector with empty storage.

This constructor creates a zero-capacity and zero-sized Vector

.. _Vector-new-int64:

new(init_size: int64): void
```````````````````````````

Constructor creates Vector by given size.

**Parameters**
    **init_size** create a Vector with size = **init_size** and capacity = **init_size**. All elements are uninitialized.

.. _Vector-delete:

delete(): void
``````````````

Destructor which releases all resources hold by instance.

*[virtual]*

.. _Vector-create:

create() : Vector<E>
````````````````````

Provides functionality to create empty vectors in C++.

**Return**
    An empty vector instance.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _Vector-create-int64:

create(init_size : int64) : Vector<E>
`````````````````````````````````````

Provides functionality to create vectors with specific size in C++.

**Return**
    A vector instance with size = **init_size** and capacity = **init_size**.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _Vector-clone:

clone(): Vector<E>
``````````````````

Create a copy of this.

**Return**
    A newly created Vector instance with same
    size, same capacity and same element values.

**Remark**
    This function performs **shallow copy**. That means
    only elements' values are copied. For non-primitive
    types, elements in the produced Vector shares same
    objects with this.

*[virtual]*

.. _Vector-pushBack:

pushBack(v: E): void
````````````````````

Add a new value into this.

Resize this to keep the pushed new value.

**Parameters**
    **v** The new value will be placed into this.

**Remark**
    This member function causes memory reallocation
    while capacity is equal to the size. Any
    reallocation causes already-created iterators
    invalidated.

.. _Vector-popBack:

popBack(): void
```````````````

Remove the last element.

Remove the last element (at index [0, N), N is the size
of this) if this is not empty. Otherwise, this status
is unchanged.

.. _Vector-set-int64-E:

set(index: int64, v: E): void
`````````````````````````````

Assign a new value to the element at specified index.

**Parameters**
    **index** Index of the element.

    **v**     New value to overwrite the element.

**Remark**
    If the index is greater than or equal to
    size() or negative, function behavior is undefined.

.. _Vector-get-int64:

get(index: int64): E
````````````````````

Get element at specified index.

**Parameters**
    **index** Index of the element.

**Return**
    The value of requested element.

**Remark**
    If the index is greater than or equal to
    size() or negative, function behavior is undefined.

.. _Vector-iterator:

iterator(): VectorIterator<E>
`````````````````````````````

Returns an iterator.

**Return**
    An iterator which points to this.

.. _Vector-size:

size(): int64
`````````````

Return the number of elements.

**Return**
    The number of elements in this.

.. _Vector-capacity:

capacity(): int64
`````````````````

Return the number of elements that can be held in currently allocated storage.

**Return**
    Capacity of the currently allocated storage.

.. _Vector-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject): void
```````````````````````````````````````````````

Member function will be called by garbage collector to
keep track objects which are still referenced in native
implementations.

**Parameters**
    **o** An object which collects still-referenced objects.

*[virtual]*

.. _VectorIterator:

class VectorIterator<E>
-----------------------

Helper class which supports navigating Vector<E> elements in foreach loop.

In foreach loop, Thor will call Vector<E> 's  member function
**iterator()** to get a VectorIterator<E> instance(shorten as **it** later).
Then Thor calls **it** 's member function **hasNext()** to determine
to terminate executing loop or not. In each iteration, Thor gets an
element by calling **it** 's member function **next()**.

**Remark**
    The behavior is undefined if the pointed Vector has been modified
    after iterator was returned.

**See**
    :ref:`Vector\<E\> <Vector>`

**Extends**
    :ref:`Object <Object>`

.. _VectorIterator-new-Vector:

new(c: Vector<E>): void
```````````````````````

Constructor creates an iterator which points to a vector.

**Parameters**
     **c** The target Vector this newly created iterator will point to.

.. _VectorIterator-delete:

delete(): void
``````````````

Destructor.

*[virtual]*

.. _VectorIterator-create-Vector-E:

create(c : Vector<E>) : VectorIterator<E>
`````````````````````````````````````````

Provides functionality to create iterators in C++.

**Parameters**
    **c** The target vector to navigate.

**Return**
    A VectorIterator instance which can work on parameter Vector.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _VectorIterator-clone:

clone(): VectorIterator<E>
``````````````````````````

Get a copy of this.

Get an iterator which has same status as this, both of them
point to the same Vector and same element in it.

**Return**
    A copy of this.

**Remark**
    This function performs **shallow copy**. That means
    the returned iterator will share the same vector with
    this.

*[virtual]*

.. _VectorIterator-hasNext:

hasNext(): bool
```````````````

Tell if there is any element not visited yet in
the target Vector this points to.

**Return**
    A boolean value indicates visiting status.

**Return Value**
    **true** There is another element not visited yet.

    **false** All elements are visited.

.. _VectorIterator-next:

next(): E
`````````

Get the next unvisited element in Vector.

**Return**
    Next unvisited element.

.. _Map:

class Map<K, V>
---------------

A sorted associative container which maps a unique
key to a value.

Map is an associative container which stores immutable type
as keys. Map are indexed by keys and keep elements the keys
map to. Keys are sorted in map, to be used as a key, user
defined types should provide a public member function
named isLessThan for comparision. For example, a type
Foo is written as:

.. code-block:: javascript

    class Foo
    {
        public function isLessThan(other: Foo): bool
        {
            return this.value < other.value;
        }

        private var value: int32;
    }

Foo should be convertible to isLessThan() 's parameter,
and the isLessThan() return type should be convertible
to bool. No limits on the value types.

**See**
    :ref:`MapIterator\<K, V\> <MapIterator>`

    :ref:`MapEntry\<K, V\> <MapEntry>`

**Extends**
    :ref:`Object <Object>`

.. _Map-new:

new(): void
```````````

Constructor creates Map with no entries.

.. _Map-delete:

delete(): void
``````````````

Destructor which releases all resources hold by instance.

*[virtual]*

.. _Map-create:

create(): Map<K, V>
```````````````````

Provides functionality to create empty maps in C++.

**Return**
    An empty map instance.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _Map-clone:

clone(): Map<K, V>
``````````````````

Create a copy of ``this``.

**Return**
    A newly created Map instance with same keys and values.

**Remark**
    This function performs **shallow copy**. That means
    only elements' values are copied. For non-primitive
    types, elements in the produced Map share same
    objects with this.

.. _Map-get-K:

get(k: K): (V, bool)
````````````````````

Get a mapped value by a given key.

**Return**
    A value-result pair, first value is the mapped
    and second boolean value indicates if the key
    was found in map or not.

**Return Value**
    **(any, true)** : Found key in map, and first is its value.

    **(any, false)** : Can not find key in map, first value is meaningless.

**Remark**
    Map guarantees the time complexity is log(N)
    on finding keys in map. (N is number of total
    keys)

.. _Map-set-K-V:

set(k: K, v: V): void
`````````````````````

Add a new key and its value.

Add a new key into map and set its value. If the key is
already in map, rewrite the mapped value by passed one.

**Parameters**
    **k** The key we want to add to map.

    **v** The value we want to map by key **k**.

**Remark**
    Map guarantees the time complexity is log(N)
    on finding keys in map. (N is number of total
    keys)

.. _Map-remove-K:

remove(k: K): void
``````````````````

Remove a key from map.

Remove a key and the value it maps from map. If the key
is not find in map, do nothing.

**Parameters**
    **k** The key we want to remove from map.

**Remark**
    Map guarantees the time complexity is log(N)
    on finding keys in map. (N is number of total
    keys)

.. _Map-iterator:

iterator(): MapIterator<K, V>
`````````````````````````````

Returns an iterator.

**Return**
    An iterator which points to ``this``.

.. _Map-has-K:

has(k: K): bool
```````````````

Tell if the given key is in map.

**Parameters**
    **k** The key we want to find.

**Return**
    Boolean value indicates  if key is in map.

**Return Value**
    **true** If the key is exist in map.

    **false** If the key is not in map.

**Remark**
    Map guarantees the time complexity is log(N)
    on finding keys in map. (N is number of total
    keys)

.. _Map-empty:

empty(): bool
`````````````

Tell if there is no keys in map.

What this function does is equivalent to call:

.. code-block:: javascript

    return this.size() == 0;

**Return**
    Boolean value indicates if ``this`` has no
    keys.

**Return Value**
    **true** No keys stay in this.

    **false** At least one key is in this.

.. _Map-size:

size(): int64
`````````````

Return the numbers of keys in this.

**Return**
    The number of keys in this.

.. _Map-getContainedObjects-CollectableObject:

getContainedObjects(o: CollectableObject): void
```````````````````````````````````````````````

Member function will be called by garbage collector to
keep track objects which are still referenced in native
implementations.

**Parameters**
    **c** An object which collects still-be-referenced objects.

*[virtual]*

.. _MapIterator:

class MapIterator<K, V>
-----------------------

Helper class which supports navigating key and values
and put them together by using MapEntry as view.

In foreach loop, Thor will call Map 's  member function
iterator() to get a MapIterator instance(shorten as it later).
Then Thor calls it 's member function hasNext() to determine
to terminate executing loop or not. In each iteration, Thor gets an
key-value pair by calling it 's member function next().

**Remark**
    The behavior is undefined if backing map has been
    modified after the iterator was returned.

**See**
    :ref:`Map\<K, V\> <Map>`

    :ref:`MapEntry\<K, V\> <MapEntry>`

**Extends**
    :ref:`Object <Object>`

.. _MapIterator-new-Map:

new(c: Map<K, V>): void
```````````````````````

Constructor creates an iterator which points to a map.

**Parameters**
    **c** The target Map this newly created iterator will point to.

.. _MapIterator-delete:

delete(): void
``````````````

Destructor which releases all resources hold by instance.

*[virtual]*

.. _MapIterator-create-Map-K-V:

create(c : Map<K, V>): MapIterator<K, V>
````````````````````````````````````````

Provides functionality to create iterators in C++.

**Parameters**
    **c** The target map to navigate.

**Return**
    An iterator instance which can work on **c**.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _MapIterator-clone:

clone(): MapIterator<K, V>
``````````````````````````

Get a copy of this.

Get an iterator which has same status as this, both of them
point to the same Map and same element in it.

**Return**
    A copy of this.

**Remark**
    This function performs **shallow copy**. That means
    the returned iterator will share the same map with
    this.

*[virtual]*

.. _MapIterator-hasNext:

hasNext(): bool
```````````````

Tell if there is any entry not visited yet in
the target Map ``this`` points to.

**Return**
    A boolean value indicates visiting status.

**Return Value**
    **true** If there is another key not visited yet.

    **false** All keys are visited.

.. _MapIterator-next:

next(): MapEntry<K, V>
``````````````````````

Get the next unvisited key and its value together
as a pair in Map.

**Return**
    A MapEntry instance which wraps unvisited key and
    associated value.

.. _MapEntry:

class MapEntry<K, V>
--------------------

A class wraps key and associated value together.

Usually this class is instantiated by MapIterator to
put key and value as a pair in loop iterations. An entry
object should be used only for the duration of the
iteration.

**Remark**
    The behavior is undefined if backing map has been
    modified after the entry was returned by the iterator.

**See**
    :ref:`Map\<K, V\> <Map>`

    :ref:`MapIterator\<K, V\> <MapIterator>`

**Extends**
    :ref:`Object <Object>`

.. _MapEntry-new-MapEntry:

new(e: MapEntry<K, V>): void
````````````````````````````

Construct a copy from given map entry.

**Parameters**
    **e** The source entry to copy from.

.. _MapEntry-new-K-V:

new(k: K, v: V): void
`````````````````````

Wrap key and value together.

**Parameters**
    **k** The map key.

    **v** The associated value.

.. _MapEntry-create-K-V:

create(k : K, v : V): MapEntry<K, V>
````````````````````````````````````

Provides functionality to create entries in C++.

**Parameters**
    **k** The map key.

    **v** The associated value.

**Return**
    An entry instance which has given arguments.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _MapEntry-key:

key: K
``````

Wrapped map key.

.. _MapEntry-value:

value: V
````````

Wrapped map value.

.. _HashMap:

class HashMap<K, V>
-------------------

A Hash Function based data structure that maps keys to values.

HashMap is an associative container which stores immutable type
as keys. HashMap are indexed by keys and keep elements the keys
map to. Keys are hashed in map, to be used as a key, user defined
types should provide two public member functions:
- isEqual()
- hash()


For example, a type Foo can be written as following:

.. code-block:: javascript

    class Foo
    {
        public function isEqual(other: Foo): bool
        {
            return this.value == other.value;
        }

        public function hash(): int64
        {
            return time_stamp;
        }

        private var time_stamp: int64; // unique
        private var value: int32;
    }

Foo should be convertiable to isEqual() 's parameter,
and the isEqual() return type should be convertible to
bool.

hash() takes no aguments and it should return a
non-negative integer and avoid to return identical hash
codes from different objects.

**See**
    :ref:`HashMapIterator\<K, V\> <HashMapIterator>`

    :ref:`HashMapEntry\<K, V\> <HashMapEntry>`

**Extends**
    :ref:`Object <Object>`

.. _HashMap-new:

new(): void
```````````

Constructor creates HashMap with no entries.

.. _HashMap-delete:

delete(): void
``````````````

Destructor which releases all resources hold by instance.

*[virtual]*

.. _HashMap-create:

create(): HashMap<K, V>
```````````````````````

Provides functionality to create empty hashmaps in C++.

**Return**
    An empty hashmap instance.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _HashMap-clone:

clone(): HashMap<K, V>
``````````````````````

Create a copy of ``this``.

**Return**
    A newly created HashMap instance with same
    keys and values.

**Remark**
    This function performs **shallow copy**. That means
    only elements' values are copied. For non-primitive
    types, elements in the produced HashMap share same
    objects with ``this``.

.. _HashMap-get-K:

get(k: K): (V, bool)
````````````````````

Get a mapped value by a given key.

**Return**
    A value-result pair, first value is the mapped
    and second boolean value indicates if the key
    was found in map or not.

**Return Value**
    **(any, true)** Found key in map, and first

    **(any, false)** Can not find key in map, first value is meaningless.

.. _HashMap-set-K-V:

set(k : K, v: V): void
``````````````````````

Add a new key and its value.

Add a new key into map and set its value. If the key is
already in map, rewrite the mapped value by passed one.

**Parameters**
    **k** The key we want to add to map.

    **v** The value we want to map by key **k**.

.. _HashMap-remove-K:

remove(k: K): void
``````````````````

Remove a key from map.

Remove a key and the value it maps from map. If the key
is not find in map, do nothing.

**Parameters**
    **k** The key we want to remove from map.

.. _HashMap-iterator:

iterator(): HashMapIterator<K, V>
`````````````````````````````````

Returns an iterator.

**Return**
    An iterator which points to this.

.. _HashMap-has:

has(k: K): bool
```````````````

Tell if the given key is in map.

**Parameters**
    **k** The key we want to find.

**Return**
    Boolean value indicates if key is in map.

**Return Value**
    **true** If the key is exist in map.

    **false** If the key is not in map.

.. _HashMap-empty:

empty(): bool
`````````````

Tell if there is no keys in map.

What this function does is equivalent to call:

.. code-block:: javascript

    return this.size() == 0;

**Return**
    Boolean value indicates if this have no keys.

**Return Value**
    **true** No keys stay in this.

    **false** At least one key is in map.

.. _HashMap-size:

size(): int64
`````````````

Reutrn the numbers of keys in this.

**Return**
    The number of keys in this.

.. _HashMap-getContainedObjects-CollectableObjects:

getContainedObjects(o: CollectableObjects): void
````````````````````````````````````````````````

Member function will be called by garbage collector to
keep track objects which are still referenced in native
implementations.

**Parameters**
    **o** An object which collects still-be-referenced objects.

*[virtual]*

.. _HashMapIterator:

class HashMapIterator<K, V>
---------------------------

Helper class which supports navigating key and values
and put them together by using HashMapEntry as view.

In foreach loop, Thor will call HashMap 's  member function
iterator() to get a HashMapIterator instance(shorten as it later).
Then Thor calls it 's member function hasNext() to determine
to terminate executing loop or not. In each iteration, Thor gets an
key-value pair by calling it 's member function next().

**Remark**
    The behavior is undefined if backing map has been
    modified after the iterator was returned.

**See**
    :ref:`HashMap\<K, V\> <HashMap>`

    :ref:`HashMapEntry\<K, V\> <HashMapEntry>`

**Extends**
    :ref:`Object <Object>`

.. _HashMapIterator-new:

new(c: HashMap<K, v>): void
```````````````````````````

Constructor creates an iterator which points to a hashmap.

**Parameters**
    **c** The target HashMap this newly created iterator will point to.

.. _HashMapIterator-delete:

delete(): void
``````````````

Destructor which releases all resources hold by instance.

*[virtual]*

.. _HashMapIterator-create-HashMap-K-V:

create(c : HashMap<K, V>): HashMapIterator<K, V>
````````````````````````````````````````````````

Provides functionality to create iterators in C++.

**Parameters**
    **c** The target hashmap to navigate.

**Return**
    An iterator instance which can work on **c**.

**Remark**
    This function is current workaround to create object through C++.

*[static]*

.. _HashMapIterator-clone:

clone(): HashMapIterator<K, V>
``````````````````````````````

Get a copy of ``this``.

Get an iterator which has same status as this, both of them
point to the same HashMap and same element in it.

**Return**
    A copy of this.

**Remark**
    This function performs **shallow copy**. That means
    the returned iterator will share the same map with
    this.

*[virtual]*

.. _HashMapIterator-hasNext:

hasNext(): bool
```````````````

Tell if there is any entry not visited yet in
the target HashMap this points to.

**Return**
    A boolean value indicates visiting status.

**Return Value**
    **true** : If there is another key not visited yet.

    **false** : All key are visited.

.. _HashMapIterator-next:

next(): HashMapEntry<K, V>
``````````````````````````

Get the next unvisited key and its value together as a pair in HashMap.

**Return**

    A HashMapEntry instance which wraps unvisited key and
    associated value.

.. _HashMapEntry:

class HashMapEntry<K, V>
------------------------

A class wraps key and associated value together.

Usually this class is instantiated by HashMapIterator to
put key and value as a pair in loop iterations. An entry
object should be used only for the duration of the
iteration.

**Remark**
    The behavior is undefined if backing map has been
    modified after the entry was returned by the iterator.

**See**
    :ref:`HashMap\<K, V\> <HashMap>`

    :ref:`HashMapIterator\<K, V\> <HashMapIterator>`

**Extends**
    :ref:`Object <Object>`

.. _HashMapEntry-new:

new(e: HashMapEntry<K, V>): void
````````````````````````````````

Construct a copy from given map entry.

**Parameters**
    **e** The source entry to copy from.

.. _HashMapEntry-new-K-V:

new(k: K, v: V): void
`````````````````````

Wrap key and value together.

**Parameters**
    **k** The map key.

    **v** The associated value.

.. _HashMapEntry-create:

create(k : K, v : V): HashMapEntry<K, V>
````````````````````````````````````````

Provides functionality to create entries in C++.

**Parameters**
    **k** The map key.

    **v** The associated value.

**Return**
    An entry instance which has given arguments.

**Remark**
    This function is current workaround to create object through C++.

.. _HashMapEntry-key:

key: K
``````

    Wrapped map key.

.. _HashMapEntry-value:

value: V
````````

    Wrapped map value.

