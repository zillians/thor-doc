thor.util
=========

Algorithm
---------

.. _binarySearch-CollectionType-Element:

binarySearch<CollectionType,Element>( collection: CollectionType, key: Element ) : int64
````````````````````````````````````````````````````````````````````````````````````````

Binary search for a element.

Example:

.. code-block:: javascript

    task test_binary_search()
    {
        var arr = [1,3,5];

        println(binarySearch(arr,3));
        println(binarySearch(arr,1));
        println(binarySearch(arr,6));
        println(binarySearch(arr,0));
        println(binarySearch(arr,2));

        exit(0);
    }

Output::

    10
    3
    -1
    1

**Parameters**
    **collection** The input sequential container.

    **key** The element to be found.

**Return**
    If **key** is found, its index is returned. If  **key** is less than the smallest
    of **collection**, return -1. If  **key** is greater than the largest of **colletion**,
    return the size of **collection**. If **key** is not in **collection** and it's between
    the smallest and the largest, the index of the largest of those elements smaller
    than **key** is returned.

**See**
    :ref:`binarySearch\<CollectionType,Element,Compare\>( collection: CollectionType, key: Element, compare: Compare ) : int64 <binarySearch-CollectionType-Element-Compare>`

.. _binarySearch-CollectionType-Element-Compare:

binarySearch<CollectionType,Element,Compare>( collection: CollectionType, key: Element, compare: Compare ) : int64
``````````````````````````````````````````````````````````````````````````````````````````````````````````````````

Binary search for a element using a user-defined comparator.

Perform binary search on a sequential container to find a given element.
It's the same as binarySearch<CollectionType,Element> except you can define your
own comparator with this version.

A user-defined comparator is a class with a method **compare()**.
It should be a function accept two arguments as consistent type with your
target sequential container. It should return **negative** value if the
first argument is less than the second, a **positive** value if the first
argument is **greater** then the second, and **zero** if they are equal.

**Parameters**
    **collection** The input sequential container.

    **key** The element to be found.

    **compare** A user-defined comparator.

**See**
    :ref:`binarySearch\<CollectionType,Element\>( collection: CollectionType, key: Element ) : int64 <binarySearch-CollectionType-Element>`

.. _copy-CollectionType1-CollectionType2:

copy<CollectionType1, CollectionType2>( destination: CollectionType1, source: CollectionType2 ) : void
``````````````````````````````````````````````````````````````````````````````````````````````````````

Copy content from one sequential container to the other.

Copy all elements of a source sequential container to a destination sequential container.
Example:

.. code-block:: javascript

    import .= thor.util;
    import .= thor.container;

    task test_copy()
    {
        var arr = [1,2,3,4,5];

        var dest = new Vector<int32>(5);
        copy(dest,arr);

        for(var v in dest)
        {
            print("\{v} ");
        }
        print("\n");

        exit(0);
    }

Output::

    1 2 3 4 5

**Parameters**
    **destination** The destination sequential container.

    **source** The source squential container.

**Remarks**
    The size of **destination** must be at least equal to  **source**.

.. _fill-CollectionType-Element:

fill<CollectionType,Element>( collection: CollectionType, element: Element ) : void
```````````````````````````````````````````````````````````````````````````````````

Fill in a container with a given value.

Example:

.. code-block:: javascript

    import .= thor.util;

    task test_fill()
    {
        var arr = [1,2,3,4,5];

        fill(arr,0);
        for(var v in arr)
        {
            print("\{v} ");
        }
        print("\n");

        exit(0);
    }

Output::

    0 0 0 0 0

**Parameters**
    **collection** The input container.

    **element** The value that is going to fill **collection**.

.. _indexOf-CollectionType-Element:

indexOf<CollectionType,Element>( collection: CollectionType, element: Element ) : int64
```````````````````````````````````````````````````````````````````````````````````````

Find index of the first element matching the given value.

Example:

.. code-block:: javascript

    task test_indexof()
    {
        var arr = [3,1,4,5,9,8,5];
        println(indexOf(arr,5));
        println(indexOf(arr,7));

        exit(0);
    }

Output::

    3
    -1

**Parameters**
    **collection** The input sequential container.

    **element** The target value.

**Return**
    Index of the first element matching the given value. -1 if it's not found.

**See**
    :ref:`indexOfSubList\<CollectionType1,CollectionType2\>( source: CollectionType1, target: CollectionType2 ) : int64 <indexOfSubList-CollectionType1-CollectionType2>`

    :ref:`lastIndexOf\<CollectionType,Element\>( collection: CollectionType, element: Element ) : int64 <lastIndexOf-CollectionType-Element>`

    :ref:`lastIndexOfSubList\<CollectionType1,CollectionType2\>( source: CollectionType1, target: CollectionType2 ) : int64 <lastIndexOfSubList-CollectionType1-CollectionType2>`

.. _indexOfSubList-CollectionType1-CollectionType2:

indexOfSubList<CollectionType1,CollectionType2>( source: CollectionType1, target: CollectionType2 ) : int64
```````````````````````````````````````````````````````````````````````````````````````````````````````````

Find the begin index of the first consecutive subsequence matching the given one.

Example:

.. code-block:: javascript

    task test_index_sub()
    {
        var arr = [3,4,5,3,4,5];

        println(indexOfSubList(arr, [3,4]));
        println(indexOfSubList(arr, [3,5]));
        println(indexOfSubList(arr, [9,10]));

        exit(0);
    }

Output::

    0
    -1
    -1

**Parameters**
    **source** The target sequential container.

    **target** The consecutive subsequence which is going to be found.

**Return**
    The begin index of the first consecutive subsequence matching **target** in **source**. -1 if it's not found.

**See**
    :ref:`indexOf\<CollectionType,Element\>( collection: CollectionType, element: Element ) : int64 <indexOf-CollectionType-Element>`

    :ref:`lastIndexOf\<CollectionType,Element\>( collection: CollectionType, element: Element ) : int64 <lastIndexOf-CollectionType-Element>`

    :ref:`lastIndexOfSubList\<CollectionType1,CollectionType2\>( source: CollectionType1, target: CollectionType2 ) : int64 <lastIndexOfSubList-CollectionType1-CollectionType2>`

.. _isAscending-CollectionType:

isAscending<CollectionType>( collection: CollectionType ) : bool
````````````````````````````````````````````````````````````````

Check if elements of the given sequential container is in ascending order.

Example:

.. code-block:: javascript

    import .= thor.util;

    task test_ascend()
    {
       var arr  = [1,2,3,4,5];
       var arr2 = [5,4,3,2,1];

       var r  = isAscending(arr);
       var r2 = isAscending(arr2);
       println("\{r} : \{r2}");

       exit(0);
    }

Output::

    true : false

**Parameters**
    **collection** The input sequential container.

**Return**
    If **collection** is in ascending order.

**Return Value**
    **true** **collection** is in ascending order.

    **false** **collection** is not in ascending order.

**See**
    :ref:`isAscending\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : bool <isAscending-CollectionType-Compare>`

.. _isAscending-CollectionType-Compare:

isAscending<CollectionType, Compare>( collection: CollectionType, compare: Compare ) : bool
```````````````````````````````````````````````````````````````````````````````````````````

Use user-defined comparator to check if elements of the given sequential container is in ascending order.

Example:

.. code-block:: javascript

    class Greater
    {
        public function compare(a : int32, b : int32) : int32
        {
            return b - a;
        }
    }

    import .= thor.util;

    task test_ascend()
    {
       var arr  = [1,2,3,4,5];
       var arr2 = [5,4,3,2,1];

       var r  = isAscending(arr, new Greater());
       var r2 = isAscending(arr2, new Greater());
       println("\{r} : \{r2}");

       exit(0);
    }

Output::

    false : true

**Parameters**
    **collection** The input sequential container.

    **compare** A user-defined comparator.

**Return**
    If **collection** is in ascending order in respect of  **compare**.

**Return Value**
    **true** **collection** is in ascending order in respect of  **compare**.

    **false** **collection** is not in ascending order in respect of  **compare**.

**See**
    :ref:`isAscending\<CollectionType\>( collection: CollectionType ) : bool <isAscending-CollectionType>`

.. _lastIndexOf-CollectionType-Element:

lastIndexOf<CollectionType,Element>( collection: CollectionType, element: Element ) : int64
```````````````````````````````````````````````````````````````````````````````````````````

Find index of the last element matching the given value.

Example:

.. code-block:: javascript

    task test_lastindexof()
    {
        var arr = [3,1,4,5,9,8,5];
        println(lastIndexOf(arr,5));
        println(lastIndexOf(arr,7));

        exit(0);
    }

Output::

    6
    -1

**Parameters**
    **collection** The input sequential container.

    **element** The target value.

**Return**
    Index of the last element matching the given value. -1 if it's not found.

**See**
    :ref:`indexOf\<CollectionType,Element\>( collection: CollectionType, element: Element ) : int64 <indexOf-CollectionType-Element>`

    :ref:`indexOfSubList\<CollectionType1,CollectionType2\>( source: CollectionType1, target: CollectionType2 ) : int64 <indexOfSubList-CollectionType1-CollectionType2>`

    :ref:`lastIndexOf\<CollectionType,Element\>( collection: CollectionType, element: Element ) : int64 <lastIndexOf-CollectionType-Element>`

.. _lastIndexOfSubList-CollectionType1-CollectionType2:

lastIndexOfSubList<CollectionType1,CollectionType2>( source: CollectionType1, target: CollectionType2 ) : int64
```````````````````````````````````````````````````````````````````````````````````````````````````````````````

Find the begin index of the last consecutive subsequence matching the given one.

Example:

.. code-block:: javascript

    task test_index_sub()
    {
        var arr = [3,4,5,3,4,5];

        println(lastIndexOfSubList(arr, [3,4]));
        println(lastIndexOfSubList(arr, [3,5]));
        println(lastIndexOfSubList(arr, [9,10]));

        exit(0);
    }

Output::

    3
    -1
    -1

**Parameters**
    **source** The target sequential container.

    **target** The consecutive subsequence which is going to be found.

**Return**
    The begin index of the last consecutive subsequence matching **target** in **source**. -1 if it's not found.

**See**
    :ref:`indexOf\<CollectionType,Element\>( collection: CollectionType, element: Element ) : int64 <indexOf-CollectionType-Element>`

    :ref:`indexOfSubList\<CollectionType1,CollectionType2\>( source: CollectionType1, target: CollectionType2 ) : int64 <indexOfSubList-CollectionType1-CollectionType2>`

    :ref:`lastIndexOf\<CollectionType,Element\>( collection: CollectionType, element: Element ) : int64 <lastIndexOf-CollectionType-Element>`

.. _nCopies-int64-Element:

nCopies<CollectionType,Element>( n: int64, element: Element ) : CollectionType
``````````````````````````````````````````````````````````````````````````````

Return a sequential container with n-copies of a given element.

Example:

.. code-block:: javascript

    import .= thor.util;
    import .= thor.container;

    task test_ncopies()
    {
        var vec = nCopies<Vector<int32> >(5,3);

        for(var v in vec)
        {
            print("\{v} ");
        }
        print("\n");

        exit(0);
    }

Output::

    3 3 3 3 3

**Parameters**
    **n** Number of copies.

    **element** Element to be copied.

**Return**
    The **CollectionType** container with  **n** copies of  **element**.

.. _partialSort-CollectionType-Size:

partialSort<CollectionType,Size>( collection: CollectionType, middle: Size ) : void
```````````````````````````````````````````````````````````````````````````````````

Rearrange a sequential container such that elements in the given range is identical to the sorted one.

Given a mid-point, m, partialSort() will rearrange elements of the sequential container such that those in [begin, m)
is identical to the fully sorted one.

Example:

.. code-block:: javascript

    import .= thor.util;

    task test_partial()
    {
       var arr = [10,9,8,7,6,5,4,3,2,1];

       partialSort(arr, 5);
       for(var v in arr)
       {
           print("\{v} ");
       }
       print("\n");

       exit(0);
    }

Output::

        1 2 3 4 5 10 9 8 7 6

**Parameters**
    **collection** The sequential container which is going to be partially sorted.

    **middle** The mid-point which separtes the sorted range and the un-sorted range.

**See**
    :ref:`sort\<CollectionType\>( collection: CollectionType ) : void <sort-CollectionType>`

    :ref:`sort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <sort-CollectionType-Compare>`

    :ref:`stableSort\<CollectionType\>( collection: CollectionType ) : void <stableSort-CollectionType>`

    :ref:`stableSort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <stableSort-CollectionType-Compare>`

    :ref:`partialSort\<CollectionType,Size, Compare\>( collection: CollectionType, middle: Size, compare: Compare ) : void <partialSort-CollectionType-Size-Compare>`

.. _partialSort-CollectionType-Size-Compare:

partialSort<CollectionType,Size, Compare>( collection: CollectionType, middle: Size, compare: Compare ) : void
``````````````````````````````````````````````````````````````````````````````````````````````````````````````

Rearrange a sequential container such that the element in the given range is identical to the sorted one using user-defined comparator.

Despite the user-defined comparator, the logic is the same as **partialSort<CollectionType,Size>**.

A user-defined comparator is a class with a method **compare()**.
It should be a function accept two arguments as consistent type with your
target sequential container. It should return **negative** value if the
first argument is less than the second, a **positive** value if the first
argument is **greater** then the second, and  **zero** if they are equal.

Example:

.. code-block:: javascript

    class Greater
    {
        public function compare(a : int32, b : int32) : int32
        {
            return b - a;
        }
    }

    task test_partial()
    {
       var arr = [1,2,3,4,5,6,7,8,9,10];

       partialSort(arr, 5, new Greater());
       for(var v in arr)
       {
           print("\{v} ");
       }
       print("\n");

       exit(0);
    }

Output::

    10 9 8 6 5 1 2 3 4 5

**Parameters**
    **collection** The sequential container which is going to be partially sorted.

    **middle** The mid-point which separtes the sorted range and the un-sorted range.

    **compare** The user-defined comparator.

**See**
    :ref:`sort\<CollectionType\>( collection: CollectionType ) : void <sort-CollectionType>`

    :ref:`sort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <sort-CollectionType-Compare>`

    :ref:`stableSort\<CollectionType\>( collection: CollectionType ) : void <stableSort-CollectionType>`

    :ref:`stableSort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <stableSort-CollectionType-Compare>`

    :ref:`partialSort\<CollectionType,Size\>( collection: CollectionType, middle: Size ) : void <partialSort-CollectionType-Size>`

.. _reverse-CollectionType:

reverse<CollectionType>( collection: CollectionType ) : void
````````````````````````````````````````````````````````````

Reverse the order of the input sequential container.

Example:

.. code-block:: javascript

    import .= thor.util;

    task test_reverse()
    {
        var arr = [5,4,3,2,1];
        reverse(arr);

        for(var v in arr)
        {
            print("\{v} ");
        }
        print("\n");

        exit(0);
    }

Output::

    1 2 3 4 5

**Parameters**
    **collection** The input sequential container.

.. _rotate-CollectionType-step:

rotate<CollectionType,Size>( collection: CollectionType, step: Size ) : void
````````````````````````````````````````````````````````````````````````````

Clockwise rotate a sequential container.

Clockwise rotate a sequential container for a given step.

Example:

.. code-block:: javascript

    import .= thor.util;

    task test_rotate()
    {
        var arr = [1,2,3,4,5];

        rotate(arr, 3);

        for(var v in arr)
        {
            print("\{v} ");
        }
        print("\n");

        exit(0);
    }

Output::

    3 4 5 1 2

**Parameters**
    **collection** The input sequential container.

    **step** The given step of rotating.

.. _replaceAll-CollectionType-oldVal-newVal:

replaceAll<CollectionType,OldVal,NewVal>( collection: CollectionType, oldVal: OldVal, newVal: NewVal ) : bool
`````````````````````````````````````````````````````````````````````````````````````````````````````````````

Replace all elements with value **oldVal** to  **newVal**.

Example:

.. code-block:: javascript

    import .= thor.util;

    task test_replace()
    {
        var arr = [1,2,1,2,1];

        replaceAll(arr, 1, 2);

        for(var v in arr)
        {
            print("\{v} ");
        }
        print("\n");

        exit(0);
    }

Output::

    2 2 2 2 2

**Parameters**
    **collection** The input sequential container.

    **oldVal** The target value to be replaced.

    **newVal** The value which is going to replace **oldVal**.

**Return Value**
    **true** At least one element is replaced by **newVal**.

    **false** No element is replaced.

.. _shuffle-CollectionType:

shuffle<CollectionType>( collection: CollectionType ) : void
````````````````````````````````````````````````````````````

Randomly reorder the input sequential container.

Example:

.. code-block:: javascript

    import .= thor.util;

    task test_shuffle()
    {
        var arr = [1,2,3,4,5];
        shuffle(arr);

        for(var v in arr)
        {
            print("\{v} ");
        }
        print("\n");

        exit(0);
    }

Output::

        2 1 5 3 4

**Parameters**
    **collection** The input sequential container.

.. _sort-CollectionType:

sort<CollectionType>( collection: CollectionType ) : void
`````````````````````````````````````````````````````````

Sort a sequential container with **less** comparator.

Example:

.. code-block:: javascript

    import .= thor.container;
    import .= thor.util;

    @entry
    task test_sort()
    {
       var vec = new Vector<int32>;
       vec.pushBack(4);
       vec.pushBack(2);
       vec.pushBack(1);
       vec.pushBack(3);

       sort(vec);

       for(var v in vec)
       {
           print("\{v} ");
       }
       print("\n");

       var arr = [2,3,1,4];
       sort(arr);

       for(var v in arr)
       {
           print("\{v} ");
       }
       print("\n");

       exit(0);
    }

Output::

        1 2 3 4
        1 2 3 4

**Parameters**
    **collection** The sequential container which is going to be sorted.

**Remarks**
    It's an unstable sort.

**See**
    :ref:`sort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <sort-CollectionType-Compare>`

    :ref:`stableSort\<CollectionType\>( collection: CollectionType ) : void <stableSort-CollectionType>`

    :ref:`stableSort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <stableSort-CollectionType-Compare>`

    :ref:`partialSort\<CollectionType,Size\>( collection: CollectionType, middle: Size ) : void <partialSort-CollectionType-Size>`

    :ref:`partialSort\<CollectionType,Size, Compare\>( collection: CollectionType, middle: Size, compare: Compare ) : void <partialSort-CollectionType-Size-Compare>`

.. _sort-CollectionType-Compare:

sort<CollectionType, Compare>( collection: CollectionType, compare: Compare ) : void
````````````````````````````````````````````````````````````````````````````````````

Sort a sequential container with **user**-defined comparator.

A user-defined comparator is a class with a method **compare()**.
It should be a function accept two arguments as consistent type with your
target sequential container. It should return **negative** value if the
first argument is less than the second, a **positive** value if the first
argument is **greater** then the second, and **zero** if they are equal.

Example:

.. code-block:: javascript

    import .= thor.container;
    import .= thor.util;

    class Greater
    {
        public function compare(a : int32, b : int32) : int32
        {
            return b - a;
        }
    }

    task test_sort()
    {
       var vec = new Vector<int32>;
       vec.pushBack(4);
       vec.pushBack(2);
       vec.pushBack(1);
       vec.pushBack(3);

       sort(vec, new Greater());

       for(var v in vec)
       {
           print("\{v} ");
       }
       print("\n");

       exit(0);
    }

Output::

        4 3 2 1

**Parameters**
    **collection** The sequential container which is going to be sorted.

**Remarks**
    It's an unstable sort.

**See**
    :ref:`sort\<CollectionType\>( collection: CollectionType ) : void <sort-CollectionType>`

    :ref:`stableSort\<CollectionType\>( collection: CollectionType ) : void <stableSort-CollectionType>`

    :ref:`stableSort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <stableSort-CollectionType-Compare>`

    :ref:`partialSort\<CollectionType,Size\>( collection: CollectionType, middle: Size ) : void <partialSort-CollectionType-Size>`

    :ref:`partialSort\<CollectionType,Size, Compare\>( collection: CollectionType, middle: Size, compare: Compare ) : void <partialSort-CollectionType-Size-Compare>`

    :ref:`stableSort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <stableSort-CollectionType-Compare>`

.. _stableSort-CollectionType:

stableSort<CollectionType>( collection: CollectionType ) : void
```````````````````````````````````````````````````````````````

Stably sort a sequential container with less comparator.

Sort a sequential container in a stable way. The usage is the same as sort().

**See**
    :ref:`sort\<CollectionType\>( collection: CollectionType ) : void <sort-CollectionType>`

    :ref:`sort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <sort-CollectionType-Compare>`

    :ref:`stableSort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <stableSort-CollectionType-Compare>`

    :ref:`partialSort\<CollectionType,Size\>( collection: CollectionType, middle: Size ) : void <partialSort-CollectionType-Size>`

    :ref:`partialSort\<CollectionType,Size, Compare\>( collection: CollectionType, middle: Size, compare: Compare ) : void <partialSort-CollectionType-Size-Compare>`

.. _stableSort-CollectionType-Compare:

stableSort<CollectionType, Compare>( collection: CollectionType, compare: Compare ) : void
``````````````````````````````````````````````````````````````````````````````````````````

Stably sort a sequential container with a user-defined comparator.

Sort a sequential container in a stable way with a user-defined comparator.
The usage is the same as sort() with a user-defined comparator.

**See**
    :ref:`sort\<CollectionType\>( collection: CollectionType ) : void <sort-CollectionType>`

    :ref:`sort\<CollectionType, Compare\>( collection: CollectionType, compare: Compare ) : void <sort-CollectionType-Compare>`

    :ref:`stableSort\<CollectionType\>( collection: CollectionType ) : void <stableSort-CollectionType>`

    :ref:`partialSort\<CollectionType,Size\>( collection: CollectionType, middle: Size ) : void <partialSort-CollectionType-Size>`

    :ref:`partialSort\<CollectionType,Size, Compare\>( collection: CollectionType, middle: Size, compare: Compare ) : void <partialSort-CollectionType-Size-Compare>`

Convert
-------

.. _Convert:

class Convert
`````````````

Provides utilities to generate string representation from
variables or vice versa.

**Remarks**
    Users should use static member functions by ``Convert``
    class name and not trying create instances of this class.

.. _Convert-toInt8-s:

toInt8(s: String): int8
~~~~~~~~~~~~~~~~~~~~~~~

Convert string content into int8 value.

**Parameters**
    **s** The source string.

**Return**
    The result value from string. For example, string "123"
    will be converted to 123.

**Remarks**
    there is no way to know if string content is valid or not,
    for example: "@" or "~".

*[static]*

.. _Convert-toInt16-s:

toInt16(s: String): int16
~~~~~~~~~~~~~~~~~~~~~~~~~

Convert string content into int16 value.

**Parameters**
    **s** The source string.

**Return**
    The result value from string. For example, string "1234"
    will be converted to 1234.

**Remarks**
    there is no way to know if string content is valid or not,
    for example: "@" or "~".

*[static]*

.. _Convert-toInt32-s:

toInt32(s: String): int32
~~~~~~~~~~~~~~~~~~~~~~~~~

Convert string content into int32 value.

**Parameters**
    **s** The source string.

**Return**
    The result value from string. For example, string "12345"
    will be converted to 12345.

**Remarks**
    there is no way to know if string content is valid or not,
    for example: "@" or "~".

*[static]*

.. _Convert-toInt64-s:

toInt64(s: String): int64
~~~~~~~~~~~~~~~~~~~~~~~~~

Convert string content into int64 value.

**Parameters**
    **s** The source string.

**Return**
    The result value from string. For example, string "123456"
    will be converted to 123456L.

**Remarks**
    there is no way to know if string content is valid or not,
    for example: "@" or "~".

*[static]*

.. _Convert-toFloat32-s:

toFloat32(s: String): float32
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Convert string content into float32 value.

**Parameters**
    **s** The source string.

**Return**
    The result value from string. For example, string "543.21"
    will be converted to 543.21f.

**Remarks**
    There is no way to know if string content is valid or not,
    for example: "@" or "~".

*[static]*

.. _Convert-toFloat64-s:

toFloat64(s: String): float64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Convert string content into float64 value.

**Parameters**
    **s** The source string.

**Return**
    The result value from string. For example, string "54.321"
    will be converted to 54.321.

**Remarks**
    there is no way to know if string content is valid or not,
    For example: "@" or "~".

*[static]*

.. _Convert-toString-int8:

toString(v: int8): String
~~~~~~~~~~~~~~~~~~~~~~~~~

Create a ``String`` which represents an int8 value.

**Parameters**
    **v** The source int8 value.

**Return**
    A String representation of the passed value. For example:
    parameter 123 will produces a string "123"

*[static]*

.. _Convert-toString-int16:

toString(v: int16): String
~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a ``String`` which represents an int16 value.

**Parameters**
    **v** The source int16 value.

**Return**
    A String representation of the passed value. For example:
    parameter 1234 will produces a string "1234"

*[static]*

.. _Convert-toString-int32:

toString(v: int32): String
~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a ``String`` which represents an int32 value.

**Parameters**
    **v** The source int32 value.

**Return**
    A String representation of the passed value. For example:
    parameter 12345 will produces a string "12345"

*[static]*

.. _Convert-toString-int64:

toString(v: int64): String
~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a ``String`` which represents an int64 value.

**Parameters**
    **v** The source int64 value.

**Return**
    A String representation of the passed value. For example:
    parameter 123456L will produces a string "123456"

*[static]*

.. _Convert-toString-float32:

toString(v: float32): String
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a ``String`` which represents an float32 value.

**Parameters**
    **v** The source float32 value.

**Return**
    A String representation of the passed value. For example:
    parameter 54.321f will produces a string "54.321"

*[static]*

.. _Convert-toString-float64:

toString(v: float64): String
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a ``String`` which represents an float64 value.

**Parameters**
    **v** The source float64 value.

**Return**
    A String representation of the passed value. For example:
    parameter 543.21 will produces a string "543.21"

*[static]*

.. _Convert-toString-value:

toString<Value>(value: Value): String
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a ``String`` which represents an Object which has
user-defined type.

The is a function template which can take any user-defined type
objects. Finally it returns the result from calling member
function ``toString()`` on parameter  ``value``. The ``toString()``
return type should be convertible to class ``thor.lang.String``.

**Parameters**
    **value** The user-defined type object.

**Return**
    The result string returned by value.toString().

*[static]*

Pseudo-random number generation
-------------------------------

.. _Uniform:

class Uniform
`````````````

A class which can be used as template type argument of ``Random``
class.

This type can specialize an uniform distribution random number
generator class from ``Random`` template.

**See**
    :ref:`Random\<T, D\> <Random-T-D>`

**Extends**
    :ref:`Object <Object>`

.. _Normal:

class Normal
````````````

A class which can be used as template type argument of ``Random``
class.

This type can specialize a normal distribution random number
generator class from ``Random`` template.

**See**
    :ref:`Random\<T, D\> <Random-T-D>`

**Extends**
    :ref:`Object <Object>`

.. _Random-T-D:

class Random<T, D>
``````````````````

A Random Number Generator class template accepts all kind of type arguments.

This is generic version of Random. We only support primitive types to be first
type argument **T** and second argument **D** is one of ``Uniform`` and ``Normal``.

Users can specialize this template and write their own generator.

**See**
    :ref:`Random\<T, D:Uniform\> <Random-T-Uniform>`

    :ref:`Random\<T, D:Normal\> <Random-T-Normal>`

**Extends**
    :ref:`Object <Object>`

.. _Random-T-Uniform:

class Random<T, D:Uniform>
``````````````````````````

An Uniform Random Number Generator class template designed to produce
primitive type values.

This template can be instantiated by passing primitive types as its first
argument, for example: ``Random<int32, Uniform>`` can produces random integer
values uniformly distributed on close interval [**min**, **max**]. The **min**,
**max** are the distribution parameters users pass to Random's constructors.

**See**
    :ref:`Random\<T, D\> <Random-T-D>`

    :ref:`Uniform <Uniform>`

**Extends**
    :ref:`Object <Object>`

.. _Random-T-Uniform-new:

new(): void
~~~~~~~~~~~

Constructor creates a random number generator which produces
any values which can be represented by type T.

All generators created by this constructor have different random
number sequence from others.

.. _Random-T-Uniform-new-int64:

new(seed: int64): void
~~~~~~~~~~~~~~~~~~~~~~

From a given random **seed**. creates a random number generator
which produces any values which can be represented by type T.

All generators created by same **seed** will produce same random number
sequence.

**Parameters**
    **seed** The random seed.

.. _Random-T-Uniform-new-T-T:

new(min:T, max:T): void
~~~~~~~~~~~~~~~~~~~~~~~

Constructor creates a random number generator which produces
any values in close interval [**min**, **max**].

All generators created by this constructor have different random
number sequence from others.

**Parameters**
    **min** Minimum result value.

    **max** Maximum result value.

.. _Random-T-Uniform-new-int64-T-T:

new(seed:int64, min:T, max:T) : void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From a given random **seed**, creates a random number generator
which produces any values in close interval [**min**, **max**].

All generators created by a same **seed** will produce same random
number sequence.

**Parameters**
    **seed** The random seed.

    **min** Minimum result value.

    **max** Maximum result value.

.. _Random-T-Uniform-delete:

delete() : void
~~~~~~~~~~~~~~~

Destructor releases internal resources.

.. _Random-T-Uniform-seed-int64:

*[virtual]*

seed(seed:int64) : void
~~~~~~~~~~~~~~~~~~~~~~~

Reset generator's random seed.

Set by another random seed, random number sequence will be renewed.
All generators which are reset by a same **seed** will produce
same sequence.

**Parameters**
    **seed** The other new random seed.

.. _Random-T-Uniform-next:

next(): T
~~~~~~~~~

Generate another random number.

**Return**
    A new random number.

.. _Random-T-Normal:

class Random<T, D:Normal>
`````````````````````````

An Normal Random Number Generator class template designed to produce
primitive type values.

This template can be instantiated by passing primitive types as its first
argument, for example: ``Random<float32, Normal>`` can produces random real
values by given **mean** and **standard** deviation to Random's
constructors. When user creates a ``Random<int32, Normal>`` instance,
the generator instance still produce floating points in its implementation,
but rounds result into a closest integer while return.

**See**
    :ref:`Random\<T, D\> <Random-T-D>`

    :ref:`Normal <Normal>`

**Extends**
    :ref:`Object <Object>`

.. _Random-T-Normal-new:

new() : void
~~~~~~~~~~~~

Constructor creates a random number generator which produces
any values by **mean** = 0.0 and **standard deviation** = 1.0.

All generators created by this constructor have different random
number sequence from others.

.. _Random-T-Normal-new-int64:

new(seed:int64) : void
~~~~~~~~~~~~~~~~~~~~~~

From a given random **seed**. creates a random number generator
which produces any values by **mean** = 0.0 and
**standard deviation** = 1.0.

All generators created by same **seed** will produce same random number
sequence.

**Parameters**
    **seed** The random seed.

.. _Random-T-Normal-new-T-T:

new(mean:T, stddev:T) : void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Constructor creates a random number generator which produces
any values by specific **mean** and **standard deviation**.

All generators created by this constructor have different random
number sequence from others.

**Parameters**
    **mean** The mean.

    **stddev** The standard deviation.

.. _Random-T-Normal-new-int64-T-T:

new(seed:int64, mean:T, stddev:T) : void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From a given random **seed**, creates a random number generator
which produces any values by specific **mean** and **standard deviation**.

All generators created by a same **seed** will produce same random number
sequence.

**Parameters**
    **seed** The random seed.

    **mean** The mean.

    **stddev** The standard deviation.

.. _Random-T-Normal-delete:

delete(): void
~~~~~~~~~~~~~~

Destructor releases internal resources.

.. _Random-T-Normal-seed-int64:

*[virtual]*

seed(seed:int64) : void
~~~~~~~~~~~~~~~~~~~~~~~

Reset generator's random seed.

Set by another random seed, random number sequence will be renewed.
All generators which are reset by a same **seed** will produce same sequence.

**Parameters**
    **seed** The other new random seed.

.. _Random-T-Normal-next:

next() : T
~~~~~~~~~~

Generate another random number.

**Return**
    A new random number.

StringBuilder
-------------

.. _StringBuilder:

class StringBuilder
```````````````````

Class manipulates characters with stream like interface.

StringBuilder provides the following functionalities:
- reverse - Reverse the characters order.
- append - Append string representation of values (for example, integer and floating point values)
- insert - Similar to append, except inserting string representation at specified position.
- toString - Dump current string representation to thor.lang.String.
.

**See**
    :ref:`String <String>`

    :ref:`MutableString <MutableString>`

.. _StringBuilder-new:

new():void
~~~~~~~~~~

Default constructor creates string representation which is the same as "".

**See**
    :ref:`new(String) <StringBuilder-new-String>`

.. _StringBuilder-new-String:

new(s:String):void
~~~~~~~~~~~~~~~~~~

Constructor creates string representation which is the same as  `s`.

**Parameters**
    **s** String to be copied as initial string representation.

**See**
    :ref:`new() <StringBuilder-new>`

.. _StringBuilder-delete:

delete():void
~~~~~~~~~~~~~

Destructor releases internal resources.

.. _StringBuilder-length:

length():int32
~~~~~~~~~~~~~~

Count the number of characters stored in string representation.

**Return**
    Number of characters.

**Remarks**
    The return value will not be negative integer.

.. _StringBuilder-reverse:

reverse():void
~~~~~~~~~~~~~~

Reverse the order of characters.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.reverse(); // builder represents "cba" after this

.. _StringBuilder-toString:

toString():String
~~~~~~~~~~~~~~~~~

Dump string representation as new thor.lang.String instance.

For example:

.. code-block:: javascript

    (new StringBuilder("abc")).toString(); // result is "abc"

**Return**
    The new thor.lang.String instance with the copied characters.

**See**
    :ref:`toString(int32, int32) <StringBuilder-toString-int32-int32>`

.. _StringBuilder-toString-int32-int32:

toString(start:int32, end:int32):String
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dump string representation in [start, end) as new thor.lang.String instance.

For example:

.. code-block:: javascript

    (new StringBuilder("abc")).toString(1, 2); // result is "b"

**Parameters**
    **start** The beginning of range to be copied.

    **end**   The post ending of range to be copied.

**Return**
    The new thor.lang.String instance with the copied characters.

**Return Value**
    **non-null** New string instance contains all characters in [start, end).

    **null**     The specified range is invalid, for example, `` `end` << start``.

**Remarks**
    The value of  `end` could larger than the value returned by
    `length()`. In such case, the result will contain all characters
    start from  `start`.

**See**
    :ref:`toString <StringBuilder-toString>`

.. _StringBuilder-append-string:

append(v:String):void
~~~~~~~~~~~~~~~~~~~~~

Append the given string.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append("abc "); // After this line, builder will be "abc "
    builder.append("def "); // After this line, builder will be "abc def "

**Parameters**
    **other** Another string to be appended.

**See**
    :ref:`append(bool) <StringBuilder-append-bool>`

    :ref:`append(int8) <StringBuilder-append-int8>`

    :ref:`append(int16) <StringBuilder-append-int16>`

    :ref:`append(int32) <StringBuilder-append-int32>`

    :ref:`append(int64) <StringBuilder-append-int64>`

    :ref:`append(float32) <StringBuilder-append-float32>`

    :ref:`append(float64) <StringBuilder-append-float64>`

.. _StringBuilder-append-bool:

append(v:bool):void
~~~~~~~~~~~~~~~~~~~

Append ``true`` or  ``false``.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append(true ); // After this line, builder will be "true"
    builder.append(false); // After this line, builder will be "truefalse"

**Parameters**
    **other** Append ``true`` if value is  `true`;  ``false`` otherwise.

**See**
    :ref:`append(int8) <StringBuilder-append-int8>`

    :ref:`append(int16) <StringBuilder-append-int16>`

    :ref:`append(int32) <StringBuilder-append-int32>`

    :ref:`append(int64) <StringBuilder-append-int64>`

    :ref:`append(float32) <StringBuilder-append-float32>`

    :ref:`append(float64) <StringBuilder-append-float64>`

    :ref:`append(String) <StringBuilder-append-string>`

.. _StringBuilder-append-int8:

append(v:int8):void
~~~~~~~~~~~~~~~~~~~

Append string representation of integer value.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append(cast<int8>(12)); // "12"
    builder.append(cast<int8>(21)); // "1221"

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`append(bool) <StringBuilder-append-bool>`

    :ref:`append(int16) <StringBuilder-append-int16>`

    :ref:`append(int32) <StringBuilder-append-int32>`

    :ref:`append(int64) <StringBuilder-append-int64>`

    :ref:`append(float32) <StringBuilder-append-float32>`

    :ref:`append(float64) <StringBuilder-append-float64>`

    :ref:`append(String) <StringBuilder-append-string>`

.. _StringBuilder-append-int16:

append(v:int16):void
~~~~~~~~~~~~~~~~~~~~

Append string representation of integer value.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append(cast<int16>(1234)); // "1234"
    builder.append(cast<int16>(7777)); // "12347777"

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`append(bool) <StringBuilder-append-bool>`

    :ref:`append(int8) <StringBuilder-append-int8>`

    :ref:`append(int32) <StringBuilder-append-int32>`

    :ref:`append(int64) <StringBuilder-append-int64>`

    :ref:`append(float32) <StringBuilder-append-float32>`

    :ref:`append(float64) <StringBuilder-append-float64>`

    :ref:`append(String) <StringBuilder-append-string>`

.. _StringBuilder-append-int32:

append(v:int32):void
~~~~~~~~~~~~~~~~~~~~

Append string representation of integer value.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append(1234); // "1234"
    builder.append(7777); // "12347777"

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`append(bool) <StringBuilder-append-bool>`

    :ref:`append(int8) <StringBuilder-append-int8>`

    :ref:`append(int16) <StringBuilder-append-int16>`

    :ref:`append(int64) <StringBuilder-append-int64>`

    :ref:`append(float32) <StringBuilder-append-float32>`

    :ref:`append(float64) <StringBuilder-append-float64>`

    :ref:`append(String) <StringBuilder-append-string>`

.. _StringBuilder-append-int64:

append(v:int64):void
~~~~~~~~~~~~~~~~~~~~

Append string representation of integer value.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append(cast<int64>(1234)); // "1234"
    builder.append(cast<int64>(7777)); // "12347777"

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`append(bool) <StringBuilder-append-bool>`

    :ref:`append(int8) <StringBuilder-append-int8>`

    :ref:`append(int16) <StringBuilder-append-int16>`

    :ref:`append(int32) <StringBuilder-append-int32>`

    :ref:`append(float32) <StringBuilder-append-float32>`

    :ref:`append(float64) <StringBuilder-append-float64>`

    :ref:`append(String) <StringBuilder-append-string>`

.. _StringBuilder-append-float32:

append(v:float32):void
~~~~~~~~~~~~~~~~~~~~~~

Append string representation of floating value.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append(cast<float32>(12.34)); // "12.34"
    builder.append(cast<float32>(77.77)); // "12.3477.77"

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`append(bool) <StringBuilder-append-bool>`

    :ref:`append(int8) <StringBuilder-append-int8>`

    :ref:`append(int16) <StringBuilder-append-int16>`

    :ref:`append(int32) <StringBuilder-append-int32>`

    :ref:`append(int64) <StringBuilder-append-int64>`

    :ref:`append(float64) <StringBuilder-append-float64>`

    :ref:`append(String) <StringBuilder-append-string>`

.. _StringBuilder-append-float64:

append(v:float64):void
~~~~~~~~~~~~~~~~~~~~~~

Append string representation of floating value.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.append(12.34); // "12.34"
    builder.append(77.77); // "12.3477.77"

**Parameters**
    **other** Value to generate as string representation.

**See**
    :ref:`append(bool) <StringBuilder-append-bool>`

    :ref:`append(int8) <StringBuilder-append-int8>`

    :ref:`append(int16) <StringBuilder-append-int16>`

    :ref:`append(int32) <StringBuilder-append-int32>`

    :ref:`append(int64) <StringBuilder-append-int64>`

    :ref:`append(float32) <StringBuilder-append-float32>`

    :ref:`append(String) <StringBuilder-append-string>`

.. _StringBuilder-appendAsCharacter-int8:

appendAsCharacter(v:int8):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append character from Unicode code-point specified by parameter.

With this function, user could insert character value by specifying
Unicode code-point directly.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.appendAsCharacter(cast<int8>(97)); // insert 'a'
    builder.appendAsCharacter(cast<int8>(98)); // insert 'b'
    builder.appendAsCharacter(cast<int8>(99)); // insert 'c'

    print(builder.toString() + "\n"); // this will print "abc" at single line on console

**Parameters**
    **v** 8-bit integer value represents Unicode code-point.

**Remarks**
    If the required value of Unicode code-pointer is larger than
    maximum value of 8-bit signed integer. User may want to take a
    look at appendAsCharacter(int16) or appendAsCharacter(int32).

**See**
    :ref:`appendAsCharacter(int16) <StringBuilder-appendAsCharacter-int16>`

    :ref:`appendAsCharacter(int32) <StringBuilder-appendAsCharacter-int32>`

.. _StringBuilder-appendAsCharacter-int16:

appendAsCharacter(v:int16):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append character from Unicode code-point specified by parameter.

With this function, user could insert character value by specifying
Unicode code-point directly.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.appendAsCharacter(cast<int16>(169)); // insert '©' (copyright sign)
    builder.appendAsCharacter(cast<int16>(174)); // insert '®' (register sign)

    print(builder.toString() + "\n"); // this will print "©®" (or similar output depends on locale) at single line on console

**Parameters**
    **v** 16-bit integer value represents Unicode code-point.

**Remarks**
    If the required value of Unicode code-pointer is larger than
    maximum value of 8-bit signed integer. User may want to take a
    look at appendAsCharacter(int16) or appendAsCharacter(int32).

**See**
    :ref:`appendAsCharacter(int16) <StringBuilder-appendAsCharacter-int8>`

    :ref:`appendAsCharacter(int32) <StringBuilder-appendAsCharacter-int32>`

.. _StringBuilder-appendAsCharacter-int32:

appendAsCharacter(v:int32):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append character from Unicode code-point specified by parameter.

With this function, user could insert character value by specifying
Unicode code-point directly.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder();

    builder.appendAsCharacter(12486); // insert 'テ'
    builder.appendAsCharacter(12473); // insert 'ス'
    builder.appendAsCharacter(12488); // insert 'ト'
    builder.appendAsCharacter(28204); // insert '測'
    builder.appendAsCharacter(35430); // insert '試'

    print(builder.toString() + "\n"); // this will print "テスト測試" at single line on console

**Parameters**
    **v** 32-bit integer value represents Unicode code-point.

**See**
    :ref:`appendAsCharacter(int8) <StringBuilder-appendAsCharacter-int8>`

    :ref:`appendAsCharacter(int16) <StringBuilder-appendAsCharacter-int16>`

.. _StringBuilder-insert-int32-string:

insert(offset:int32, v:String):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Insert the given string at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.append(1, "abc "); // After this line, builder will be "aabc bc"
    builder.append(1, "def "); // After this line, builder will be "adef abc bc"

**Parameters**
    **offset** The position to insert to.

    **other**  Another string to be inserted.

**Remarks**
    The value of  `offset` should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, bool) <StringBuilder-insert-int32-bool>`

    :ref:`insert(int32, int8) <StringBuilder-insert-int32-int8>`

    :ref:`insert(int32, int16) <StringBuilder-insert-int32-int16>`

    :ref:`insert(int32, int32) <StringBuilder-insert-int32-int32>`

    :ref:`insert(int32, int64) <StringBuilder-insert-int32-int64>`

    :ref:`insert(int32, float32) <StringBuilder-insert-int32-float32>`

    :ref:`insert(int32, float64) <StringBuilder-insert-int32-float64>`

.. _StringBuilder-insert-int32-bool:

insert(offset:int32, v:bool):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append ``true`` or  ``false`` at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.insert(1, true ); // After this line, builder will be "atruebc"
    builder.insert(1, false); // After this line, builder will be "afalsetruebc"

**Parameters**
    **offset** The position to insert to.

    **other**  Append ``true`` if value is  `true`;  ``false`` otherwise.

**Remarks**
    The value of  `offset` should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, int8) <StringBuilder-insert-int32-int8>`

    :ref:`insert(int32, int16) <StringBuilder-insert-int32-int16>`

    :ref:`insert(int32, int32) <StringBuilder-insert-int32-int32>`

    :ref:`insert(int32, int64) <StringBuilder-insert-int32-int64>`

    :ref:`insert(int32, float32) <StringBuilder-insert-int32-float32>`

    :ref:`insert(int32, float64) <StringBuilder-insert-int32-float64>`

    :ref:`insert(int32, String) <StringBuilder-insert-int32-String>`

.. _StringBuilder-insert-int32-int8:

insert(offset:int32, v:int8):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append string representation of integer value at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.insert(1, cast<int8>(12)); // "a12bc"
    builder.insert(1, cast<int8>(21)); // "a2112bc"

**Parameters**
    **offset** The position to insert to.

    **other** Value to generate as string representation.

**Remarks**
    The value of  `offset` should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, bool) <StringBuilder-insert-int32-bool>`

    :ref:`insert(int32, int16) <StringBuilder-insert-int32-int16>`

    :ref:`insert(int32, int32) <StringBuilder-insert-int32-int32>`

    :ref:`insert(int32, int64) <StringBuilder-insert-int32-int64>`

    :ref:`insert(int32, float32) <StringBuilder-insert-int32-float32>`

    :ref:`insert(int32, float64) <StringBuilder-insert-int32-float64>`

    :ref:`insert(int32, String) <StringBuilder-insert-int32-String>`

.. _StringBuilder-insert-int32-int16:

insert(offset:int32, v:int16):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append string representation of integer value at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.insert(1, cast<int16>(1234)); // "a1234bc"
    builder.insert(1, cast<int16>(7777)); // "a77771234bc"

**Parameters**
    **offset** The position to insert to.

    **other**  Value to generate as string representation.

**Remarks**
    The value of **offset** should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, bool) <StringBuilder-insert-int32-bool>`

    :ref:`insert(int32, int8) <StringBuilder-insert-int32-int8>`

    :ref:`insert(int32, int32) <StringBuilder-insert-int32-int32>`

    :ref:`insert(int32, int64) <StringBuilder-insert-int32-int64>`

    :ref:`insert(int32, float32) <StringBuilder-insert-int32-float32>`

    :ref:`insert(int32, float64) <StringBuilder-insert-int32-float64>`

    :ref:`insert(int32, String) <StringBuilder-insert-int32-String>`

.. _StringBuilder-insert-int32-int32:

insert(offset:int32, v:int32):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append string representation of integer value at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.insert(1, 1234); // "a1234bc"
    builder.insert(1, 7777); // "a77771234bc"

**Parameters**
    **offset** The position to insert to.

    **other** Value to generate as string representation.

**Remarks**
    The value of **offset** should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, bool) <StringBuilder-insert-int32-bool>`

    :ref:`insert(int32, int8) <StringBuilder-insert-int32-int8>`

    :ref:`insert(int32, int16) <StringBuilder-insert-int32-int16>`

    :ref:`insert(int32, int64) <StringBuilder-insert-int32-int64>`

    :ref:`insert(int32, float32) <StringBuilder-insert-int32-float32>`

    :ref:`insert(int32, float64) <StringBuilder-insert-int32-float64>`

    :ref:`insert(int32, String) <StringBuilder-insert-int32-String>`

.. _StringBuilder-insert-int32-int64:

insert(offset:int32, v:int64):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append string representation of integer value at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.insert(1, cast<int64>(1234)); // "a1234bc"
    builder.insert(1, cast<int64>(7777)); // "a77771234bc"

**Parameters**
    **offset** The position to insert to.

    **other**  Value to generate as string representation.

**Remarks**
    The value of  `offset` should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, bool) <StringBuilder-insert-int32-bool>`

    :ref:`insert(int32, int8) <StringBuilder-insert-int32-int8>`

    :ref:`insert(int32, int16) <StringBuilder-insert-int32-int16>`

    :ref:`insert(int32, int32) <StringBuilder-insert-int32-int32>`

    :ref:`insert(int32, float32) <StringBuilder-insert-int32-float32>`

    :ref:`insert(int32, float64) <StringBuilder-insert-int32-float64>`

    :ref:`insert(int32, String) <StringBuilder-insert-int32-String>`

.. _StringBuilder-insert-int32-float32:

insert(offset:int32, v:float32):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append string representation of floating value at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.insert(1, cast<float32>(12.34)); // "a12.34bc"
    builder.insert(1, cast<float32>(77.77)); // "a77.7712.34bc"

**Parameters**
    **offset** The position to insert to.

    **other** Value to generate as string representation.

**Remarks**
    The value of **offset** should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, bool) <StringBuilder-insert-int32-bool>`

    :ref:`insert(int32, int8) <StringBuilder-insert-int32-int8>`

    :ref:`insert(int32, int16) <StringBuilder-insert-int32-int16>`

    :ref:`insert(int32, int32) <StringBuilder-insert-int32-int32>`

    :ref:`insert(int32, int64) <StringBuilder-insert-int32-int64>`

    :ref:`insert(int32, float64) <StringBuilder-insert-int32-float64>`

    :ref:`insert(int32, String) <StringBuilder-insert-int32-String>`

.. _StringBuilder-insert-int32-float64:

insert(offset:int32, v:float64):void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Append string representation of floating value at specified position.

For example:

.. code-block:: javascript

    var builder: StringBuilder = new StringBuilder("abc");

    builder.append(12.34); // "a12.34bc"
    builder.append(77.77); // "a77.7712.34bc"

**Parameters**
    **offset** The position to insert to.

    **other**  Value to generate as string representation.

**Remarks**
    The value of  **offset** should be in range [0, length()]. Or the
    data is not changed.

**See**
    :ref:`insert(int32, bool) <StringBuilder-insert-int32-bool>`

    :ref:`insert(int32, int8) <StringBuilder-insert-int32-int8>`

    :ref:`insert(int32, int16) <StringBuilder-insert-int32-int16>`

    :ref:`insert(int32, int32) <StringBuilder-insert-int32-int32>`

    :ref:`insert(int32, int64) <StringBuilder-insert-int32-int64>`

    :ref:`insert(int32, float32) <StringBuilder-insert-int32-float32>`

    :ref:`insert(int32, String) <StringBuilder-insert-int32-String>`

UUID
----

.. _UUID:

class UUID
``````````

Class encapsulates common operations for UUID.

User could work with UUID using this class, for example, generate random
UUID or test for nil.

The string representation used by this class are in the following form:

01234567-0123-0123-0123-0123456789ab

**Extends**
    :ref:`Object <Object>`

**Implements**
    :ref:`Cloneable <Cloneable>`

.. _UUID-new:

new(): void
~~~~~~~~~~~

Constructor creates new instance represents nil.

.. _UUID-delete:

delete(): void
~~~~~~~~~~~~~~

Destructor releases resources.

.. _UUID-clone:

clone(): UUID
~~~~~~~~~~~~~

Create new instance with the same UUID value.

**Return**
    The created instance.

*[virtual]*

.. _UUID-hash:

hash(): int64
~~~~~~~~~~~~~
Generate hash value according to UUID value.

**Return**
    The generated hash value.

**Remarks**
    O(1) time complexity.

*[virtual]*

.. _UUID-isNil:

isNil(): bool
~~~~~~~~~~~~~

Test if this is nil.

**Return**
    Boolean value indicates this is nil or not.

**Return Value**
    **true**  This is nil.

    **false** This is not nil.

**See**
    :ref:`setNil() <UUID-setNil>`

.. _UUID-setNil:

setNil(): void
~~~~~~~~~~~~~~

Set this to nil.

**See**
    :ref:`isNil() <UUID-isNil>`

.. _UUID-isEqual-UUID:

isEqual(rhs: UUID): bool
~~~~~~~~~~~~~~~~~~~~~~~~

Test if this is the same as the given one.

**Return**
    Boolean indicates they are the same.

**Return Value**
    **true**  They are the same.

    **false** They are not the same.

.. _UUID-toString:

toString(): String
~~~~~~~~~~~~~~~~~~

Dump the value to equivalent string representation.

**Return**
    The generated string representation by contained value.

.. _UUID-nil:

nil(): UUID
~~~~~~~~~~~

Create new instance represents value nil.

**Return**
    The new instance.

*[static]*

.. _UUID-random:

random(): UUID
~~~~~~~~~~~~~~

Create new instance with randomized value.

**Return**
    The new instance.

*[static]*

.. _UUID-parse-String:

parse(s: String): UUID
~~~~~~~~~~~~~~~~~~~~~~

Create new instance with value from given string representation.

**Return**
    The new instance.

**Return Value**
    **null**     The given string representation is not fit the expected form.

    **non-null** The new instance with the parsed value.

*[static]*

