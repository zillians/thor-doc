System Library API reference
============================

Thor provides commonly-required functionalities in daily programming tasks with its system library.

**thor.lang**
    It acts like a set of built-in functions and types because it is implicitly
    imported as ``import .= thor.lang;``.
    Most of them are already refered in the previous sections. We still list
    them here for a quick reference, and elaborate the rest.

**thor.container**
    Provides common data structures to organize data.

**thor.math**
    Mathematics functions.

**thor.util**
    Mostly handy algorithms and utility functions.

.. toctree::

    container
    lang
    math
    unmanaged
    util
