thor.unmanaged
==============

.. UnmanagedApi.t

.. _ptr_-T:

class ptr_<T>
-------------

Class reserves the same size of pointer for variables.

**Remark**
    This class is provided as current workaround to reserve spaces for
    native pointers. Please do **NOT** use it for other purposes.

**Extends**
    :ref:`Object <Object>`
