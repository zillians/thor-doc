thor.math
=========

.. Math.t

.. _hugeValue:

hugeValue(): float64
````````````````````

Floating point value indicates that the function's
result value is too large in magnitude to be
representable with its return type.

.. _infinity:

infinity(): float64
```````````````````

Floating point value represents positive infinity.

**Return**
    Positive infinity.

.. _NaN:

NaN(): float64
``````````````

Floating point value represents Not-a-Number.

**Return**
    Not-a-Number.

.. _radix:

radix(): float64
````````````````

Base for all floating point types(float32, float64).

**Return**
    Base of floating point

.. _roundMode:

roundMode(): int32
``````````````````

Return value corresponding to the current rounding
mode.

**Return Value**
    **-1** undetermined.

    **0**  toward zero

    **1**  to nearest

    **2**  toward positive infinity

    **3**  toward negative infinity

.. _isFinite-float32:

isFinite(arg: float32): bool
````````````````````````````

Determines if the given floating point number **arg** has finite
value i.e. it is normal, subnormal or zero, but not infinite or
NaN.

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** has finite value.

    **false** **arg** is infinite.

.. _isFinite-float64:

isFinite(arg: float64): bool
````````````````````````````

Determines if the given floating point number **arg** has finite
value i.e. it is normal, subnormal or zero, but not infinite or
NaN.

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** has finite value.

    **false** **arg** is infinite.

.. _isInfinite-float32:

isInfinite(arg: float32): bool
``````````````````````````````

Determines if the given floating point number **arg** is
positive or negative infinity.

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** is infinite.

    **false** **arg** has finite value.

.. _isInfinite-float64:

isInfinite(arg: float64): bool
``````````````````````````````

Determines if the given floating point number **arg** is
positive or negative infinity.

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** is infinite.

    **false** **arg** has finite value.

.. _isNaN-float32:

isNaN(arg: float32): bool
`````````````````````````

Determines if the given floating point number **arg** is
not-a-number (NaN).

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** is NaN.

    **false** Otherwise.

.. _isNaN-float64:

isNaN(arg: float64): bool
`````````````````````````
Determines if the given floating point number **arg** is
not-a-number (NaN).

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** is NaN.

    **false** Otherwise.

.. _isNormal-float32:

isNormal(arg: float32): bool
````````````````````````````

Determines if the given floating point number **arg** is
normal, i.e. is neither zero, subnormal, infinite, nor
NaN.

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** is normal.

    **false** Otherwise.

.. _isNormal-float64:

isNormal(arg: float64): bool
````````````````````````````

Determines if the given floating point number **arg** is
normal, i.e. is neither zero, subnormal, infinite, nor
NaN.

**Parameters**
    **arg** Floating point value.

**Return Value**
    **true**  **arg** is normal.

    **false** Otherwise.

.. _acos-float64:

acos(arg: float64): float64
```````````````````````````

Compute arc cosine of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Arc cosine of **arg** in radians.

**Return Value**
    **[0, π]** If **arg** is inside the range [-1.0, 1.0].

    **NaN()**  If **arg** is outside the range [-1.0, 1.0].

**See**
    :ref:`NaN() <NaN>`

.. _asin-float64:

asin(arg: float64): float64
```````````````````````````

Compute arc sine of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Arc sine of **arg** in radians.

**Return Value**
    **[0, π]** If **arg** is inside the range [-1.0, 1.0].

    **NaN()**  if **arg** is outside the range [-1.0, 1.0].

**See**
    :ref:`NaN() <NaN>`

.. _atan-float64:

atan(arg: float64): float64
```````````````````````````

Compute arc tangent of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Arc tangent of **arg** in radians in the range [-π/2, π/2].

.. _atan2-float64-float64:

atan2(y: float64, x: float64): float64
``````````````````````````````````````

Compute the inverse tangent of ``y/x`` using the signs of arguments to
correctly determine quadrant.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
   Arc tangent of ``y/x`` in radians in the range [-π, π].

.. _cos-float64:

cos(arg: float64): float64
``````````````````````````

Compute cosine of **arg**.

**Parameters**
    **arg** Floating point value representing angle in radians.

**Return**
    Cosine of **arg**.

**Return Value**
    **[-1.0, 1.0]** For normal floating point values.

    **NaN()**       If **arg** is infinite.

**See**
    :ref:`NaN() <NaN>`

.. _sin-float64:

sin(arg: float64): float64
``````````````````````````

Compute sine of **arg**.

**Parameters**
    **arg** Floating point value representing angle in radians.

**Return**
    Sine of **arg**.

**Return Value**
    **[-1.0, 1.0]** For normal floating point values.

    **NaN()**       If **arg** is infinite.

**See**
    :ref:`NaN() <NaN>`

.. _tan-flaot64:

tan(arg: float64): float64
``````````````````````````

Compute tangent of **arg**.

**Parameters**
    **arg** Floating point value representing angle in radians.

**Return**
    Tangent of **arg**.

**Return Value**
    **NaN()** if **arg** is infinite.

    **Other** Tangent of **arg**.

**See**
    :ref:`NaN() <NaN>`

.. _acosh-float64:

acosh(arg: float64): float64
````````````````````````````

Compute hyperbolic area cosine of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Inverse hyperbolic cosine of **arg**.

.. _asinh-float64:

asinh(arg: float64): float64
````````````````````````````

Compute area hyperbolic sine of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Inverse hyperbolic sine of **arg**.

.. _atanh-float64:

atanh(arg: float64): float64
````````````````````````````

Compute area hyperbolic tangent of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    **arg** Inverse hyperbolic tangent of **arg**.

.. _cosh-float64:

cosh(arg: float64): float64
```````````````````````````

Compute hyperbolic cosine of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Hyperbolic cosine of **arg**.

.. _sinh-float64:

sinh(arg: float64): float64
```````````````````````````

Compute hyperbolic sine of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Hyperbolic sine of **arg**.

.. _tanh-float64:

tanh(arg: float64): float64
```````````````````````````

Compute hyperbolic tangent of **arg**.

**Paramters**
    **arg** Floating point value.

**Return**
    Hyperbolic tangent of **arg**.

.. _exp-float64:

exp(arg: float64): float64
``````````````````````````

Compute the e (Euler's number, ``2.7182818``) raised to
the given power **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    e Raised to the power **arg**.

**Return Value**
    **hugeValue()** If the result is too large for **float64** to hold it.

    **Other**       A normal floating point value.

**See**
    :ref:`hugeValue() <hugeValue>`

.. _exp2-float64:

exp2(n: float64): float64
`````````````````````````

Compute 2 raised to the given power **n**.

**Parameters**
    **n** Floating point value.

**Return**
    2 raised to the **n**.

**Return Value**
    **hugeValue()** If the result is too large for **float64** to hold it.

    **Other**       A normal floating point value.

**See**
    :ref:`hugeValue() <hugeValue>`

.. _expm1-float64:

expm1(arg: float64): float64
````````````````````````````

Compute the e (Euler's number, ``2.7182818``) raised to
the given power **arg**, minus 1.

This function is more accurate than the expression
``exp(arg)-1`` if **arg** is close to zero.

**Parameters**
    **arg** Floating point value.

**Return**
    :math:`e^{arg}-1`

**Return Value**
    **hugeValue()** If the result is too large for **float64** to hold it.

    **Other**       A normal floating point value.

**See**
    :ref:`hugeValue() <hugeValue>`

.. _ilogb0:

ilogb0(): float64
`````````````````

Special value returned by ilogb()

The return value indicates the argument of ilogb() is 0.

**See**
    :ref:`ilogb() <ilogb-float64>`

.. _ilogbNaN:

ilogbNaN(): float64
```````````````````

Special value returned by ilogb()

The return value indicates the argument of ilogb() is NaN().

**See**
    :ref:`ilogb() <ilogb-float64>`

    :ref:`isNaN() <isNaN-float64>`

.. _ilogb-float64:

ilogb(arg: float64): int64
``````````````````````````

Extract the value of the exponent from the floating-point
argument **arg**, and returns it as a signed integer value.

Formally, the result is the integral part of :math:`\log_{r}{|arg|}`
as a signed integral value, for non-zero **arg**, where r is radix().

**Parameters**
    **arg** Floating point value.

**Return**
    The floating-point exponent, cast to integer.

**Return Value**
    **ilogb0()** If **arg** is zero.

    :math:`2^{31}-1` If **arg** is infinite(the largest value **int64** can represents).

    **ilogbNaN()** If **arg** is NaN.

    **Other** A normal integer value.

**Remark**
    If the result cannot be represented as **int64**, the result is undefined.

**See**
    :ref:`ilogb0() <ilogb0>`

    :ref:`ilogbNaN() <ilogbNaN>`

    :ref:`radix() <radix>`

.. _ldexp-float64-int64:

ldexp(arg: float64, iexp: int64): float64
`````````````````````````````````````````

Multiply a floating point value **arg** by ``2`` raised to power **iexp**.

**Parameters**
    **arg** Floating point value.

    **iexp** Integer value.

**Return**
    **arg** × :math:`2^{exp}`.

**Return Value**
    **hugeValue()** If the result is too large for **float64** to hold it.

    **Other** A normal floating point value.

**See**
    :ref:`hugeValue() <hugeValue>`

.. _log-float64:

log(arg: float64): float64
``````````````````````````

Compute the natural (base e) logarithm of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Natural logarithm of **arg**.

**Return Value**
    **NaN()** If **arg** is negative.

    **-hugeValue()** If **arg** is ``0``.

    **Other** A normal floating point value.

**See**
    :ref:`NaN() <NaN>`

    :ref:`hugeValue() <hugeValue>`

.. _log10-float64:

log10(arg: float64): float64
````````````````````````````

Compute the common (base ``10``) logarithm of **arg**.

**Parameters**
    *arg* Floating point value.

**Return**
    Base ``10`` logarithm of **arg**.

**Return Value**
    **NaN()** If **arg** is negative.

    **-hugeValue()** If **arg** is ``0``.

    **Other** A normal floating point value for other cases.

**See**
    :ref:`NaN() <NaN>`

    :ref:`hugeValue() <hugeValue>`

.. _log1p-float64:

log1p(arg: float64): float64
````````````````````````````

Compute the natural (base **e**) logarithm of ``1+arg``.

This function is more precise than the expression ``log(1+arg)``
if ``arg`` is close to zero.

**Parameters**
    **arg** Floating point value.

**Return**
    :math:`\ln(1+arg)`

**Return Value**
    **NaN()** If **arg** is negative.

    **-hugeValue()** If **arg** is ``0``.

    **Other** A normal floating point value for other cases.

**See**
    :ref:`NaN() <NaN>`

    :ref:`hugeValue() <hugeValue>`

.. _log2-float64:

log2(arg: float64): float64
```````````````````````````

Compute the base ``2`` logarithm of ``arg``.

**Parameters**
    **arg** Floating point value.

**Return**
    :math:`\log_2(arg)`

**Return Value**
    **NaN()** If **arg** is negative.

    **-hugeValue()** If **arg** is ``0``.

    **Other** A normal floating point value for other cases.

**See**
    :ref:`NaN() <NaN>`

    :ref:`hugeValue() <hugeValue>`

.. _logb-float64:

logb(arg: float64): float64
```````````````````````````

Extract the value of the exponent from the floating-point
argument **arg**, and returns it as a floating-point value.

Formally, the result is the integral part of :math:`\log_{r}{|arg|}`
as a signed floating-point value, for non-zero arg, where r is
radix(). If **arg** is subnormal, it is treated as though it
was normalized.

**Parameters**
    **arg** Floating piont value.

**Return**
    The floating-point exponent.

**Remark**
    Domain or range error may occur if **arg** is zero.

**See**
    :ref:`radix() <radix>`

.. _scalbn-float64-int64:

scalbn(x: float64, n: int64): float64
`````````````````````````````````````

Multiply a floating point value x by radix() raised to power **n**.

**Parameters**
    **arg** Floating point value.

    **n** Integer value.

**Return**
    x × :math:`radix()^{exp}`.

**Return Value**
    **hugeValue()** If the result is too large for **float64** to hold it.

    **Other** A normal floting point value.

**See**
    :ref:`hugeValue() <hugeValue>`

    :ref:`radix() <radix>`

.. _cbrt-float64:

cbrt(arg: float64): float64
```````````````````````````

Compute cubic root of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Cubic root of **arg**.

.. _fabs-float64:

fabs(arg: float64): float64
```````````````````````````

Compute the absolute value of a floating point value **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    The absolute value of **arg** (:math:`|arg|`).

.. _hypot-float64-float64:

hypot(x: float64, y: float64): float64
``````````````````````````````````````

Compute the square root of the sum of the squares of
**x** and **y**, without undue overflow or underflow
at intermediate stages of the computation.

This is the length of the hypotenuse of a right-angled
triangle with sides of length **x** and **y**, or the distance
of the point (**x**, **y**) from the origin (0,0), or the
magnitude of a complex number ``x+iy``.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
    The hypotenuse of a right-angled triangle.

.. _pow-float64-float64:

pow(base: float64, n: float64): float64
```````````````````````````````````````

Compute the value of **base** raised to the power **n**.

**Parameters**
    **base** Floating point value.

    **n** Floating point value.

**Return**
    base raised by power.

**Return Value**
    **NaN()** If **base** == 0 and **n** <= 0, or **base** < 0 and **n** is not an integer.

    **hugeValue()** If an overflow takes place.

    **Other** A normal floating point value for other cases.

**See**
    :ref:`hugeValue() <hugeValue>`

    :ref:`NaN() <NaN>`

.. _sqrt:

sqrt(arg: float64): float64
```````````````````````````

Compute square root of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Square root of **arg**.

**Return Value**
    **NaN()** If **arg** is negative.

    **Other** A Non-negative floating point value.

**See**
    :ref:`NaN() <NaN>`

.. _error function: http://en.wikipedia.org/wiki/Error_function

.. _erf-float64:

erf(arg: float64): float64
``````````````````````````

Compute the `error function`_ of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    The value of the error function of **arg**.

.. _complementary error function: http://en.wikipedia.org/wiki/Complementary_error_function

.. _erfc-float64:

erfc(arg: float64): float64
```````````````````````````

Compute the `complementary error function`_ of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    The value of the complementary error function of **arg**.

**See**
    :ref:`erf(float64) <erf-float64>`

.. _lgamma-float64:

.. _gamma function: http://en.wikipedia.org/wiki/Gamma_function

lgamma(arg: float64): float64
`````````````````````````````
Compute the natural logarithm of the absolute value of the `gamma function`_ of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    :math:`\mathrm{lgamma}(x) = \ln{|\Gamma(x)|}`

**See**
    :ref:`tgamma() <tgamma-float64>`

.. _tgamma-float64:

tgamma(arg: float64): float64
`````````````````````````````

Compute the `gamma function`_ of **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    :math:`\mathrm{tgamma}(x) = \Gamma(x)`

.. _ceil-float64:

ceil(arg: float64): float64
```````````````````````````

Compute nearest integer not less than **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Nearest integer not less than **arg**.

.. _floor-float64:

floor(arg: float64): float64
````````````````````````````

Compute nearest integer not greater than **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Nearest integer not greater than **arg**.

.. _nearbyint-float64:

nearbyint(arg: float64): float64
````````````````````````````````

Round the floating-point argument **arg** to an
integer value in floating-point format, using
the current rounding mode.

**Parameters**
    **arg** Floating point value.

**Return**
    The integer result of rounding **arg**.

.. _rint-float64:

rint(arg: float64): float64
```````````````````````````

Round the floating-point argument **arg** to an
integer value in floating-point format, using the
current rounding mode.

**Parameters**
    **arg** Floating point value.

**Return**
    The integer result of rounding **arg**.

.. _irint-float64:

irint(arg: float64): int64
``````````````````````````

Round the floating-point argument **arg** to an
integer value in floating-point format, using the
current rounding mode.

**Parameters**
    **arg** Floating point value.

**Return**
    The integer result of rounding **arg**.

.. _round-float64:

round(arg: float64): float64
````````````````````````````

Compute nearest integer to **arg**. Number is
rounded away from zero in halfway cases.

**Parameters**
    **arg** Floating point value.

**Return**
    Nearest integer to **arg**.

.. _trunc-float64:

trunc(arg: float64): float64
````````````````````````````

Compute nearest integer not greater in magnitude
than **arg**.

**Parameters**
    **arg** Floating point value.

**Return**
    Nearest integer not greater in magnitude than **arg**.

.. _mod-float64:

mod(x: float64, y: float64): float64
````````````````````````````````````

Compute the remainder of the division operation
``x/y``.

Specifically, the returned value is ``x - n*y``, where n is
``x/y`` with its fractional part truncated. The returned value
will have the same sign as **x**.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
    Remainder of dividing arguments. The returned
    value will have the same sign as **x**.

.. _remainder-float64:

remainder(x: float64, y: float64): float64
``````````````````````````````````````````

Compute the signed remainder of the floating
point division operation ``x/y``.

Specifically, the returned value is ``x - n*y``,
where n is ``x/y`` rounded to the nearest integer,
or the nearest even integer if ``x/y`` is halfway
between two integers. In contrast to fmod(), the returned
value is not guaranteed to have the same sign as **x**. If the
returned value is ``0``, it will have the same sign as ``x``.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
    Remainder of dividing arguments.

.. _copysign-float64-float64:

copysign(x: float64, y: float64): float64
`````````````````````````````````````````

Compose a floating point value with the magnitude
of **x** and the sign of **y**.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
    Floating point value with the magnitude of **x** and
    the sign of **y**

.. _nextafter-float64-float64:

nextafter(from: float64, to: float64): float64
``````````````````````````````````````````````

Return the next representable value of **from** in
the direction of **to**. If **from** equals to **to**,
**to** is returned.

**Parameters**
    **from** Floating point value.

    **to** Floating point value.

**Return**
    The next representable value of **from** in the
    direction of **to**.

.. _nexttoward-float64-float64:

nexttoward(from: float64, to: float64): float64
```````````````````````````````````````````````

Return the next representable value of **from** in
the direction of **to**. If **from** equals to **to**,
**to** is returned.

**Parameters**
    **from** Floating point value.

    **to** Floating point value.

**Return**
    The next representable value of **from** in the
    direction of **to**.

.. _dim-float64-float64:

dim(x: float64, y: float64): float64
````````````````````````````````````

Return the positive difference between **x** and **y**.

This could be implemented as ``max(x - y, 0)``,
so if ``x ≤ y``, the result is always equals to
``0``, otherwise it is ``x - y``.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
    The positive difference value.

**See**
    :ref:`max() <max-float64-float64>`

.. _max-float64-float64:

max(x: float64, y: float64): float64
````````````````````````````````````

Return the larger of two floating point arguments.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
    The larger of two floating point values.

**See**
    :ref:`min() <min-float64-float64>`

.. _min-float64-float64:

min(x: float64, y: float64): float64
````````````````````````````````````

Return the smaller of two floating point arguments.

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

**Return**
    The smaller of two floating point values.

**See**
    :ref:`max() <max-float64-float64>`

.. _ma-float64-float64-float64:

ma(x: float64, y: float64, z: float64): float64
```````````````````````````````````````````````

The ma function computes ``(x*y) + z``,
rounded as one ternary operation, according to the
rounding mode characterized by the value of
roundMode().

**Parameters**
    **x** Floating point value.

    **y** Floating point value.

    **x** Floating point value.

**Return**
    ``(x*y) + z``, rounded as one ternary operation.

**See**
    :ref:`roundMode <roundMode>`
