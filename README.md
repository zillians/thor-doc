How to generate document
========================

At current directory, type command ``make``, then the resulting documentations  
will be generated under **./build** folder(in HTML form).

Environment Setting
===================

Ubuntu 12.04 or later versions
------------------------------

*Install SPHINX*  
SPHINX is Python Documentation Generator, view detail [here](http://sphinx-doc.org/).

*Install Packages*  
``sudo apt-get install texlive``  
``sudo apt-get install texlive-latex-extra``
